import React from 'react';
import {observer} from "mobx-react"
import { ILoginState, RoleEnum, compareRoles } from './store/LoginState';
import Axios from "axios"
import { IStatusState } from './store/StatusState';
import { BrowserRouter, Switch, Route } from "react-router-dom"
import Login from './pages/Login';
import NavigationBar from './components/NavigationBar';
import Landing from './pages/Landing';
import Dashboard from './pages/Dashboard';
import NotFound from './pages/NotFound';
import Forbidden from './pages/Forbidden';
import Users from './pages/Users';
import UserCreate from './pages/User/UserCreate';
import UserSelf from './pages/User/UserSelf';
import UserModify from './pages/User/UserModify';
import ItemTypes from './pages/ItemTypes';
import ItemTypeCreate from './pages/ItemTypes/ItemTypeCreate';
import ItemTypeModify from './pages/ItemTypes/ItemTypeModify';
import ItemsByItemType from './pages/Item/ItemsByItemtype';
import Shelves from './pages/Shelves';
import ShelfCreate from './pages/Shelves/ShelfCreate';
import Shelf from './pages/Shelves/Shelf';
import Item from './pages/Item';
import { AppState } from './store/AppState';
import PrintQueue from './pages/PrintQueue';
import ShelfEdit from './pages/Shelves/ShelfEdit';

class App extends React.Component<AppState>{

	private canAccessResource(role : RoleEnum) : boolean{
		if(!this.props.loginState.isLoggedIn){
			return false;
		}
		return compareRoles(role, this.props.loginState.role as RoleEnum) >= 0
	}

	public render(){
		return (
			<div className="App">
				<BrowserRouter>
					<NavigationBar 
						loginState={this.props.loginState} 
						statusState={this.props.statusState} 
					/>
					<Switch>
							<Route path="/login" exact>
								<Login loginState={this.props.loginState}/>
							</Route>
							<Route exact path="/users">
								{this.canAccessResource("manager") ? <Users/> : <Forbidden/>}
							</Route>
							<Route path="/users/:uuid" render={
								props => {
									if(!this.canAccessResource("manager")){
										return <Forbidden/>
									}
									const userId=props.match.params.uuid
									switch(userId){
										case "new":
											return <UserCreate loginState={this.props.loginState} />
										case "self":
											return <UserSelf loginState={this.props.loginState} />
										default:
											return <UserModify uuid={userId} loginState={this.props.loginState} />
									}
								}
							}  />
							<Route exact path="/types">
								{this.canAccessResource("regular") ? <ItemTypes canModify={this.canAccessResource("manager")} /> : <Forbidden/>}
							</Route>
							<Route path="/types/:uuid/edit" render={
								props => {
									if(!this.canAccessResource("manager")){
										return <Forbidden/>
									}
									const typeId = props.match.params.uuid;
									return <ItemTypeModify uuid={typeId}/>
								}
							} />
							<Route path="/printqueue" exact>{this.canAccessResource("regular") ? <PrintQueue/> : <Forbidden/>}</Route>
							<Route path="/types/new" exact>{this.canAccessResource("manager") ? <ItemTypeCreate/> : <Forbidden/> }</Route>
							<Route path="/types/:uuid" render={
								props => {
									if(!this.canAccessResource("regular")){
										return <Forbidden/>
									}
									return <ItemsByItemType type={props.match.params.uuid} />
								}
							}/>
							<Route path="/shelves" exact>{this.canAccessResource("regular") ? <Shelves canModify={this.canAccessResource("manager")}/> : <Forbidden/>}</Route>
							<Route path="/shelves/:uuid/edit" render={
								props => {
									if(!this.canAccessResource("manager")){
										return <Forbidden/>
									}else{
										return <ShelfEdit uuid={props.match.params.uuid}/>
									}
								}
							}/>
							<Route path="/shelves/:uuid" render={
								props => {
									if(!this.canAccessResource("manager")){
										return <Forbidden/>
									}else{
										if(props.match.params.uuid === "new"){
											return <ShelfCreate/> 
										}else{
											return <Shelf uuid={props.match.params.uuid} />
										}
									}
								}
							}	/>
							<Route path="/items/:uuid" render={
								props => {
									if(!this.canAccessResource("regular")){
										return <Forbidden/>
									}else{
										return <Item uuid={props.match.params.uuid} canDelete={this.canAccessResource("manager")} />
									}
								}
							}/>

							<Route exact path="/">
								{this.props.loginState.isLoggedIn ? <Dashboard loginState={this.props.loginState} />: <Landing/>}
							</Route>
							<Route>
								<NotFound />
							</Route>
					</Switch> 
				</BrowserRouter>
			</div>
		);
	}

	public componentDidMount(){
		const getStatus = Axios.get("/api/status");
		const getLoggedIn = Axios.get("/api/auth", { validateStatus: function(status){ return [200, 404].some(e => e === status)}});
		Promise.all([getStatus, getLoggedIn]).then(([statusObject, loggedInObject]) => {
			const status : IStatusState = {
				version: statusObject.data.version,
				productName: statusObject.data.productName
			}
			const loggedIn = loggedInObject.status === 200;

			if(loggedIn){
				const dataObj = loggedInObject.data.data;
				const loginObject : ILoginState = {
					username: dataObj.username as string,
					firstname: dataObj.firstname as string,
					lastname: dataObj.lastname as string,
					role: dataObj.role as string,
					uuid: dataObj.uuid as string
				};
				this.props.loginState.updateByStatusCall(loginObject);
			}else{
				this.props.loginState.updateByStatusCall(false);
			}
			this.props.statusState.updateByStatusCall(status)
			
			
		})
	}
}

export default observer((App));
