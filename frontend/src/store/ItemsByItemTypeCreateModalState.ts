import { action, makeObservable, observable } from "mobx";

export class ItemsByItemTypeCreateModalState {
    @observable autoCompleteData: [string, number][] = [];
    @observable notes: string = "";
    @observable shelveName: string = "";
    @observable selectedShelf?: number;

    constructor() {
        makeObservable(this);
    }

    @action updateNotes(str: string) { this.notes = str; }
    @action updateShelveName(str: string) { this.shelveName = str; this.selectedShelf = undefined; }
    @action updateAutocompleteData(data: [string, number][]) { this.autoCompleteData = [...data]; }
    @action updateSelectedShelf(data: [string, number]) { this.selectedShelf = data[1] ?? undefined; }
}
