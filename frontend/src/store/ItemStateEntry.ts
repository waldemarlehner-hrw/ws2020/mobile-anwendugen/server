import { action, computed, makeObservable, observable } from "mobx";



export class ItemStateEntry {
    private backendState?: ItemStateEntry
    @observable status: string
    @observable notes: string
    @observable type: IType
    @observable shelf?: {
        uuid: string,
        name: string
    }

    @action updateNotes(str: string){ this.notes = str; }
    @action updateStatus(str: string){ this.status = str;}
    @action updateSelectedShelf(name: string, uuid: string){
        this.shelf = { name , uuid }
    }
    @action removeShelf(){
        if(this.shelf){
            this.shelf = undefined;
        }
    }
    @action resetNotes(){this.notes = this.backendState?.notes ?? ""}
    @action resetStatus(){this.status = this.backendState?.status ?? ""}
    @action resetShelf(){this.shelf = this.backendState?.shelf ?? undefined}

    @action resetAll(){
        this.resetNotes();
        this.resetShelf();
        this.resetStatus();
    }

    @computed get simplifiedStatus(){
        if(["stored","moving"].some(e => e === this.status)){
            return "default"
        }
        return this.status;
    }

    @computed get changed(){
        return {
            status: this.simplifiedStatus !== this.backendState?.simplifiedStatus,
            notes: (this.backendState?.notes?.trim() ?? "") !== this.notes,
            type: this.backendState?.type.uuid !== this.type.uuid,
            shelf: this.backendState?.shelf?.uuid !== this.shelf?.uuid
        }
    }

    @computed get anyChanged(){
        return Object.values(this.changed).some(e => e)
    }

    constructor(initialStatus: string, initialType: IType, initialNotes: string, parent?: ItemStateEntry){
        this.notes = initialNotes;
        this.status = initialStatus;
        this.type = initialType;
        this.backendState = parent;
        makeObservable(this);
    }
    
    public clone() : ItemStateEntry{
        const newEntry = new ItemStateEntry(this.status, this.type, this.notes, this);
        if(this.notes){
            newEntry.notes = this.notes;
        }
        if(this.shelf){
            newEntry.shelf = {
                uuid: this.shelf.uuid,
                name: this.shelf.name
            }
        }
        return newEntry;
    }

    @computed get patchBody(){
        const patchBodyFactory = new PatchBodyFactory(this);
        if(this.changed.notes){
            patchBodyFactory.withNotes()
        }
        if(this.changed.shelf){
            patchBodyFactory.withShelf()
        }
        if(this.changed.status){
            patchBodyFactory.withStatus()
        }
        console.warn(patchBodyFactory.build())
        return patchBodyFactory.build()
    }
}

class PatchBodyFactory{
    private deltaState;
    private body : PatchBodyFactoryResponse = {}

    public constructor(deltaState: ItemStateEntry){
        this.deltaState = deltaState;
    }

    public withNotes(){
        this.body.notes = this.deltaState.notes?.trim()
    }
    public withShelf(){
        if(this.deltaState.shelf){
            this.body.shelf = this.deltaState.shelf.uuid
        }else{
            this.body.shelf = "remove"
        }
    }
    public withStatus(){
        if(this.deltaState.simplifiedStatus === "default"){
            if(this.deltaState.changed.status){
                this.body.status = this.deltaState.shelf ? "stored" : "moving"
            }
        }else{
            this.body.status = this.deltaState.status
        }
    }

    public build(){
        return this.body;
    }
}

interface PatchBodyFactoryResponse{
    notes?:string,
    shelf?:string,
    status?:string
}

interface IType {
    name: string,
    description: string,
    uuid: number
}

