import { makeObservable, observable } from "mobx";
import LoginState from './LoginState';
import StatusState from './StatusState';


export class AppState {

	constructor() {
		this.loginState = new LoginState();
		this.statusState = new StatusState();
		makeObservable(this);
	}

	@observable
	loginState;

	@observable
	statusState;
}
