import { action, computed, makeObservable, observable } from "mobx";


export default class UserSelfState {
	@observable username: string = ""
	@observable newPassword: string = "";
	@observable oldPassword: string = "";
	@observable firstname: string = "";
	@observable lastname: string = "";
	@observable isLoading: boolean = false;
	@observable responseError?: string;
	

	@action clearFields(){
		this.newPassword = this.oldPassword = this.firstname = this.lastname = ""
		this.responseError = undefined;
	}

	@action updateUsername(str: string){
		this.username = str;
	}

	@action updateResponseErrorMessage(str : string | undefined){
		this.responseError = str;
	}

	@action updateNewPassword(password: string){
		this.newPassword = password;
	}
	@action updateOldassword(password: string){
		this.oldPassword = password;
	}

	@action updateFirstname(name: string){
		this.firstname = name;
	}
	@action updateLastname(name: string){
		this.lastname = name;
	}
	constructor(){
		makeObservable(this)
	}

	@computed get errorMessages() : string[] {
		const msgs : string[] = []
		if(this.username !== "" && this.username.trim().length < 5){
			msgs.push("Username needs to be at least 5 characters long")
		}
		if(this.firstname !== "" && this.firstname.trim().length === 0){
			msgs.push("First Name cannot be empty.")
		}
		if(this.lastname !== "" && this.lastname.trim().length === 0){
			msgs.push("Last Name cannot be empty.")
		}
		
		if(this.oldPassword !== "" && this.oldPassword.length < 8){
			msgs.push("Old Password needs to be at least 8 Characters long.")
		}
		if(this.newPassword !== "" && this.newPassword.length < 8){
			msgs.push("New Password needs to be at least 8 Characters long.")
		}
		if(this.newPassword !== "" && this.oldPassword === ""){
			msgs.push("Please provide the old password. If you have forgotten it, contact a higher-up.")
		}
		return msgs;
	}

	@computed get canCommit(){
		if(this.errorMessages.length > 0 || this.isLoading){
			return false;
		}else{
			return [this.oldPassword, this.newPassword, this.firstname, this.lastname, this.username].some(e => e.length > 0)
		}
	}

	@action updateLoading(bool: boolean){
		this.isLoading = bool;
	}
		
	

	@action.bound
	onResponse(){
		
	}

}