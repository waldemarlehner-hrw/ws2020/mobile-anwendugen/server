import { action, makeObservable, observable } from "mobx";
import { IItemTypeEntry } from "./ItemTypeState";
import { IItem } from "../pages/Item/ItemsByItemtype";


export class ItemsByItemTypeState {

    @observable isLoading = true;
    @observable entries: IItem[] = [];
    @observable itemType?: IItemTypeEntry;
    @observable notFound = false;
    @observable showCreateModal = false;

    constructor() {
        makeObservable(this);
    }

    @action updateData(data: any) {
        this.itemType = {
            uuid: data.uuid,
            description: data.description,
            capacity: data.capacity,
            ordersCount: data.ordersCount,
            itemCount: data.itemCount,
            name: data.name
        };
        this.entries = [...data.items as []];
    }
    @action updateIsLoading(bool: boolean) { this.isLoading = bool; }
    @action updateNotFound(bool: boolean) { this.notFound = bool; }
    @action updateShowCreateModal(bool: boolean) { this.showCreateModal = bool; }
}
