import { action, computed, makeObservable, observable } from "mobx";
import { RoleEnum } from "./LoginState";

export default class UserSearchbarState{

	@observable isExpanded: boolean;
	@observable username: string;
	@observable firstname: string;
	@observable lastname: string;

	@observable roleRoot: boolean;
	@observable roleAdmin: boolean;
	@observable roleManager: boolean;
	@observable roleRegular: boolean;

	constructor(){
		makeObservable(this);	
		this.isExpanded = false;
		this.username = "";
		this.firstname = "";
		this.lastname = "";
		this.roleAdmin = false;
		this.roleRegular = false;
		this.roleRoot = false;
		this.roleManager = false;
	}

	@action clickAdmin(){ this.roleAdmin = !this.roleAdmin; }
	@action clickRoot(){ this.roleRoot = !this.roleRoot; }
	@action clickRegular(){ this.roleRegular = !this.roleRegular; }
	@action clickManager(){ this.roleManager = !this.roleManager; }
	@action clickExpand(){ this.isExpanded = !this.isExpanded; }

	@action updateUsername(username: string){ this.username = username; }
	@action updateFirstname(firstname: string){ this.firstname = firstname; }
	@action updateLastname(lastname: string){ this.lastname = lastname; }

	@computed get anyRoleSelected(){
		return [this.roleAdmin, this.roleRegular, this.roleRoot, this.roleManager].some(e => e);
	}

	@computed get queryParams(){
		return {
			...this.username.length > 0 && {username: this.username},
			...(this.isExpanded && this.firstname.length > 0) && {firstname: this.firstname},
			...(this.isExpanded && this.lastname.length > 0) && {lastname: this.lastname},
			...(this.selectedRoles.length > 0) && {roles: this.selectedRoles}
		}
	}

	@computed get selectedRoles(){
		const array : RoleEnum[] = [];
		if(this.anyRoleSelected){
			if(this.roleAdmin){
				array.push("admin")
			}
			if(this.roleRegular){
				array.push("regular")
			}
			if(this.roleManager){
				array.push("manager")
			}
			if(this.roleRoot){
				array.push("root")
			}
		}
		return array;
	}
}