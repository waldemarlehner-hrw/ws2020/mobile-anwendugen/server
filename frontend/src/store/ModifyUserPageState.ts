import { computed, makeObservable, observable, action } from "mobx";
import { RoleEnum } from "./LoginState";

class ModifyUserPageState{

	@observable doesUserExist: boolean = false;
	@observable isLoading: boolean = true;
	@observable backendState?: IUser 
	@observable currentState: IUser

	@observable showDeleteAlert: boolean = false;

	@observable usernameInUse: boolean = false
	@observable responseError: string | undefined
	@observable showSuccess: boolean = false;

	constructor(){
		makeObservable(this);
		this.currentState = {
			username: "",
			firstname: "",
			lastname: "",
			role: "regular",
			password: ""
		}
	}
	@action updateShowSuccess(bool: boolean){
		this.showSuccess = bool;
	}

	@action updateDoesUserExist(bool: boolean){
		this.doesUserExist = bool;
	}
	@action updateBackendState(user : IUser){
		this.backendState = user;
		this.currentState = {
			username: "",
			firstname: "",
			lastname: "",
			password: "",
			role: user.role
		}
	}
	@action updateLoading(bool: boolean){
		this.isLoading = bool;
	}

	@action updateShowDeleteAlert(bool: boolean){
		this.showDeleteAlert = bool;
	}

	@action updateUsername(string : string){
		if(this.currentState){
			this.currentState = {
				...this.currentState,
				username: string
			};
		}
	}
	@action updateFirstname(string : string){
		if(this.currentState){
			this.currentState = {
				...this.currentState,
				firstname: string
			};
		}
	}
	@action updateLastname(string : string){
		if(this.currentState){
			this.currentState = {
				...this.currentState,
				lastname: string
			};
		}
	}
	@action updateRole(string : string){
		if(this.currentState){
			this.currentState = {
				...this.currentState,
				role: string as RoleEnum
			};
		}
	}
	@action updatePassword(string: string){
		if(this.currentState){
			this.currentState = {
				...this.currentState,
				password: string
			};
		}
	}

	@computed get changedValues(){
		return {
			username: (this.currentState?.username.length ?? 0) > 0,
			firstname: (this.currentState?.firstname.length ?? 0) > 0,
			lastname: (this.currentState?.lastname.length ?? 0) > 0,
			password: (this.currentState?.password?.length ?? 0) > 0,
			role: typeof this.currentState?.role !== "undefined" && this.currentState.role !== this.backendState?.role
		}
	}

	@computed get anyValueChanged(){
		return Object.values(this.changedValues).some(e => e)
	}

	@computed get canCommit(){
		return this.errorsWithInput.length === 0
	}

	@computed get errorsWithInput(){
		const arr : string[] = [];
		if(this.currentState.username !== "" && this.currentState.username.trim().length < 5){
			arr.push("Username needs to be at least 5 characters long")
		}
		if(this.currentState.password && this.currentState.password !== "" && this.currentState.password.length < 8){
			arr.push("Password needs to be at least 8 characters long")
		}
		return arr;
	}

	@computed get deltaValues(){
		return {
			...this.changedValues.username && {username: this.currentState.username.trim()},
			...this.changedValues.firstname && {firstname: this.currentState.firstname.trim()},
			...this.changedValues.lastname && {lastname: this.currentState.lastname.trim()},
			...(this.changedValues.password && this.currentState.password) && {password: this.currentState.password},
			...this.changedValues.role && {role: this.currentState.role}
		}
	}
}

export interface IUser{
	username: string,
	firstname: string,
	lastname: string,
	password?: string,
	role: RoleEnum
}

export default ModifyUserPageState;