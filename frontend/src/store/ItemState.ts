import { observable, makeObservable, action, computed } from "mobx";
import { ItemStateEntry } from "./ItemStateEntry";



export default class ItemState{

    @observable notFound: boolean = false;
    @observable isLoading: boolean = true;
    @observable showSuccess: boolean = false;
    @observable backendState?: ItemStateEntry
    @observable deltaState?: ItemStateEntry
    @observable showUpdateShelfModal: boolean = false;
    @observable showDeleteModal: boolean = false;
    @observable isInPrintQueue: boolean = false;

    @observable shelfNameSearch : string = ""
    @observable shelvesAutofill : {name:string, uuid:string}[] = []

    constructor(){
        makeObservable(this)
    }

    @action updateIsInPrintQueue(bool: boolean){this.isInPrintQueue = bool;}
    @action updateNotFound(bool: boolean){this.notFound = bool;}
    @action updateShowDeleteModal(bool: boolean){this.showDeleteModal = bool;}
    @action updateShelvesAutofill(data: {name:string, uuid:string}[]){ this.shelvesAutofill = [...data]; }
    @action updateShowSuccess(bool: boolean){this.showSuccess = bool;}
    @action updateIsLoading(bool: boolean){this.isLoading = bool;}
    @action updateState(e: any){
        console.log(e)
        this.backendState = new ItemStateEntry(e.status,e.type,e.notes ?? "");
        if(e.shelf){
            this.backendState.shelf = e.shelf
        }

        this.deltaState = this.backendState.clone()
    }

    @action updateShowUpdateShelfModal(bool: boolean){
        this.showUpdateShelfModal = bool;
    }
    @action updateShelfNameSearch(str: string){
        this.shelfNameSearch = str;
    }

    @computed get canCommit(){
        return (this.deltaState?.notes?.trim().length ?? 256) <= 255
    }

    @computed get canDelete(){
        return this.backendState?.simplifiedStatus === "default"
    }

}