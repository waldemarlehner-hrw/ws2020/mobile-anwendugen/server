import { action, computed, makeObservable, observable } from "mobx";

export class ShelfCreateState {
    @observable shelfName: string = "";
    @observable capacity: string = "";
    constructor() {
        makeObservable(this);
    }

    @observable showSuccess: boolean = false;
    @observable isLoading: boolean = false;
    @observable responseError?: string;

    @action reset() {
        this.shelfName = "";
        this.capacity = "";
    }

    @action updateShelfName(str: string) { this.shelfName = str; }
    @action updateCapacity(str: string) { this.capacity = str; }
    @action updateShowSuccess(bool: boolean) { this.showSuccess = bool; }
    @action updateIsLoading(bool: boolean) { this.isLoading = bool; }
    @action updateResponseError(str?: string) {
        this.responseError = str;
    }

    @computed get errorsWithInput() {
        let returnArray: string[] = [];
        if (this.shelfName.trim().length < 5) {
            returnArray.push("Name needs to be at least 5 characters long.");
        }
        if (Number.isNaN(Number(this.capacity))) {
            returnArray.push("Needed capacity cannot be empty.");
        }
        else if (Number(this.capacity) <= 0) {
            returnArray.push("The required capacity needs to be positive.");
        }
        return returnArray;
    }

    @computed get canCreate() {
        return this.errorsWithInput.length === 0;
    }

    @computed get postBody() {
        if (this.canCreate) {
            return {
                name: this.shelfName,
                capacity: this.capacity
            };
        } else {
            return {};
        }
    }
}
