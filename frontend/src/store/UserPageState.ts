import { action, makeObservable, observable } from "mobx";
import { RoleEnum } from "./LoginState";
import UserSearchbarState from "./UserSearchbarState";

export default class UserPageState{
	@observable searchbar : UserSearchbarState;
	@observable result : IUserPageStateResult[];
	@observable isLoading: boolean = false;

	constructor(){
		makeObservable(this);
		this.searchbar = new UserSearchbarState();
		this.result = [];
	}

	@action updateLoading(bool: boolean){
		this.isLoading = bool;
	}

	@action updateResult(data: IUserPageStateResult[]){
		this.result = [...data]
	}

}


export interface IUserPageStateResult {
	username: string,
	firstname: string,
	lastname: string,
	role: RoleEnum,
	uuid: string
}