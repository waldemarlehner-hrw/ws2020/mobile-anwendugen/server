import { action, computed, makeObservable, observable } from "mobx";


export default class LoginPageState{
	@observable userName: string;
	@observable password: string;
	@observable lastTryFailed: boolean;
	@observable lastTryFailedMessage?: string;
	@observable isLoading: boolean;

	@observable openLostPassword: boolean;
	@observable openGetAccess: boolean;

	constructor(){
		this.openLostPassword = false;
		this.openGetAccess = false;
		this.userName = "";
		this.password = "";
		this.isLoading = false;
		this.lastTryFailed = false;
		makeObservable(this)
		console.info(this)
	}

	@action clickLostPasswordBtn(){
		if(this.openLostPassword){
			this.openLostPassword = false;
		}else{
			this.openGetAccess = false;
			this.openLostPassword = true;
		}
	}

	@action clickGetAccessBtn(){
		if(this.openGetAccess){
			this.openGetAccess = false;
		}else{
			this.openGetAccess = true;
			this.openLostPassword = false;
		}
	}

	@action updateUsername(newUsername: string){
		console.info(this.userName, newUsername)
		this.userName = newUsername;
	}

	@action updatePassword(newPassword: string){
		this.password = newPassword;
	}

	@action updateLastTryFailed(hasFailed: boolean, reason?: string){
		this.lastTryFailed = hasFailed;
		this.lastTryFailedMessage = reason;
	}

	@action startLoading(){
		this.isLoading = true;
	}

	@action stopLoading(){
		this.isLoading = false;
	}


	
	@computed get isUsernameValid(){
		return this.userName.trim().length >= 5;
	}

	@computed get isPasswordValid(){
		return this.password.length >= 8;
	}
}
