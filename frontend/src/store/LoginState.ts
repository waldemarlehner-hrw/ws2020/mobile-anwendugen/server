
import { action, makeObservable, observable } from "mobx";

export default class LoginState{
	
	@observable isLoggedIn: boolean;
	@observable isInit: boolean;
	@observable username?: string;
	@observable firstname?: string;
	@observable lastname?: string;
	@observable role?: string;
	@observable uuid?: string

	constructor(){
		this.isInit = false;
		this.isLoggedIn = false;
		makeObservable(this);
	}

	@action updateByStatusCall(response : false | ILoginState){
		if(response === false){
			this.isLoggedIn = false;
			this.isInit = true;
			this.username = this.firstname = this.lastname = this.role = undefined;
		}else{
			this.username = response.username;
			this.firstname = response.firstname;
			this.lastname = response.lastname;
			this.role = response.role;
			this.isLoggedIn = true;
			this.uuid = response.uuid;
		}
	}

	
}

export interface ILoginState{
	username: string
	firstname: string,
	lastname: string
	role: string,
	uuid: string
}

export type RoleEnum = "regular" | "manager" | "admin" | "root";

/**
 * Checks which role has the higher precedence.
 * @param r1 First Role
 * @param r2 Second Role
 * @returns A number indicating which role is higher. `return < 0` means r1 is higher, `return > 0` means r2 is higher, `return == 0` means that both are at the same level.
 */
export function compareRoles(r1: RoleEnum, r2: RoleEnum): number{
	const pos1 = AllRolesByPrecedence.indexOf(r1);
	const pos2 = AllRolesByPrecedence.indexOf(r2);
	return pos1-pos2;
}

export const AllRolesByPrecedence = ["root", "admin", "manager", "regular"]