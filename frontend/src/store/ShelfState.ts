import { action, makeObservable, observable } from "mobx";
import { IItemFromShelf } from "../pages/Shelves/Shelf";

export class ShelfState {
    @observable isLoading: boolean = true;
    @observable items: IItemFromShelf[] = [];
    @observable name: string = "";
    @observable capacity: number = 0;
    @observable capacityUsed: number = 0;
    @observable doesNotExist = false;
    @observable isInPrintQueue: boolean = false;

    constructor() {
        makeObservable(this);
    }

    @action updateIsInPrintQueue(bool: boolean){this.isInPrintQueue = bool;}
    @action updateFromAPIResponse(e: any) {
        this.name = e.name;
        this.capacity = e.capacity;
        this.capacityUsed = e.capacityUsed;
        this.items = e.items ?? [];
    }
    @action updateIsLoading(bool: boolean) {
        this.isLoading = bool;
    }
    @action updateDoesNotExist(bool: boolean) {
        this.doesNotExist = bool;
    }
}
