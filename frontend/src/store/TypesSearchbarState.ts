import { action, computed, makeObservable, observable } from "mobx";

export default class TypesSearchbarState{

	@observable typeName : string ;
	@observable isExpanded: boolean;
	@observable onlyDisplayUsed: boolean;

	constructor(){
		makeObservable(this);
		this.typeName = ""
		this.isExpanded = false;
		this.onlyDisplayUsed = false;
	}

	@action updateTypeName(str: string){this.typeName = str;}
	@action onExpandClick(){this.isExpanded = !this.isExpanded;}
	@action onOnlyDisplayUsedClick(){this.onlyDisplayUsed = !this.onlyDisplayUsed;}

	@computed get queryObject(){
		return {
			...this.typeName && {name: this.typeName},
			...this.onlyDisplayUsed && {onlyUsed: true}
		}
	}
}