import { makeObservable, observable, action, computed } from "mobx";

export default class ShelfEditState{

    constructor(){
        makeObservable(this);
    }

    @observable isLoading: boolean = false;
    @observable showDeleteModal: boolean = false;
    @observable showSuccess: boolean = false;
    @observable oldName?: string;
    @observable newName?: string;
    @observable oldCapacity?: string;
    @observable newCapacity?: string;
    @observable isEmpty: boolean = false;
    @observable showNotExists: boolean = false;

    @action updateShowNotExists(bool: boolean){this.showNotExists = bool;}
    
    @action updateShowDeleteModal(bool: boolean){this.showDeleteModal = bool;}
    @action updateShowSuccess(bool: boolean){this.showSuccess = bool;}
    @action updateIsLoading(bool: boolean){this.isLoading = bool;}
    @action updateName(str: string){this.newName = str;}
    @action updateCapacity(cap: string){this.newCapacity = cap;}
    @action updateFromBackend(name: string, cap: string, empty: boolean){
        this.oldCapacity = cap;
        this.oldName = name;
        this.isEmpty = empty;
    }
    
    @computed get anyValueChanged(){
        return Object.values(this.changedValues).some(e => e === true);
    }
    @computed get errorsWithInput(){
        let arr: string[] = [];
        if(this.changedValues.name){
            const trimmed = (this.newName as string).trim();
            if((trimmed.length) <= 5){
                arr.push("Name needs to be longer that 5 Characters")
            }
        }
        if(this.changedValues.capacity){
            const capacity = Number(this.newCapacity);
            if(Number.isNaN(capacity)){
                arr.push("New Capacity is not a number");
            }else if(capacity <= 0){
                arr.push("Capacity needs to be positive.");
            }
        }
        return arr;
    }
    @computed get changedValues(){
        const name = typeof this.newName === "string" && this.newName !== this.oldName && this.newName !== "";
        const capacity = typeof this.newCapacity === "string" && this.newCapacity !== this.oldCapacity && this.oldCapacity !== "";
        return {name, capacity};
    }
    @computed get canDelete(){
        return this.isEmpty;
    }
    @computed get canUpdate(){
        return !this.isLoading && this.anyValueChanged && this.errorsWithInput.length === 0
    }
    @computed get patchBody(){
        if(this.anyValueChanged){
            return {
                ...this.changedValues.name && {name: this.newName},
                ...this.changedValues.capacity && {capacity: this.newCapacity}
            }
        }
        return {}
    }
}

