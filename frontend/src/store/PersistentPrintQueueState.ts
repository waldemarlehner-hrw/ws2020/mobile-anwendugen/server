import { action, computed, makeObservable, observable } from "mobx";

export class PersistentPrintQueueState{
    private static _instance: PersistentPrintQueueState | undefined = undefined;
    private usePersistentStore: boolean
    private constructor(){
        this.usePersistentStore = isLocalStorageSupported();
        if(this.usePersistentStore){
            const dataStringLocalStorage = localStorage.getItem("printqueue");
            if(dataStringLocalStorage){
                this.entries = JSON.parse(dataStringLocalStorage);
            }else{
                this.entries = [];
                this.persist();
            }
        }else{
            this.entries = [];   
        }
        makeObservable(this);
    }
    public static get instance(){
        if(!this._instance){
            this._instance = new PersistentPrintQueueState();
        }
        return this._instance;
    }

    @observable 
    private entries : PersistentPrintQueueEntry[];

    @action
    public addEntry(entry: PersistentPrintQueueEntry){
        this.entries.push(entry);
        this.entries = [...this.entries];
        this.persist();
    }

    @action 
    public removeEntry(index: number){
        this.entries.splice(index,1)
        this.entries = [...this.entries];
        this.persist();
    }

    @action
    public selectAll(){
        for(let entry of this.entries){
            entry.selected = true;
        }
        this.persist();
    }
    @action
    public deselectAll(){
        for(let entry of this.entries){
            entry.selected = false;
        }
        this.persist();
    }

    @computed get count(){
        return this.entries.length;
    }

    @action swapSelection(uuid: string){
        let i = 0;
        for(i; i < this.entries.length; i++){
            if(this.entries[i].uuid === uuid){
                this.entries[i] = {
                    ...this.entries[i],
                    selected: !this.entries[i].selected
                }
                break;
            }
        }
        this.persist();
    }

    @computed get selected(){
        return this.entries.filter(e => e.selected);
    }

    @computed get all(){
        return this.entries;
    }

    public get isPersistent(){
        return this.usePersistentStore;
    }

    private persist(){
        localStorage.setItem("printqueue", JSON.stringify(this.entries.map(e => {
            const returnObject : PersistentPrintQueueEntry = {
                type: e.type,
                uuid: e.uuid,
                selected: e.selected
            };
            return returnObject;
        })));
    }

}

export default PersistentPrintQueueState.instance;

export interface PersistentPrintQueueEntry{
    type: "shelf" | "item",
    uuid: string,
    selected: boolean
}

function isLocalStorageSupported(){
    try{
        const test = "test";
        localStorage.setItem(test,test);
        localStorage.removeItem(test);
        return true;
    }catch{
        return false;
    }
}