
import { action, makeObservable, observable } from "mobx";

export default class StatusState{
	
	@observable isInit: boolean;
	@observable productName?: string;
	@observable version?: string;


	constructor(){
		this.isInit = false;
		makeObservable(this);
	}

	@action updateByStatusCall(response : IStatusState){
		this.isInit = true;
		this.productName = response.productName;
		this.version = response.version;
	}

	
}

export interface IStatusState{
	productName: string,
	version: string
}