import { action, computed, makeObservable, observable } from "mobx";
import { IShelfEntry } from "../pages/Shelves";

export class CreateItemModalState {
    private itemtypeuuid: string;
    @observable shelfInput: string = "";
    @observable notes: string = "";
    @observable status: string = "default";

    @observable autocompleteShelves: IShelfEntry[] = [];
    @observable selectedShelfUuid: string = "";

    @observable useShelf: boolean = false;
    constructor(itemtypeuuid: string) {
        this.itemtypeuuid = itemtypeuuid;
        makeObservable(this);
    }

    @action clear() {
        this.shelfInput = "";
        this.notes = "";
        this.status = "default";
        this.selectedShelfUuid = "";
        this.shelfInput = "";
        this.autocompleteShelves = [];
        this.useShelf = false;
    }
    @action updateNotes(str: string) { this.notes = str; }
    @action updateStatus(str: string) { this.status = str; }
    @action updateShelfInput(str: string) { this.shelfInput = str; }
    @action flipUseShelf() { this.useShelf = !this.useShelf; }
    @action updateAutocompleteData(data: IShelfEntry[]) { this.autocompleteShelves = data; }
    @action selectShelf(uuid: string) { this.selectedShelfUuid = uuid; }

    @computed get getObject() {
        if (this.shelfInput.trim().length > 0) {
            return { nameLike: this.shelfInput.trim() };
        } else {
            return {};
        }
    }

    @computed get canCreate() {
        if (this.notes.trim().length > 255) {
            return false;
        }
        if (this.useShelf) {
            if (this.selectedShelfUuid === "") {
                return false;
            }
        }
        return true;
    }

    @computed get postArgs() {
        return {
            ...this.notes.trim().length > 0 && { notes: this.notes.trim() },
            ...this.status.trim() !== "default" && { status: this.status.trim() }
        };
    }

    @computed get postUrl() {
        return "/api/items/" + this.itemtypeuuid + (this.useShelf ? "/" + this.selectedShelfUuid : "");
    }
}
