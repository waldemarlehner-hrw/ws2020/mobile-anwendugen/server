import { observable, makeObservable, action, computed } from "mobx";

export default class ShelvesSearchbarState {
    @observable isExpanded: boolean = false;
    @observable onlyDisplayUsed: boolean = false;
    @observable shelfName: string = ""

    constructor(){
        makeObservable(this);
    }

    @action updateShelfName(str: string){ this.shelfName = str; }
    @action onOnlyDisplayUsedClick(){ this.onlyDisplayUsed = !this.onlyDisplayUsed; }
    @action onIsExpandedClick(){this.isExpanded = !this.isExpanded;}

    @computed get queryObject(){
        return {
            ...this.shelfName.trim().length > 0 && {nameLike: this.shelfName.trim()},
            noEmpty: this.onlyDisplayUsed
        }
    }
    
}