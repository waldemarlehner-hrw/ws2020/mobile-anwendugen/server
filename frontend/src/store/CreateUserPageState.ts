import { action, computed, makeObservable, observable } from "mobx";
import { RoleEnum } from "./LoginState";


export default class CreateUserPageState {

	@observable username: string = ""
	@observable firstname: string = ""
	@observable lastname: string = ""
	@observable password: string = ""
	@observable role: RoleEnum = "regular"
	@observable usernameInUse: boolean = false
	@observable responseError: string | undefined
	@observable showSuccess: boolean = false;


	@observable isLoading: boolean = false

	constructor(){
		makeObservable(this);
	}

	@action updateShowSuccess(bool: boolean){
		this.showSuccess = bool;
	}
	@action updateUsernameInUse(bool: boolean){
		this.usernameInUse = bool;
	}
	@action updateUsername(string : string){
		this.username = string;
	}
	@action updateFirstname(string : string){
		this.firstname = string;
	}
	@action updateLastname(string : string){
		this.lastname = string;
	}
	@action updateRole(role: RoleEnum){
		this.role = role;
	}
	@action updatePassword(string : string){
		this.password = string;
	}
	@action updateLoading(bool: boolean){
		this.isLoading = bool;
	}
	@action updateResponseError(msg? : string){
		this.responseError = msg;
	}
	@action clear(){
		this.username = this.firstname = this.lastname = ""
        this.role = "regular"
        this.password = ""
	}


	@computed get canCreate(){
		if([this.firstname, this.lastname].some(e=> e.length === 0)){
			return false;
		}
		if(this.password.length < 8 || this.username.length < 5){
			return false;
		}
		return true;
	}

	@computed get errorsWithInput(){
		const arr : string[] = [];
		if(this.username.trim().length < 5){
			arr.push("Username needs to be at least 5 characters long")
		}
		if(this.password.length < 8){
			arr.push("Password needs to be at least 8 characters long")
		}
		if(this.firstname.length === 0){
			arr.push("First name cannot be empty")
		}
		if(this.lastname.length === 0){
			arr.push("Last name cannot be empty")
		}
		return arr;
	}
}
