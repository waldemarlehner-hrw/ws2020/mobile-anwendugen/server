import { observable, makeObservable, action } from "mobx";
import TypesSearchbarState from "./TypesSearchbarState";

export class ItemTypesState {
    @observable data : IItemTypeEntry[] = []
    @observable isLoading: boolean = true;
    @observable searchbarState : TypesSearchbarState;

    constructor(){
        this.searchbarState = new TypesSearchbarState();
        makeObservable(this);
    }

    @action updateIsLoading(bool: boolean){
        this.isLoading = bool;
    }
    @action updateData(data: IItemTypeEntry[]){
        this.data = data;
    }
}

export interface IItemTypeEntry{
    uuid: number,
    name: string,
    description: string,
    capacity: string,
    ordersCount: number,
    itemCount: number
}
