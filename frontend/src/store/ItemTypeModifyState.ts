import { action, computed, makeObservable, observable } from "mobx";
import { ICurrentState, IBackendState } from "../pages/ItemTypes/ItemTypeModify";

export class ItemTypeModifyState {

    @observable isLoading = true;
    @observable showSuccess = false;
    @observable showNotExists = false;
    @observable showDeleteAlert = false;

    @observable currentState: ICurrentState = { name: "", description: "", capacity: "" };
    @observable backendState?: IBackendState;

    constructor() {
        makeObservable(this);
    }

    @action updateIsLoading(bool: boolean) { this.isLoading = bool; }
    @action updateName(str: string) { this.currentState.name = str; }
    @action updateDescription(str: string) { this.currentState.description = str; }
    @action updateCapacity(str: string) { this.currentState.capacity = str; }
    @action updateShowSuccess(bool: boolean) { this.showSuccess = bool; }
    @action updateShowNotExists(bool: boolean) { this.showNotExists = bool; }
    @action updateBackendState(newState: IBackendState) { this.backendState = newState; }
    @action updateShowDeleteAlert(bool: boolean) { this.showDeleteAlert = bool; }

    @computed get changedValues() {
        return {
            name: this.currentState.name !== "" && this.currentState.name !== this.backendState?.name,
            description: this.currentState.description !== "" && this.currentState.description !== this.backendState?.description,
            capacity: this.currentState.capacity !== "" && this.currentState.capacity !== this.backendState?.capacity
        };
    }
    @computed get anyValueChanged() {
        const cv = this.changedValues;
        return cv.capacity || cv.description || cv.name;
    }
    @computed get errorsWithInput() {
        const returnArray: string[] = [];
        if (this.changedValues.name && this.currentState.name.trim().length < 5) {
            returnArray.push("Name needs to be at least 5 characters long.");
        }
        if (this.changedValues.capacity) {
            if (Number.isNaN(Number(this.currentState.capacity))) {
                returnArray.push("Input is not a valid number");
            }
            const number = Number(this.currentState.capacity);
            if (number <= 0) {
                returnArray.push("Required Capacity needs to be positive number.");
            }
        }
        return returnArray;
    }
    @computed get patchBody() {
        return {
            ...this.changedValues.name && { name: this.currentState.name },
            ...this.changedValues.description && { description: this.currentState.description },
            ...this.changedValues.capacity && { capacity: this.currentState.capacity }
        };
    }
    @computed get canUpdate() {
        if (this.isLoading) {
            return false;
        }
        if (this.errorsWithInput.length > 0) {
            return false;
        }
        return this.anyValueChanged;
    }
    @computed get canDelete() {
        if (this.backendState) {
            return (Number(this.backendState.orderCount) === 0 && Number(this.backendState.itemCount) === 0);
        } else {
            return false;
        }
    }
}
