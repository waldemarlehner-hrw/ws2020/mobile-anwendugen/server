import axios from "axios";
import { observer } from "mobx-react";
import React from "react";
import Alert from "react-bootstrap/esm/Alert";
import Button from "react-bootstrap/esm/Button";
import Container from "react-bootstrap/esm/Container";
import FormControl from "react-bootstrap/esm/FormControl";
import InputGroup from "react-bootstrap/esm/InputGroup";
import BetterBreadcrumbs from "../../components/BetterBreadcrumbs";
import CreateUserPageState from "../../store/CreateUserPageState";
import LoginState, { compareRoles, RoleEnum } from "../../store/LoginState";


class UserCreate extends React.Component<{loginState: LoginState}, CreateUserPageState>{

	constructor(props: {loginState: LoginState}){
		super(props)
		this.state = new CreateUserPageState();
	}

	private async tryCreate(){
		if(!this.state.canCreate){
			return;
		}
		this.state.updateLoading(true)
		const response = await axios.post("/api/users",{
			username: this.state.username.trim(),
			firstname: this.state.firstname.trim(),
			lastname: this.state.lastname.trim(),
			password: this.state.password,
			role: this.state.role
		}, {validateStatus: (state) => [200,400,403,409].some(e=>e===state)});
		this.state.updateLoading(false)
		if(response.status === 200){
			this.state.updateShowSuccess(true);
			this.state.updateResponseError();
			this.state.clear();
		}else if(response.status === 409){
			this.state.updateResponseError("A user already exists with that username");
			this.state.updateShowSuccess(false);
		}
		else{
			this.state.updateShowSuccess(false);
			this.state.updateResponseError(response.data.reason);
		}
	}


	public render() : JSX.Element{
		return <>
			<BetterBreadcrumbs activeNode="New" previousRoute={[{name: "Dashboard", link:"/"}, {name:"Users", link:"/users"}]}/>
			<Container className="mt-3">
				<h1>Add User</h1>
				<hr/>
				{this.state.showSuccess && <Alert onClose={()=>{this.state.updateShowSuccess(false)}} dismissible variant="success">Added User</Alert>}
				{this.state.responseError && <Alert onClose={()=>{this.state.updateResponseError()}} dismissible variant="danger">Cound not complete creation: {this.state.responseError}</Alert>}
				{this.state.usernameInUse && <Alert variant="danger">Username is already in use.</Alert>}
				{this.state.errorsWithInput.map((e,i) => {
					return <Alert key={i}  variant="warning">{e}</Alert>
				})}
				<InputGroup className="mb-2">
					<InputGroup.Prepend><InputGroup.Text>Username</InputGroup.Text></InputGroup.Prepend>
					<FormControl disabled={this.state.isLoading} placeholder="Enter Username" value={this.state.username} onChange={e => this.state.updateUsername(e.target.value)} />
					<FormControl as="select" value={this.state.role} onChange={(e)=>{this.state.updateRole(e.target.value as RoleEnum)}}>
						<option value="regular" disabled={compareRoles(this.props.loginState.role as RoleEnum, "regular") >= 0}>Regular</option>
						<option value="manager" disabled={compareRoles(this.props.loginState.role as RoleEnum, "manager") >= 0}>Manager</option>
						<option value="admin" disabled={compareRoles(this.props.loginState.role as RoleEnum, "admin") >= 0}>Admin</option>
						<option value="root" disabled={compareRoles(this.props.loginState.role as RoleEnum, "root") >= 0}>Root</option>
					</FormControl>
				</InputGroup>

				<InputGroup className="mb-2">
					<InputGroup.Prepend><InputGroup.Text>Name</InputGroup.Text></InputGroup.Prepend>
					<FormControl disabled={this.state.isLoading} placeholder="Enter First Name" value={this.state.firstname} onChange={e => this.state.updateFirstname(e.target.value)} />
					<FormControl disabled={this.state.isLoading} placeholder="Enter Last Name" value={this.state.lastname} onChange={e => this.state.updateLastname(e.target.value)} />
				</InputGroup>

				<InputGroup className="mb-2">
					<InputGroup.Prepend><InputGroup.Text>Password</InputGroup.Text></InputGroup.Prepend>
					<FormControl disabled={this.state.isLoading} placeholder="Enter Password" type="password" value={this.state.password} onChange={e => this.state.updatePassword(e.target.value)} />
				</InputGroup>

				<Button onClick={()=>this.tryCreate()} variant="primary" disabled={!this.state.canCreate}>
					Create
				</Button>
			</Container>
		</>
	}
}

export default observer(UserCreate);