import React from "react";
import Container from "react-bootstrap/Container";
import { RouteComponentProps, withRouter } from "react-router-dom";
import { observer } from "mobx-react";
import LoginState, { compareRoles, RoleEnum } from "../../store/LoginState";
import Row from "react-bootstrap/esm/Row";
import Collapse from "react-bootstrap/esm/Collapse";
import Alert from "react-bootstrap/esm/Alert";
import Button from "react-bootstrap/esm/Button";
import FormControl from "react-bootstrap/esm/FormControl";
import Fade from "react-bootstrap/esm/Fade";
import InputGroup from "react-bootstrap/esm/InputGroup";
import Badge from "react-bootstrap/esm/Badge";
import ModifyUserPageState, { IUser } from "../../store/ModifyUserPageState";
import axios from "axios";
import ButtonGroup from "react-bootstrap/ButtonGroup"
import Modal from "react-bootstrap/Modal"
import BetterBreadcrumbs from "../../components/BetterBreadcrumbs";

interface IUserModifyPageProps{
	loginState: LoginState
	uuid: string
}

class UserModify extends React.Component<IUserModifyPageProps & RouteComponentProps, ModifyUserPageState>{

	constructor(props: IUserModifyPageProps & RouteComponentProps){
		super(props);
		this.state = new ModifyUserPageState()
	}

	async componentDidMount(){
		// Get user by Id
		this.getBackendData();
	}

	private async getBackendData(){
		this.state.updateLoading(true);
		const response = await axios.get("/api/users/"+this.props.uuid,{validateStatus: (status)=>[200,404,400].some(e => e === status)}) 
		if(response.status === 200){
			const user = response.data.data;
			const fetchedUser : IUser = {
				username: user.username,
				firstname: user.firstName,
				lastname: user.lastName,
				role: user.role
			}
			this.state.updateBackendState(fetchedUser)
			this.state.updateDoesUserExist(true)
		}else{
			/// 404
			this.state.updateDoesUserExist(false)
		}
		this.state.updateLoading(false);
	}




	usernameSubFragment(){
		return (<>
			<Fade in={this.state.changedValues.username}>
				<div><Badge variant="primary">Modified</Badge></div>
			</Fade>
			<InputGroup >
				<InputGroup.Prepend><InputGroup.Text>Username</InputGroup.Text></InputGroup.Prepend>
				<FormControl 
				disabled={this.state.isLoading} 
				value={this.state.currentState.username ?? ""} 
				onChange={(e) => {this.state.updateUsername(e.target.value)}} 
				placeholder={this.state.backendState?.username ?? "..."}/>
			</InputGroup>
			<footer className="text-muted mb-1 text-subtext">Usernames need to be at least 5 characters long. You will get notified if a user already exists with that username.</footer>
		</>)
	}

	nameSubFragment(){
		return(<>
			<Fade in={this.state.changedValues.firstname || this.state.changedValues.lastname}>
				<div><Badge variant="primary">Modified</Badge></div>
			</Fade>
			<InputGroup className="mb-1">
				<InputGroup.Prepend><InputGroup.Text>Name</InputGroup.Text></InputGroup.Prepend>
				<FormControl 
					disabled={this.state.isLoading} 
					value={this.state.currentState.firstname ?? ""} 
					onChange={(e) => {this.state.updateFirstname(e.target.value)}} 
					placeholder={this.state.backendState?.firstname ?? "..."}/>
				<FormControl 
					disabled={this.state.isLoading} 
					value={this.state.currentState.lastname ?? ""} 
					onChange={(e) => {this.state.updateLastname(e.target.value)}} 
					placeholder={this.state.backendState?.lastname ?? "..."}/>
			</InputGroup>
		</>)
	}

	roleSubFragment(){
		return (<>
			<Fade in={this.state.changedValues.role}>
				<div><Badge variant="primary">Modified</Badge></div>
			</Fade>
			<InputGroup className="mb-1">
				<InputGroup.Prepend>
					<InputGroup.Text>Role</InputGroup.Text>
				</InputGroup.Prepend>
				<FormControl onChange={(e) => {this.state.updateRole(e.target.value)}} value={this.state.currentState?.role} disabled={this.state.isLoading||typeof this.state.backendState?.role === "undefined"} as="select">
					<option value="root" disabled={compareRoles(this.props.loginState.role as RoleEnum, "root") >= 0 }>Root</option>
					<option value="admin" disabled={compareRoles(this.props.loginState.role as RoleEnum, "admin") >= 0 } >Admin</option>
					<option value="manager" disabled={compareRoles(this.props.loginState.role as RoleEnum, "manager") >= 0 }>Manager</option>
					<option value="regular"  disabled={compareRoles(this.props.loginState.role as RoleEnum, "regular") >= 0 }>Regular</option>
				</FormControl>
			</InputGroup>
		</>)
	}

	passwordSubFragment(){
		return (<>
			<Collapse in={this.state.changedValues.password}>
				<div><Badge variant="primary">Modified</Badge></div>
			</Collapse>
			<InputGroup className="mb-4">
				<InputGroup.Prepend><InputGroup.Text>Password</InputGroup.Text></InputGroup.Prepend>
				<FormControl disabled={typeof this.state.backendState === "undefined" || this.state.isLoading} value={this.state.currentState?.password} onChange={(e)=>this.state.updatePassword(e.target.value)} type="password" placeholder="Password"></FormControl>
			</InputGroup>
		</>)
	}

	renderForm(){
		return <>
			<Collapse in={this.state.anyValueChanged}>
				<div>
					<Alert variant="info" className="">
						You have unsaved changed.
					</Alert>
				</div>
			</Collapse>
			<Collapse in={this.state.showSuccess}>
				<div>
					<Alert dismissible variant="success" className="" onClose={()=>this.state.updateShowSuccess(false)}>
						Modified successfully.
					</Alert>
				</div>
			</Collapse>
			
			<Collapse in={this.state.errorsWithInput.length > 0}>
				<div>
					{this.state.errorsWithInput.map((e,i) => {
						return <Alert key={i} variant="warning">{e}</Alert>
					})}
				</div>
			</Collapse>

			{this.usernameSubFragment()}
			{this.nameSubFragment()}			
			{this.roleSubFragment()}
			{this.passwordSubFragment()}
			
			<ButtonGroup>
				<Button disabled={!this.state.canCommit} onClick={e => this.commit()} variant="primary">Commit Changes</Button>
				<Button disabled={!this.state.canCommit} onClick={e => this.state.updateShowDeleteAlert(true)} variant="outline-danger">Delete</Button>
			</ButtonGroup>
</>
	}

	render() : JSX.Element{
		return(<>
			<BetterBreadcrumbs activeNode={this.state.backendState?.username ?? this.props.uuid} previousRoute={[{name: "Dashboard", link:"/"}, {name:"Users", link:"/users"}]}/>
			<Container className="mt-5">
			<Row>
				<h1 className="mx-3">Modify User</h1>
			</Row>
			<hr/>

			{this.state.isLoading || compareRoles(this.props.loginState.role as RoleEnum, this.state.backendState?.role ?? "root") < 0?
			this.renderForm() :
			<Alert variant="danger">Cannot modify this user as they need to have a lower role than you.</Alert>}
				
							
			</Container>
			<Modal show={this.state.showDeleteAlert} onHide={()=>this.state.updateShowDeleteAlert(false)}>
				<Modal.Header closeButton>Are you sure you want to delete<span className="mx-1 text-primary"> {this.state.backendState?.username} </span></Modal.Header>
				<Modal.Body> The account holder will lose access to their account.</Modal.Body>
				<Modal.Footer>
					<Button variant="danger" onClick={()=>this.delete()}>Delete Account</Button>
					<Button variant="link" onClick={()=>this.state.updateShowDeleteAlert(false)}>Cancel</Button>
				</Modal.Footer>
			</Modal>
		</>)
	}

	private async commit(){
		this.state.updateLoading(true);
		const updatedFields = this.state.deltaValues;
		const response = await axios.patch("/api/users/"+this.props.uuid, updatedFields)
		if(response.status === 200){
			this.state.updateShowSuccess(true)
			await this.getBackendData()
		}
		this.state.updateLoading(false);
	}

	private async delete(){
		const response = await axios.delete("/api/users/"+this.props.uuid);
		if(response.status === 200){
			this.props.history.push("/users")
		}
	}

	
}

export default withRouter(observer(UserModify));