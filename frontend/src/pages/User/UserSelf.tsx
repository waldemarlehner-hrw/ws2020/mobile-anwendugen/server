import axios from "axios";
import { observer } from "mobx-react";
import React from "react";
import Alert from "react-bootstrap/esm/Alert";
import Badge from "react-bootstrap/esm/Badge";
import Button from "react-bootstrap/esm/Button";
import Collapse from "react-bootstrap/esm/Collapse";
import Container from "react-bootstrap/esm/Container";
import Fade from "react-bootstrap/esm/Fade";
import FormControl from "react-bootstrap/esm/FormControl";
import InputGroup from "react-bootstrap/esm/InputGroup";
import BetterBreadcrumbs from "../../components/BetterBreadcrumbs";
import LoginState, { ILoginState } from "../../store/LoginState";
import UserSelfState from "../../store/UserSelfPageState";

class UserSelf extends React.Component<{loginState: LoginState}, UserSelfState>{

	constructor(props : {loginState: LoginState}){
		super(props)
		this.state = new UserSelfState();
	}

	async onCommit(){
		const patchObject : any = {
			...(this.state.username) && {username: this.state.username.trim()},
			...(this.state.firstname) && {firstname: this.state.firstname.trim()},
			...(this.state.lastname) && {lastname: this.state.lastname.trim()},
			...(this.state.newPassword && this.state.oldPassword) && {
				oldpassword: this.state.oldPassword,
				password: this.state.newPassword
			}
		}

		const res = await axios.patch("/api/users/self", patchObject, {
			validateStatus: e => [200, 409].some(f => f === e)
		});

		if(res.status === 200){
			this.state.updateLoading(false);
			this.state.clearFields()
			const loggedInRes = await axios.get("/api/auth");

			if(loggedInRes.status === 200){
				const dataObj = loggedInRes.data.data;
				const loginObject : ILoginState = {
					username: dataObj.username as string,
					firstname: dataObj.firstname as string,
					lastname: dataObj.lastname as string,
					role: dataObj.role as string,
					uuid: dataObj.uuid as string
				};
				this.props.loginState.updateByStatusCall(loginObject);
			}else{
				this.props.loginState.updateByStatusCall(false);
			}
			
		}
		else{
			this.state.updateResponseErrorMessage(res.data?.reason ?? "No Reason given.");
			this.state.updateLoading(false);
		}
	}

	renderInputs(){
		return <>
			<Fade in={this.state.username.length > 0}>
				<Badge className="text-primary">Changed</Badge>
			</Fade>
			<InputGroup className="mb-2">
				<InputGroup.Prepend><InputGroup.Text>Username</InputGroup.Text></InputGroup.Prepend>
				<FormControl disabled={this.state.isLoading} placeholder={this.props.loginState.username} value={this.state.username} onChange={e => this.state.updateUsername(e.target.value)} />
			</InputGroup>

			<Fade in={this.state.firstname.length > 0 || this.state.lastname.length > 0}>
				<Badge className="text-primary">Changed</Badge>
			</Fade>
			<InputGroup className="mb-2">
				<InputGroup.Prepend><InputGroup.Text>Name</InputGroup.Text></InputGroup.Prepend>
				<FormControl disabled={this.state.isLoading} placeholder={this.props.loginState.firstname} value={this.state.firstname} onChange={e => this.state.updateFirstname(e.target.value)} />
				<FormControl disabled={this.state.isLoading} placeholder={this.props.loginState.lastname} value={this.state.lastname} onChange={e => this.state.updateLastname(e.target.value)} />
			</InputGroup>
			<Fade in={this.state.newPassword.length > 0 || this.state.oldPassword.length > 0}>
				<Badge className="text-primary">Changed</Badge>
			</Fade>
			<InputGroup>
				<InputGroup.Prepend><InputGroup.Text>Password</InputGroup.Text></InputGroup.Prepend>
				<FormControl disabled={this.state.isLoading} placeholder="Old Password" type="password" value={this.state.oldPassword} onChange={e => this.state.updateOldassword(e.target.value)} />
				<FormControl disabled={this.state.isLoading} placeholder="New Password" type="password" value={this.state.newPassword} onChange={e => this.state.updateNewPassword(e.target.value)} />
			</InputGroup>
		</>
	}

	render(){
		return <>
			<BetterBreadcrumbs activeNode={this.props.loginState.username ?? "self"} previousRoute={[{name: "Dashboard", link:"/"}, {name:"Users", link:"/users"}]}/>
			<Container className="mt-3">
				<h1>Hello, {this.props.loginState.firstname}</h1>
				<hr/>
				{
					this.state.errorMessages.map((e,i) => <Alert key={i} variant="danger">{e}</Alert>)
				}
				<Collapse in={typeof this.state.responseError === "string"}>
					<div>
						<Alert variant="danger">{this.state.responseError}</Alert>
					</div>
				</Collapse>
				{this.renderInputs()}
				<Button onClick={e => this.onCommit()} className="mt-3" disabled={!this.state.canCommit} variant="primary">
					Commit
				</Button>
			</Container>
		</>
	}
}

export default observer(UserSelf);