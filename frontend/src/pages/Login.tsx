import React from "react";
import Card from "react-bootstrap/Card"
import Container from "react-bootstrap/Container"
import Row from "react-bootstrap/Row"
import Col from "react-bootstrap/Col"
import Button from "react-bootstrap/Button"
import Form from "react-bootstrap/Form";
import FormControl from 'react-bootstrap/FormControl'
import Alert from "react-bootstrap/Alert"
import Spinner from "react-bootstrap/Spinner"

import { observer } from "mobx-react";
import LoginPageState from "../store/LoginPageState";
import axios from "axios";
import LoginState, { ILoginState } from "../store/LoginState";
import Collapse from "react-bootstrap/Collapse";
import { RouteComponentProps, withRouter } from "react-router-dom";

export interface ILoginPageProps{
	loginState: LoginState
}

class Login extends React.Component<ILoginPageProps & RouteComponentProps, LoginPageState>{

	constructor(props: ILoginPageProps & RouteComponentProps){
		super(props);
		this.state = new LoginPageState();
	}

	private sleep(time: number){
		return new Promise(e => setTimeout(e,time))
	}

	private forgotPasswordFragment(show: boolean) : JSX.Element {

		return <Collapse in={show}>
			<div>
				<p className="py-5">
					Please contact your System Administrator. 
					They will generate you a new password, 
					from which you can change to your own once you have gained access.
				</p>
			</div>
		</Collapse>
	}

	private gainAccessFragment(show: boolean) : JSX.Element {
		return <Collapse in={show}>
			<div>
				<p className="py-5">
					Please contact your System Administrator.
					They will create an account for you.
				</p>
			</div>
		</Collapse>
	}

	private alertFragment(message: string | undefined, show: boolean): JSX.Element{
		return(
		<Collapse in={show}>
			<div>
				<Alert variant="danger">
					{message || "Could not Login."}
				</Alert>
			</div>
		</Collapse>
		)
	}

	private formFragment(isShown: boolean): JSX.Element{
		return (
			<Collapse in={isShown}>
				<div>
					<Form className="mb-1">
						<Form.Group controlId="loginUsername">
							<Form.Label>Username</Form.Label>
							<Form.Control 
								isInvalid={!this.state.isUsernameValid}
								value={this.state.userName}
								onChange={(e) => {this.state.updateUsername(e.target.value)}} 
								type="text" 
								placeholder="Enter Username"/>
							{ !this.state.isUsernameValid && 
								<FormControl.Feedback type="invalid">
									Username is at least 5 characters long.
								</FormControl.Feedback>
							}

						</Form.Group>
						<Form.Group controlId="loginPassword">
							<Form.Label>Password</Form.Label>
							<Form.Control 
								isInvalid={!this.state.isPasswordValid}
								value={this.state.password}
								type="password" 
								onChange={(e) => {this.state.updatePassword(e.target.value)}} 
								placeholder="Enter Password"/>
							{ !this.state.isPasswordValid && <FormControl.Feedback type="invalid">Password is at least 8 characters long.</FormControl.Feedback>}
						</Form.Group>

						{this.alertFragment(this.state.lastTryFailedMessage,this.state.lastTryFailed)}
						
						<Button onClick={this.tryLogin.bind(this)} disabled={!this.state.isPasswordValid || !this.state.isUsernameValid || this.state.isLoading}>
							{this.state.isLoading ? (<Spinner as="span" animation="grow" size="sm" role="status"></Spinner>) : "Log in"}
						</Button>
					</Form>
				</div>
			</Collapse>
			
		)
	}

	private loggedInFragment(isOpen: boolean) : JSX.Element{
		
		return <Collapse in={isOpen}>
			<div>
				<Alert variant="success">
					<Spinner animation="grow" className="mr-3 align-middle"/> Logged in successfully...
				</Alert>
			</div>
		</Collapse>
		
	}

	public render(){
		return <Container className="" fluid>
			<Row className="justify-content-md-center mt-5">
				<Col sm="12" md="8" lg="6">
					<Card>
						<Card.Body>
							<Card.Title>Login</Card.Title>
							<Card.Text className="text-muted">Please Login using your credentials.</Card.Text>
							{this.formFragment.bind(this)(!this.props.loginState.isLoggedIn)}
							{this.loggedInFragment.bind(this)(this.props.loginState.isLoggedIn)}
							<hr/>
							<Card.Link onClick={()=>{this.state.clickLostPasswordBtn()}} href="#" className={this.state.openLostPassword ? "text-primary" : "text-dark"}>Forgot Password</Card.Link>
							<Card.Link onClick={()=>{this.state.clickGetAccessBtn()}} href="#" className={this.state.openGetAccess ? "text-primary" : "text-dark"}>Gain Access</Card.Link>
							<br/>
							{this.forgotPasswordFragment.bind(this)(this.state.openLostPassword)}
							{this.gainAccessFragment.bind(this)(this.state.openGetAccess)}
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	}

	private async tryLogin(){
		const userName = this.state.userName.trim();
		const password = this.state.password;
		this.state.startLoading();
		const response = await axios.post("/api/auth",{
			username: userName,
			password: password
		}, 
		{
			validateStatus: (status) => [200,404].some( e=> e === status)
		});
		if(response.status === 404){
			this.state.updateLastTryFailed(true, response.data.reason);
		}else if(response.status === 200){
			// 200 -> Success! Get new Login Data
			const dataObj = (await axios.get("/api/auth")).data.data;
			const loginObject : ILoginState = {
				username: dataObj.username as string,
				firstname: dataObj.firstname as string,
				lastname: dataObj.lastname as string,
				role: dataObj.role as string,
				uuid: dataObj.uuid as string
			};
			this.props.loginState.updateByStatusCall(loginObject);
			console.info(this.props.loginState);

			this.state.updateLastTryFailed(false);
			await this.sleep(1000);
			this.props.history.push("/")

		}
		this.state.stopLoading();
		console.warn(response)
	}



}

export default withRouter(observer(Login));