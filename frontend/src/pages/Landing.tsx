import React from "react"
import Container from "react-bootstrap/Container"
import Jumbotron from "react-bootstrap/Jumbotron"
import { Link } from "react-router-dom"
import CSS from "csstype"
import Card from "react-bootstrap/Card"

const styles : CSS.Properties = {
	background: "url('/img/warehouseStock.jpg')",
	minHeight: "90vh",
	backgroundPosition: "bottom",
	borderRadius: 0
};

class Landing extends React.Component {

	public render() : JSX.Element{
		return (
			<Jumbotron className="" style={styles}>
				<Container className="py-5" >
					<Card>
						<Card.Body>
							<Card.Title>Warehouse Management System</Card.Title>
							<Card.Text>
								This is the Webinterface to the Warehouse Management System.
								You need to <Link to="/login">log in</Link> to gain access.
							</Card.Text>
						</Card.Body>
					</Card>
				</Container>
			</Jumbotron>
		)
	}
}

export default Landing;