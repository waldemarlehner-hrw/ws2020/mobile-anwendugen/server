import React from "react"
import Container from "react-bootstrap/Container"
import Card from "react-bootstrap/Card"
import Row from "react-bootstrap/Row"
import Col from "react-bootstrap/Col"
import Badge from "react-bootstrap/Badge"
import { RouteComponentProps, withRouter } from "react-router-dom"
import "../styles/dashboardCardStyles.css"
import LoginState from "../store/LoginState"
import BetterBreadcrumbs from "../components/BetterBreadcrumbs"

type RoleEnum = "regular" | "manager" | "admin" | "root";

function getBadge(role: RoleEnum) : JSX.Element{
	const writtenNames: Map<RoleEnum, string> = new Map([
		["root", "Root"],
		["admin", "Administrator"],
		["manager", "Manager"],
		["regular", "User"]
	])

	return (
		<Badge variant="primary">{writtenNames.get(role)}</Badge>
	)
}

const entries : IDashboardEntries[] = [
	{
		categoryName: "Manage Users",
		description: "Create, Delete, Modify and Query Users",
		color: 0x30398c,
		route: "/users"
	},
	{
		categoryName: "Manage Shelves",
		description: "Modify, create and query Shelves",
		color: 0x115A4E,
		route: "/shelves"
	},
	{
		categoryName: "Manage Itemtypes",
		description: "Create, Delete, Modify and Query Itemtypes",
		color: 0x30938c,
		route: "/types"
	},
	{
		categoryName: "Print Queue",
		description: "Queue for QR Codes to print.",
		color: 0x8c9330,
		route: "/printqueue"
	}
]


class Dashboard extends React.Component<RouteComponentProps & {loginState: LoginState}>{

	public render() : JSX.Element {
		return (<>
			<BetterBreadcrumbs activeNode="Dashboard" previousRoute={[]} />
			<Container>
				<Row className="my-5 mx-1">
					<h2>Hello, {this.props.loginState.firstname} {getBadge(this.props.loginState.role as RoleEnum)}</h2>
				</Row>
				<hr/>
				<Row>
					{
						entries.map((e, index) => {
							return (
								<Col key={index} sm={12} md={6} lg={4} xl={3} className="py-3 px-3" style={{display: "flex", flexDirection: "row"}}>
									<Card 
										className="text-white carddashboard" 
										style={{backgroundColor: "#"+(e.color).toString(16)}}
										onClick={() => this.props.history.push(e.route)}
									>
										<Card.Body>
											<Card.Title >{e.categoryName}</Card.Title>
											<Card.Text>
												{e.description}
											</Card.Text>
										</Card.Body>
									</Card>
								</Col>
							)
						})
					}

				</Row>
				
			</Container>
		</>)
	}
}

interface IDashboardEntries{
	categoryName: string,
	description: string,
	color: number,
	route: string
}

export default withRouter(Dashboard)
