import React from "react"
import Container from "react-bootstrap/Container"
import Button from "react-bootstrap/Button"
import {Link} from "react-router-dom"

class NotFound extends React.Component{
	public render(){
		return (
			<Container style={{alignItems: "center", justifyContent: "center", display: "flex", minHeight: "90vh"}}>
				<div>
					<h1 className="text-primary text-center" style={{fontSize: 150, fontWeight: 800}}>404</h1>
					<h3>Whoops... seems like you're lost.</h3>
					<Link to="/" className="text-center" style={{justifyContent: "center", display: "flex"}}>
						<Button className="">Main Page</Button>
					</Link>
				</div>
			</Container>
		)
	}
}

export default NotFound;