
import axios from "axios";
import { observer } from "mobx-react";
import React from "react";
import Button from "react-bootstrap/esm/Button";
import Card from "react-bootstrap/esm/Card";
import Container from "react-bootstrap/esm/Container";
import Spinner from "react-bootstrap/esm/Spinner";
import { Link, RouteComponentProps, withRouter } from "react-router-dom";

import BetterBreadcrumbs from "../../components/BetterBreadcrumbs";
import CreateItemModal from "../../components/CreateItemModal";
import Table from "react-bootstrap/esm/Table";
import { ItemsByItemTypeState } from "../../store/ItemsByItemTypeState";

export interface IItem{
    uuid: string,
    status: string,
    notes?: string,
    shelf?: {
        uuid: string,
        name: string
    }
}

class ItemsByItemType extends React.Component<{type:string} & RouteComponentProps, ItemsByItemTypeState>{

    constructor(props: any){
        super(props);
        this.state = new ItemsByItemTypeState();
    }

    async fetchData(){
        this.state.updateIsLoading(true);
        const response = await axios.get("/api/itemtypes/"+this.props.type,{
            params: {
                includeItems: true,
            },
            validateStatus(status){
                return [200,400,404].some(e => e === status)
            }
        })
        if(response.status === 200){
            this.state.updateData(response.data.data);
        }else{
            this.state.updateNotFound(true);
        }
        this.state.updateIsLoading(false)
    }

    async componentDidMount(){
        this.fetchData()
    }

    private renderBreadCrumb(){
        return <BetterBreadcrumbs activeNode={this.props.type} previousRoute={[{name:"Dashboard", link:"/"}, {name: "Item Types", link:"/types"}]}>
            <Link to={`/types/${this.props.type}/edit`}>
                <Button variant="outline-primary" style={{position: "absolute", right: "1em", top: "50%", transform: "translateY(-50%)"}}>
                    Edit
                </Button>
            </Link>
        </BetterBreadcrumbs>
        
    }

    private renderEntries(entries: IItem[]){
        return <Table striped responsive hover bordered>
            <thead>
                <tr>
                    <td>ID Slug</td>
                    <td>Location</td>
                    <td>Status</td>
                    <td>Notes</td>
                </tr>
            </thead>
            <tbody>
            {
                entries.map((e) => {
                    return <tr key={e.uuid}> 
                        <td style={{cursor: "pointer"}} onClick={()=>this.props.history.push("/items/"+e.uuid)}> {e.uuid.slice(0,6)}</td>
                        <td style={{cursor: (e.shelf?.uuid) ? "pointer" : "default"}} onClick={()=>{if(e.shelf?.uuid){this.props.history.push("/shelves/"+e.shelf.uuid)}}}>{e.shelf?.name}</td>
                        <td>{e.status}</td>
                        <td>{e.notes}</td>
                    </tr>
                })
            }

            </tbody>

        </Table>
    }

    private renderBody(){
        if(this.state.isLoading){
            return <Container className="mt-5 d-flex justify-content-center">
                <Spinner variant="primary" animation="grow"/>
            </Container>
        }if(this.state.notFound){
            return <Container className="mt-5 ">
            <h1>Item Type does not exist</h1>
            <p> No Type with the given ID exists</p>
            <Link to={"/types"}><Button>Back to all Types</Button></Link>
        </Container>
        }
        return <Container>
            <Card className="my-4">
                <Card.Body>
                    <Card.Title>{this.state.itemType?.name}</Card.Title>
                    <Card.Text>{this.state.itemType?.description}</Card.Text>
                    <Button onClick={()=>this.state.updateShowCreateModal(true)} className="" variant="outline-success">
                        Add Item
                    </Button>
                </Card.Body>
            </Card>
            {this.state.entries.length === 0 ? 
                <p className="text-muted">There are no Items of this Type. Use the Button above to add some.</p> :
                this.renderEntries(this.state.entries)
            }
        </Container>
    }

    render(){
        return <>
            {this.renderBreadCrumb()}
            {this.renderBody()}
            <CreateItemModal typeUuid={this.props.type} parent={this.state} />
        </>
    }
}

export default withRouter(observer(ItemsByItemType));