import { observer } from "mobx-react";
import React from "react";
import Button from "react-bootstrap/Button";
import FormControl from "react-bootstrap/FormControl";
import InputGroup from "react-bootstrap/InputGroup";
import Modal from "react-bootstrap/Modal";
import { ItemsByItemTypeState } from "../../store/ItemsByItemTypeState";
import Autocomplete from "react-autocomplete"
import { ItemsByItemTypeCreateModalState } from "../../store/ItemsByItemTypeCreateModalState";

class ItemsByItemtypeCreateModal extends React.Component<{parent: ItemsByItemTypeState}, ItemsByItemTypeCreateModalState>{
    constructor(props: {parent: ItemsByItemTypeState}){
        super(props);
        this.state = new ItemsByItemTypeCreateModalState()
    }

    private timer? : NodeJS.Timer;
	private debounceAndQuery(){
		if(this.timer){
			clearTimeout(this.timer)
		}
		this.timer = setTimeout(()=>{this.fetchAutocomplete()}, 500);
    }
    
    private async fetchAutocomplete(){
    }

    render(){
        return <Modal.Dialog>
            <Modal.Header> Create new Item</Modal.Header>
            <Modal.Body>
                <InputGroup className="mb-1">
                    <InputGroup.Prepend><InputGroup.Text>Notes</InputGroup.Text></InputGroup.Prepend>
                    <FormControl placeholder="Add optional notes..." />
                </InputGroup>
                
                <Autocomplete
                    value={this.state.shelveName}
                    items={this.state.autoCompleteData}
                    getItemValue={e => e[0]}
                    onSelect={(e,item) => {this.state.updateSelectedShelf(item)}}
                    onChange={e => {this.state.updateShelveName(e.target.value); this.debounceAndQuery();}}
                    renderMenu={chidren => <div className="menu">{chidren[0]}</div>}
                    renderItem={(item,highlighted) => {
                        return <div className={highlighted ? "text-primary":"text-muted"} key={item[1]}>
                            {item[0]}
                        </div>
                    }}
                />

            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary">Cancel</Button>
                <Button variant="primary">Create</Button>
            </Modal.Footer>
        </Modal.Dialog>
    }
}

export default observer(ItemsByItemtypeCreateModal);