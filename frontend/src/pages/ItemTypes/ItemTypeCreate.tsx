import axios from "axios";
import { observer } from "mobx-react";
import React from "react"
import Alert from "react-bootstrap/Alert";
import Button from "react-bootstrap/Button";
import Container from "react-bootstrap/Container";
import FormControl from "react-bootstrap/FormControl";
import InputGroup from "react-bootstrap/InputGroup";
import { RouteComponentProps, withRouter } from "react-router-dom";
import BetterBreadcrumbs from "../../components/BetterBreadcrumbs";
import { ItemTypeCreateState } from "../../store/ItemTypeCreateState";

class ItemTypeCreate extends React.Component<{} & RouteComponentProps, ItemTypeCreateState>{

	constructor(props: {} & RouteComponentProps){
		super(props);
		this.state = new ItemTypeCreateState();
	}

	async tryCreate(){
		this.state.updateIsLoading(true);
		const response = await axios.post("/api/itemtypes", this.state.postBody, {
			validateStatus(status){ return [200,400].some(e => e === status)}
		});
		if(response.status === 400){
			const responseString = response.data.reason + (response.data.data as string[]).join("\n")
			this.state.updateResponseError(responseString)
		}else{
			this.state.reset();
			this.state.updateShowSuccess(true);
		}
		this.state.updateIsLoading(false);
	}

	render(){
		return <>
			<BetterBreadcrumbs activeNode="New" previousRoute={[{name: "Dashboard", link:"/"}, {name:"Item Types", link:"/types"}]}/>
			<Container className="mt-3">
				<h1>Add Item Type</h1>
				<hr/>
				{this.state.showSuccess && <Alert onClose={()=>{this.state.updateShowSuccess(false)}} dismissible variant="success">Added Item Type</Alert>}
				{this.state.responseError && <Alert onClose={()=>{this.state.updateResponseError()}} dismissible variant="danger">Cound not complete creation: {this.state.responseError}</Alert>}
				{this.state.errorsWithInput.map((e,i) => {
					return <Alert key={i}  variant="warning">{e}</Alert>
				})}
				<InputGroup className="mb-2">
					<InputGroup.Prepend><InputGroup.Text>Name and Capacity</InputGroup.Text></InputGroup.Prepend>
					<FormControl disabled={this.state.isLoading} placeholder="Enter Type Name" value={this.state.name} onChange={e => this.state.updateName(e.target.value)} />
					<FormControl min={0} type="number" disabled={this.state.isLoading} placeholder="Enter required capacity" value={this.state.capacity} onChange={e => this.state.updateCapacity(e.target.value)} />
				</InputGroup>

				<InputGroup>
					<InputGroup.Prepend>
						<InputGroup.Text>Description</InputGroup.Text>
					</InputGroup.Prepend>
					<FormControl disabled={this.state.isLoading} as="textarea" value={this.state.description} onChange={e => this.state.updateDescription(e.target.value)} />
				</InputGroup>
				
				<Button onClick={()=>this.tryCreate()} variant="primary" className="my-3" disabled={!this.state.canCreate}>
					Create
				</Button>
			</Container>
		</>
	}

	componentDidMount(){
		this.state.updateIsLoading(false);
	}
}

export default withRouter(observer(ItemTypeCreate))