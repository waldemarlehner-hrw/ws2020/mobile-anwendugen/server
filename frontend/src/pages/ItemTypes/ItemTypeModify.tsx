import axios from "axios";
import { observer } from "mobx-react";
import React from "react";
import Alert from "react-bootstrap/Alert";
import Button from "react-bootstrap/Button";
import ButtonGroup from "react-bootstrap/ButtonGroup";
import Collapse from "react-bootstrap/Collapse";
import Container from "react-bootstrap/Container";
import FormControl from "react-bootstrap/FormControl";
import InputGroup from "react-bootstrap/InputGroup";
import Modal from "react-bootstrap/Modal";
import Row from "react-bootstrap/Row";
import Spinner from "react-bootstrap/Spinner";
import BetterBreadcrumbs from "../../components/BetterBreadcrumbs";
import ModifiedBadge from "../../components/ModifiedBadge";
import { ItemTypeModifyState } from "../../store/ItemTypeModifyState";

export interface ICurrentState{
    name: string,
    description: string,
    capacity: string
}
export interface IBackendState{
    name: string,
    description: string,
    capacity: string,
    orderCount: number,
    itemCount: number
}


class ItemTypeModify extends React.Component<{uuid:string}, ItemTypeModifyState>{

    constructor(props: {uuid:string}){
        super(props);
        this.state = new ItemTypeModifyState();
    }

    private renderWarningsAndErrors(){
        return <>
            <Collapse in={this.state.anyValueChanged}>
                <div>
                    <Alert variant="info">
                        You have unsaved changes.
                    </Alert>
                </div>
            </Collapse>
            <Collapse in={this.state.showSuccess}>
                <div>
                    <Alert dismissible variant="success" onClose={()=>this.state.updateShowSuccess(false)}>
                        Modified successfully.
                    </Alert>
                </div>
            </Collapse>
            <Collapse in={this.state.errorsWithInput.length > 0}>
                <div>
                    {this.state.errorsWithInput.map((e: string,i) => {
                        return <Alert key={i} variant="warning">{e}</Alert>
                    })}
                </div>
            </Collapse>
        </>
    }

    private renderForm() : JSX.Element{
        return <>
            {this.renderWarningsAndErrors()}

            <ModifiedBadge show={this.state.changedValues.name}/>
            <InputGroup>
                <InputGroup.Prepend><InputGroup.Text>Name</InputGroup.Text></InputGroup.Prepend>
                <FormControl
                    value={this.state.currentState.name}
                    onChange={e => this.state.updateName(e.target.value)}
                    placeholder={this.state.backendState?.name ?? "..."}
                />
            </InputGroup>
            <footer className="text-muted mb-1 text-subtext">Name needs to be at least 5 Characters long</footer>

            <ModifiedBadge show={this.state.changedValues.description}/>
            <InputGroup className="mt-1">
                <InputGroup.Prepend><InputGroup.Text>Description</InputGroup.Text></InputGroup.Prepend>
                <FormControl 
                    as="textarea" 
                    value={this.state.currentState.description} 
                    onChange={e => this.state.updateDescription(e.target.value)} 
                    placeholder={this.state.backendState?.description ?? "..."}    
                />
            </InputGroup>

            <ModifiedBadge show={this.state.changedValues.capacity}/>
            <InputGroup>
                <InputGroup.Prepend><InputGroup.Text>Capacity</InputGroup.Text></InputGroup.Prepend>
                <FormControl 
                    type="number"
                    value={this.state.currentState.capacity} 
                    onChange={e => this.state.updateCapacity(e.target.value)} 
                    placeholder={this.state.backendState?.capacity ?? "..."}    
                />
            </InputGroup>
            <footer className="text-muted mb-5 text-subtext">Capacity needs to be a positive numeric value.</footer>
            <ButtonGroup>
                <Button variant="primary" disabled={!this.state.canUpdate} onClick={() => this.patchAndFetch()}>Update</Button>
                <Button variant="outline-danger" onClick={()=>this.state.updateShowDeleteAlert(true)}>Delete</Button>
            </ButtonGroup>

        </>
    }

    private renderBody() : JSX.Element{
        if(this.state.isLoading){
            return <div className="d-flex justify-content-center">
                <Spinner className="mt-5" animation="grow" variant="primary" />
            </div>
        }
        return this.renderForm();
    }

    render() : JSX.Element{
        return <>
            <BetterBreadcrumbs activeNode="edit" previousRoute={
                [{name: "Dashboard", link:"/"}, {name:"Item Types", link:"/types"}, {name:this.props.uuid, link:"/types/"+this.props.uuid}]
            }/>

            <Container className="mt-5">
                <Row>
                    <h1 className="mx-3">Modify Item Type</h1>
                </Row>
                <hr/>

                { this.state.showNotExists ? 
                <Alert variant="danger">Could not find an Item Type with the ID {this.props.uuid}</Alert>:
                this.renderBody()
                }
            </Container>
            <Modal show={this.state.showDeleteAlert} onHide={()=>this.state.updateShowDeleteAlert(false)}>
                <Modal.Header closeButton>{ this.state.canDelete ? <> Are you sure you want to delete<span className="mx-1 text-primary"> {this.state.backendState?.name} </span></> : "Cannot delete because this Item Type is in use."}</Modal.Header>
                
                {this.state.canDelete ? 
                    <Modal.Body> The item type will no longer be available.</Modal.Body>:
                    <Modal.Body> Used by {this.state.backendState?.itemCount} Items and {this.state.backendState?.orderCount} Orders.</Modal.Body>
                }
				<Modal.Footer>
					<Button variant="danger" disabled={!this.state.canDelete} onClick={()=>this.delete()}>Delete Item Type</Button>
					<Button variant="link" onClick={()=>this.state.updateShowDeleteAlert(false)}>Cancel</Button>
				</Modal.Footer>
            </Modal>
        </>
    }

    private async patchAndFetch(){
        if(this.state.canUpdate){
            this.state.updateIsLoading(true)
            await axios.patch("/api/itemtypes/"+this.props.uuid, this.state.patchBody)
            await this.fetchNewData()
            this.state.updateShowSuccess(true)
        }
    }

    private async delete(){
        await axios.delete("/api/itemtypes/"+this.props.uuid);

    }

    private async fetchNewData(){
        const response = await axios.get("/api/itemtypes/"+this.props.uuid, {
            validateStatus(status){
                return [200,404,400].some(e => e === status);
            }
        })
        if(response.status === 404 || response.status === 400){
            this.state.updateShowNotExists(true)
        }else{
            const data = response.data.data;
            const newBackEndState : IBackendState = {
                name: data.name,
                description: data.description,
                capacity: data.capacity,
                itemCount: data.itemCount,
                orderCount: data.ordersCount
            };
            this.state.updateBackendState(newBackEndState);
        }
        this.state.updateIsLoading(false);
    }

    componentDidMount(){
        this.fetchNewData();
    }
}

export default observer(ItemTypeModify);