import axios from "axios";
import { observer } from "mobx-react";
import React from "react";
import Alert from "react-bootstrap/Alert";
import Badge from "react-bootstrap/Badge";
import Button from "react-bootstrap/Button";
import ButtonGroup from "react-bootstrap/ButtonGroup";
import Container from "react-bootstrap/Container";
import FormControl from "react-bootstrap/FormControl";
import InputGroup from "react-bootstrap/InputGroup";
import ListGroup from "react-bootstrap/ListGroup";
import Modal from "react-bootstrap/Modal";
import Spinner from "react-bootstrap/Spinner";
import { Link, RouteComponentProps, withRouter } from "react-router-dom";
import BetterBreadcrumbs from "../components/BetterBreadcrumbs";
import ModifiedBadge from "../components/ModifiedBadge";
import ItemState from "../store/ItemState";
import { ItemStateEntry } from "../store/ItemStateEntry";
import { PersistentPrintQueueState } from "../store/PersistentPrintQueueState";

export interface IItemProps{
    uuid: string,
    canDelete: boolean
}

class Item extends React.Component<IItemProps & RouteComponentProps, ItemState>{

    private timer? : NodeJS.Timer;
    private async debounceShelfSearch(){
        if(this.timer){
            clearTimeout(this.timer)
        }
        this.timer = setTimeout(() => { this.fetchAutofillShelves() }, 500)
    }

    private async fetchAutofillShelves(){
        const response = await axios.get("/api/shelves", {params:{nameLike: this.state.shelfNameSearch}, validateStatus(status){return status === 200 || status === 404}})
        if(response.status === 200){
            const data = (response.data.data as any[]).map(e => {
                return {
                    name: e.name,
                    uuid: e.uuid
                }
            })
            this.state.updateShelvesAutofill(data)
        }else{
            this.state.updateShelvesAutofill([])
        }
    }

    constructor(props : IItemProps & RouteComponentProps){
        super(props);
        this.state = new ItemState()
    }

    private async commit(){
        this.state.updateIsLoading(true)
        const patchResponse = await axios.patch("/api/items/"+this.props.uuid,(this.state.deltaState as ItemStateEntry).patchBody,{validateStatus(status){return [200,400,404].some(e => e === status)}})
        if(patchResponse.status === 200){
            this.state.updateShowSuccess(true);
            this.updateState()
        }else{
            this.state.updateIsLoading(false)
        }
    }

    private renderBody(){
        if(this.state.notFound){
            return <>
                <h1>Item does not exist</h1>
                <p>No Item with the given ID exists</p>
                <Link to="/"><Button>Back to Dashboard</Button></Link>
            </>
        }
        return <>
            <h1>{this.state.backendState?.type.name}</h1>
            <Badge variant="secondary">{this.props.uuid}</Badge> 
            <hr/>
            <ModifiedBadge show={this.state.deltaState?.changed.notes ?? false} />
            <InputGroup className="">
                <InputGroup.Prepend>
                    <InputGroup.Text>Notes</InputGroup.Text>
                </InputGroup.Prepend>
                <FormControl
                    as="textarea"
                    value={this.state.deltaState?.notes ?? ""}
                    onChange={e => (this.state.deltaState as ItemStateEntry).updateNotes(e.target.value)}
                />
                <InputGroup.Append>
                    <Button onClick={()=>(this.state.deltaState as ItemStateEntry).resetNotes()} disabled={!(this.state.deltaState as ItemStateEntry).changed.notes}>Reset</Button>
                </InputGroup.Append>
            </InputGroup>
            {
                (this.state.deltaState as ItemStateEntry).changed.notes && <p className={(this.state.deltaState?.notes?.trim().length ?? 255) > 255  ? "text-danger" : "text-muted"}>{this.state.deltaState?.notes?.trim().length} / 255</p>
            }
            <span className="mb-2" />
            <ModifiedBadge show={(this.state.deltaState as ItemStateEntry).changed.status} />
            <InputGroup className="mb-2">
                <InputGroup.Prepend>
                    <InputGroup.Text>Status</InputGroup.Text>
                </InputGroup.Prepend>
                <FormControl as="select" value={(this.state.deltaState as ItemStateEntry).status} onChange={e => (this.state.deltaState as ItemStateEntry).updateStatus(e.target.value)}>
                    <option value="default">Default</option>
                    <option value="outofsystem">Out of System</option>
                    <option value="lost">Lost</option>
                    <option value="reserved">Reserved</option>
                </FormControl>
                <InputGroup.Append>
                    <Button onClick={()=>(this.state.deltaState as ItemStateEntry).resetStatus()} disabled={!(this.state.deltaState as ItemStateEntry).changed.status}>Reset</Button>
                </InputGroup.Append>
            </InputGroup>
            <ModifiedBadge show={(this.state.deltaState as ItemStateEntry).changed.shelf} />
            <InputGroup>
                <InputGroup.Prepend><InputGroup.Text>Shelf</InputGroup.Text></InputGroup.Prepend>
                <FormControl value={this.state.deltaState?.shelf?.name ?? ""} disabled/>
                <InputGroup.Append>
                    <Button variant="outline-primary" onClick={() =>{this.state.updateShowUpdateShelfModal(true); this.fetchAutofillShelves()}}>Update</Button>
                    <Button disabled={!(this.state.deltaState as ItemStateEntry).changed.shelf} onClick={()=>{(this.state.deltaState as ItemStateEntry).resetShelf()}}>Reset</Button>
                </InputGroup.Append>
            </InputGroup>
            <ButtonGroup className="mt-3">
                <Button disabled={!((this.state.deltaState as ItemStateEntry).anyChanged && this.state.canCommit)} onClick={()=>this.commit()}>Commit</Button>
                <Button variant="secondary" onClick={()=>{this.handleOnPrintQueueClick();}}>{(this.state.isInPrintQueue)?"Remove from Print Queue":"Add to Print Queue"}</Button> 
                <Button disabled={!(this.state.deltaState as ItemStateEntry).anyChanged} onClick={() => (this.state.deltaState as ItemStateEntry).resetAll()} variant="outline-danger">Reset All</Button>
            </ButtonGroup>
            {this.renderModals()}
        </>
    }

    private handleOnPrintQueueClick() {

        if(this.state.isInPrintQueue){
            let i = 0;
            let index = -1;
            const queue = PersistentPrintQueueState.instance.all;
            for(i; i < queue.length; i++){
                if(queue[i].uuid === this.props.uuid){
                    index = i;
                    break;
                }
            }
            PersistentPrintQueueState.instance.removeEntry(index);
            this.state.updateIsInPrintQueue(false);
        }else{
            PersistentPrintQueueState.instance.addEntry({
                type: "item",
                uuid: this.props.uuid,
                selected: true
            })
            this.state.updateIsInPrintQueue(true)
        }

    }

    private async delete(){
        const deleteRequest = await axios.delete("/api/items/"+this.props.uuid);
        if(deleteRequest.status === 200){
            //success
            this.props.history.push("/types/"+this.state.backendState?.type.uuid)
        }
    }


    private renderModals(){
        return <>
            <Modal backdrop="static" show={this.state.showDeleteModal} onHide={()=>this.state.updateShowDeleteModal(false)}>
                <Modal.Header closeButton>Delete Item</Modal.Header>
                <Modal.Body>
                    {!this.state.canDelete ? 
                        <p>Cannot delete the item because it has a special status. Please change the status to "default" first and commit.</p> :
                        <p>Are you sure you want to delete this Item?</p>
                    }
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={()=>this.state.updateShowDeleteModal(false)}>Close</Button>
                    {this.state.canDelete && <Button variant="danger" onClick={()=>this.delete()}>Delete</Button>}
                </Modal.Footer>
            </Modal>

            <Modal backdrop="static" show={this.state.showUpdateShelfModal} onHide={()=>this.state.updateShowUpdateShelfModal(false)}>
                <Modal.Header closeButton>Update Shelf</Modal.Header>
                <Modal.Body>
                    <p className="mb-2">Currently Selected: {this.state.deltaState?.shelf?.name ?? "None selected"}</p>
                    <InputGroup className="mb-2">
                        <InputGroup.Prepend>
                            <InputGroup.Text>Find by Name</InputGroup.Text>
                        </InputGroup.Prepend>
                        <FormControl value={this.state.shelfNameSearch} onChange={e => {this.state.updateShelfNameSearch(e.target.value); this.debounceShelfSearch();}} />
                    </InputGroup>
                    <ListGroup>
                        {this.state.shelvesAutofill.slice(0,10).map(e => {
                            return <ListGroup.Item style={{cursor: "pointer"}} active={this.state.deltaState?.shelf?.uuid === e.uuid}  onClick={() => (this.state.deltaState as ItemStateEntry).updateSelectedShelf(e.name, e.uuid)} key={e.uuid}>{e.name}</ListGroup.Item>
                        })}
                    </ListGroup>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" disabled={typeof this.state.deltaState?.shelf === "undefined"} onClick={() => (this.state.deltaState as ItemStateEntry).removeShelf()}>Deselect</Button>
                    <Button variant="primary" onClick={()=>this.state.updateShowUpdateShelfModal(false)}>Close</Button>
                </Modal.Footer>
            </Modal>
        </>
    }

    private async updateState(){
        this.state.updateIsLoading(true)
        const response = await axios.get("/api/items/"+this.props.uuid, {validateStatus(status){return [200,400,404].some(e => e === status)}});
        if(response.status === 200){
            console.info(response.data.data)
            this.state.updateState(response.data.data)
        }else if(response.status === 400 || response.status === 404){
            this.state.updateNotFound(true);
        }
        this.state.updateIsLoading(false)
    }

    componentDidMount(){
        this.updateState()
        if(PersistentPrintQueueState.instance.all.some(e => e.uuid === this.props.uuid)){
            this.state.updateIsInPrintQueue(true)
        }
    }

    render(){
        return <>
            <BetterBreadcrumbs activeNode={this.props.uuid} previousRoute={[{name:"Dashboard",link:"/"}, {name:"Item Types",link:"/types"}, {name:this.state.backendState?.type.name ?? "...", link: "/types/"+this.state.backendState?.type.uuid}]}>
                {
                this.props.canDelete && <Button variant="danger" onClick={()=>this.state.updateShowDeleteModal(true)} style={{position: "absolute", right: "1em", top: "50%", transform: "translateY(-50%)"}}>Delete</Button>
                }               
            </BetterBreadcrumbs>
            <Container className="mt-3">
                {this.state.showSuccess && <Alert variant="success" dismissible onClose={()=>this.state.updateShowSuccess(false)}>Updated successfully</Alert>}
                {this.state.isLoading ? <div className="d-flex flex-row justify-content-center mt-5"><Spinner animation="grow" variant="primary" /></div> : 
                this.renderBody()}
            </Container>
        </>
    }
}

export default withRouter(observer(Item));