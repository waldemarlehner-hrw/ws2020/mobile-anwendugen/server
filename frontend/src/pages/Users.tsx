import React from "react"
import Container from "react-bootstrap/Container";
import UserSearchResult from "../components/UserSearchResult";
import Searchbar from "../components/UsersSearchbar";
import UserPageState from "../store/UserPageState";
import { Link } from "react-router-dom";
import { observer } from "mobx-react";
import Button from "react-bootstrap/Button";
import BetterBreadcrumbs from "../components/BetterBreadcrumbs";

class Users extends React.Component<any,UserPageState>{

	constructor(props: any){
		super(props);
		this.state = (new UserPageState())
	}

	render(){
		return (<>
			<BetterBreadcrumbs activeNode="Users" previousRoute={[{link:"/", name:"Dashboard"}]}>
				<Link to="/users/new"><Button variant="outline-primary" style={{position: "absolute", right: "1em", top: "50%", transform: "translateY(-50%)"}}>Create New User</Button></Link>
			</BetterBreadcrumbs>
			<Container className="mt-4 d-flex flex-column">
				<Searchbar parent={this.state} />
				<hr className="w-100"/>
				<UserSearchResult loading={this.state.isLoading} data={this.state.result} />
			</Container>
		</>)
	}
}
export default observer(Users);