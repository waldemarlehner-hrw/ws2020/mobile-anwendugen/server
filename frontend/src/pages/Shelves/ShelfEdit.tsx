import axios from "axios";
import { observer } from "mobx-react";
import React from "react";
import Alert from "react-bootstrap/Alert";
import Button from "react-bootstrap/Button";
import ButtonGroup from "react-bootstrap/ButtonGroup";
import Collapse from "react-bootstrap/Collapse";
import Container from "react-bootstrap/Container";
import FormControl from "react-bootstrap/FormControl";
import InputGroup from "react-bootstrap/InputGroup";
import Modal from "react-bootstrap/Modal";
import Row from "react-bootstrap/Row";
import Spinner from "react-bootstrap/Spinner";
import { RouteComponentProps, withRouter } from "react-router-dom";
import BetterBreadcrumbs from "../../components/BetterBreadcrumbs";
import ModifiedBadge from "../../components/ModifiedBadge";
import ShelfEditState from "../../store/ShelfEditState";

class ShelfEdit extends React.Component<{uuid:string} & RouteComponentProps, ShelfEditState>{

    constructor(props: {uuid:string} & RouteComponentProps){
        super(props);
        this.state = new ShelfEditState();
    }

    private renderWarningsAndErrors(){
        return <>
            <Collapse in={this.state.anyValueChanged}>
                <div>
                    <Alert variant="info">
                        You have unsaved changes.
                    </Alert>
                </div>
            </Collapse>
            <Collapse in={this.state.showSuccess}>
                <div>
                    <Alert dismissible variant="success" onClose={()=>this.state.updateShowSuccess(false)}>
                        Modified successfully.
                    </Alert>
                </div>
            </Collapse>
            <Collapse in={this.state.errorsWithInput.length > 0}>
                <div>
                    {this.state.errorsWithInput.map((e: string,i) => {
                        return <Alert key={i} variant="warning">{e}</Alert>
                    })}
                </div>
            </Collapse>
        </>
    }

    private renderForm() : JSX.Element{
        return <>
            {this.renderWarningsAndErrors()}

            <ModifiedBadge show={this.state.changedValues.name}/>
            <InputGroup>
                <InputGroup.Prepend><InputGroup.Text>Name</InputGroup.Text></InputGroup.Prepend>
                <FormControl
                    value={this.state.newName}
                    onChange={e => this.state.updateName(e.target.value)}
                    placeholder={this.state.oldName ?? "..."}
                />
            </InputGroup>
            <footer className="text-muted mb-1 text-subtext">Name needs to be at least 5 Characters long</footer>

        

            <ModifiedBadge show={this.state.changedValues.capacity}/>
            <InputGroup>
                <InputGroup.Prepend><InputGroup.Text>Capacity</InputGroup.Text></InputGroup.Prepend>
                <FormControl 
                    type="number"
                    value={this.state.newCapacity} 
                    onChange={e => this.state.updateCapacity((e.target.value))} 
                    placeholder={this.state.oldCapacity ?? "..."}    
                />
            </InputGroup>
            <footer className="text-muted mb-5 text-subtext">Capacity needs to be a positive numeric value.</footer>
            <ButtonGroup>
                <Button variant="primary" disabled={!this.state.canUpdate} onClick={() => this.patchAndFetch()}>Update</Button>
                <Button variant="outline-danger" onClick={()=>this.state.updateShowDeleteModal(true)}>Delete</Button>
            </ButtonGroup>

        </>
    }

    private renderBody() : JSX.Element{
        if(this.state.isLoading){
            return <div className="d-flex justify-content-center">
                <Spinner className="mt-5" animation="grow" variant="primary" />
            </div>
        }
        return this.renderForm();
    }

    render() : JSX.Element{
        return <>
            <BetterBreadcrumbs activeNode="edit" previousRoute={
                [{name: "Dashboard", link:"/"}, {name:"Shelves", link:"/shelves"}, {name:this.props.uuid, link:"/shelves/"+this.props.uuid}]
            }/>

            <Container className="mt-5">
                <Row>
                    <h1 className="mx-3">Modify Shelf</h1>
                </Row>
                <hr/>

                { this.state.showNotExists ? 
                <Alert variant="danger">Could not find an Shelf with the ID {this.props.uuid}</Alert>:
                this.renderBody()
                }
            </Container>
            <Modal show={this.state.showDeleteModal} onHide={()=>this.state.updateShowDeleteModal(false)}>
                <Modal.Header closeButton>{ this.state.canDelete ? <> Are you sure you want to delete<span className="mx-1 text-primary"> {this.state.oldName} </span></> : "Cannot delete because this Item Type is in use."}</Modal.Header>
                
                {this.state.canDelete ? 
                    <Modal.Body> The shelf will no longer be available.</Modal.Body>:
                    <Modal.Body> Cannot delete because the shelf is not empty.</Modal.Body>
                }
				<Modal.Footer>
					<Button variant="danger" disabled={!this.state.canDelete} onClick={()=>this.delete()}>Delete Shelf</Button>
					<Button variant="link" onClick={()=>this.state.updateShowDeleteModal(false)}>Cancel</Button>
				</Modal.Footer>
            </Modal>
        </>
    }

    private async patchAndFetch(){
        if(this.state.canUpdate){
            this.state.updateIsLoading(true)
            await axios.patch("/api/shelves/"+this.props.uuid, this.state.patchBody)
            await this.fetchNewData()
            this.state.updateShowSuccess(true)
        }
    }

    private async delete(){
        await axios.delete("/api/shelves/"+this.props.uuid);
        this.props.history.push("/shelves")
    }

    private async fetchNewData(){
        const response = await axios.get("/api/shelves/"+this.props.uuid, {
            validateStatus(status){
                return [200,404,400].some(e => e === status);
            }
        })
        if(response.status === 404 || response.status === 400){
            this.state.updateShowNotExists(true)
        }else{
            const data = response.data.data;
            const newBackEndState  = {
                name: data.name,
                capacity: data.capacity,
                itemCount: data.itemCount,
            };
            this.state.updateFromBackend(
                newBackEndState.name, 
                newBackEndState.capacity, 
                newBackEndState.itemCount === 0
                );
        }
        this.state.updateIsLoading(false);
    }

    componentDidMount(){
        this.fetchNewData();
    }
}

export default withRouter(observer(ShelfEdit));