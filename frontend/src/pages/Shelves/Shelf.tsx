import axios from "axios";
import { observer } from "mobx-react";
import React from "react";
import { Button } from "react-bootstrap";
import Badge from "react-bootstrap/Badge";
import Container from "react-bootstrap/Container";
import ProgressBar from "react-bootstrap/ProgressBar";
import Spinner from "react-bootstrap/Spinner";
import Table from "react-bootstrap/Table";
import { Link, RouteComponentProps, withRouter } from "react-router-dom";
import BetterBreadcrumbs from "../../components/BetterBreadcrumbs";
import { PersistentPrintQueueState } from "../../store/PersistentPrintQueueState";
import { ShelfState } from "../../store/ShelfState";

export interface IItemFromShelf{
    uuid: string,
    notes?:string,
    status: string,
    itemtype: {
        uuid: number,
        name: string,
        description: string
    }
}

class Shelf extends React.Component<{uuid: string} & RouteComponentProps, ShelfState>{
    
    constructor(props: any){
        super(props);
        this.state = new ShelfState();
    }

    async componentDidMount(){
        const response = await axios.get("/api/shelves/"+this.props.uuid, {params: {withItems: true} ,validateStatus(){return true}})
        if(response.status === 200){
            this.state.updateFromAPIResponse(response.data.data)
        }else if(response.status === 404){
            this.state.updateDoesNotExist(true);
        }
        this.state.updateIsLoading(false);
        if(PersistentPrintQueueState.instance.all.some(e => e.uuid === this.props.uuid)){
            this.state.updateIsInPrintQueue(true)
        }
    }

    private isFull(e: {capacity: number, capacityUsed: number}){
        return e.capacityUsed > e.capacity
    }
    private percentage(e: {capacity: number, capacityUsed: number}){
        if(this.isFull(e)){
            return 100;
        }else{
            return 100 * (e.capacityUsed / e.capacity);
        }
    }
    private getVariant(e: {capacity: number, capacityUsed: number}){
        if(this.isFull(e)){
            return "danger";
        }
        if(this.percentage(e) > 80){
            return "warning";
        }
        return "success";
    }

    private renderTable(){
        return <Table striped bordered hover size="sm">
            <thead>
                <tr>
                    <th>Type</th>
                    <th>Notes</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                {this.state.items.map((e) => {
                    return <tr style={{cursor: "pointer"}} onClick={()=>this.props.history.push("/items/"+e.uuid)}>
                        <td>{e.itemtype.name}</td>
                        <td>{e.notes ?? ""}</td>
                        <td>{e.status}</td>
                    </tr>
                })}
            </tbody>
        </Table>
    }

    private handleOnPrintQueueClick() {

        if(this.state.isInPrintQueue){
            let i = 0;
            let index = -1;
            const queue = PersistentPrintQueueState.instance.all;
            for(i; i < queue.length; i++){
                if(queue[i].uuid === this.props.uuid){
                    index = i;
                    break;
                }
            }
            PersistentPrintQueueState.instance.removeEntry(index);
            this.state.updateIsInPrintQueue(false);
        }else{
            PersistentPrintQueueState.instance.addEntry({
                type: "shelf",
                uuid: this.props.uuid,
                selected: true
            })
            this.state.updateIsInPrintQueue(true)
        }

    }
    
    render(){
        return <>
            <BetterBreadcrumbs activeNode={this.props.uuid} previousRoute={[{name: "Dashboard", link:"/"}, {name: "Shelves", link:"/shelves"}]} >
                <Link to={`/shelves/${this.props.uuid}/edit`}>
                    <Button variant="outline-primary" style={{position: "absolute", right: "1em", top: "50%", transform: "translateY(-50%)"}}>
                        Edit
                    </Button>
                </Link>
            </BetterBreadcrumbs>
            <Container className="mt-3">
                {
                    this.state.isLoading ? 
                    <div className="d-flex flex-row justify-content-center">
                        <Spinner animation="grow" variant="primary"/>
                    </div>
                    : 
                    <>
                        <h1>{this.state.name}<span><Badge>{this.state.capacityUsed} / {this.state.capacity}</Badge></span></h1>
                        <p className="text-muted">{this.props.uuid}</p>
                        <ProgressBar 
                            variant={this.getVariant({capacity: this.state.capacity, capacityUsed: this.state.capacityUsed})} 
                            now={this.percentage({capacity: this.state.capacity, capacityUsed: this.state.capacityUsed})}
                        />
                        <Button variant="secondary" className="my-2" onClick={()=>{this.handleOnPrintQueueClick();}}>{(this.state.isInPrintQueue)?"Remove from Print Queue":"Add to Print Queue"}</Button> 
                        <hr/>
                        <p className="text-muted">{this.state.items.length} Items found.</p>
                        {
                            this.state.items.length > 0 && 
                            this.renderTable()
                        }
                        
                    </>
                }
            </Container>
        </>
    }
}

export default withRouter(observer(Shelf));