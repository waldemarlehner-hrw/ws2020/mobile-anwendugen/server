import axios from "axios";
import { observer } from "mobx-react";
import React from "react";
import Alert from "react-bootstrap/Alert";
import Button from "react-bootstrap/Button";
import Container from "react-bootstrap/Container";
import FormControl from "react-bootstrap/FormControl";
import InputGroup from "react-bootstrap/InputGroup";
import BetterBreadcrumbs from "../../components/BetterBreadcrumbs";
import { ShelfCreateState } from "../../store/ShelfCreateState";
class ShelfCreate extends React.Component<{}, ShelfCreateState>{

    constructor(props: any){
        super(props);
        this.state = new ShelfCreateState();
    }

    async tryCreate(){
        this.state.updateIsLoading(true)
        const response = await axios.post("/api/shelves", this.state.postBody, {validateStatus(status){return true;}});
        if(response.status === 200){
            this.state.updateShowSuccess(true);
            this.state.reset();
        }else{
            this.state.updateShowSuccess(false);
            this.state.updateResponseError(response.data.reason + (response.data.data as string[] | undefined)?.join(" ") ?? "")
        }
        this.state.updateIsLoading(false);
    }

    render() : JSX.Element {
        return <>
            <BetterBreadcrumbs activeNode="Create" previousRoute={[{name:"Dashboard", link:"/"}, {name:"Shelves", link:"/shelves"}]}/>
            <Container className="mt-3">
                <h1>Add Shelf</h1>
                <hr/>
                {this.state.showSuccess && <Alert onClose={()=>{this.state.updateShowSuccess(false)}} dismissible variant="success">Added Item Type</Alert>}
				{this.state.responseError && <Alert onClose={()=>{this.state.updateResponseError()}} dismissible variant="danger">Cound not complete creation: {this.state.responseError}</Alert>}
				{this.state.errorsWithInput.map((e,i) => {
					return <Alert key={i}  variant="warning">{e}</Alert>
                })}

                <InputGroup className="mb-2">
                    <InputGroup.Prepend>
                        <InputGroup.Text>
                            Name
                        </InputGroup.Text>
                    </InputGroup.Prepend>
                    <FormControl
                        disabled={this.state.isLoading} placeholder="Add Shelf Name"
                        value={this.state.shelfName} onChange={e => this.state.updateShelfName(e.target.value)}
                    />
                </InputGroup>

                <InputGroup className="mb-2">
                    <InputGroup.Prepend>
                        <InputGroup.Text>
                            Capacity
                        </InputGroup.Text>
                    </InputGroup.Prepend>
                    <FormControl
                        type="number" min={0}
                        disabled={this.state.isLoading} placeholder="Add Capacity of Shelf."
                        value={this.state.capacity} onChange={e => this.state.updateCapacity(e.target.value)}
                    />
                </InputGroup>

                <Button disabled={!this.state.canCreate} onClick={()=>this.tryCreate()} variant="primary">
                    Create
                </Button>

            </Container>

        </>
    }
}


export default observer(ShelfCreate);