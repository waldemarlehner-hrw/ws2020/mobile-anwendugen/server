import { action, makeObservable, observable } from "mobx";
import { observer } from "mobx-react";
import React from "react";
import Button from "react-bootstrap/Button";
import Container from "react-bootstrap/Container";
import { Link } from "react-router-dom";
import BetterBreadcrumbs from "../components/BetterBreadcrumbs";
import ShelvesResult from "../components/ShelvesResult";
import ShelvesSearchbar from "../components/ShelvesSearchbar";
import ShelvesSearchbarState from "../store/ShelvesSearchbarState";

export class ShelvesState{

    @observable data: IShelfEntry[] = [];
    @observable isLoading: boolean = true;
    @observable searchbarState: ShelvesSearchbarState;

    constructor(){
        this.searchbarState = new ShelvesSearchbarState();
        makeObservable(this);
    }
    @action updateIsLoading(bool: boolean){this.isLoading = bool;}
    @action updateData(data: any[]){
        this.data = data.map(e => {
            return {
                name: e.name,
                uuid: e.uuid,
                capacity: e.capacity,
                capacityUsed: e.capacityUsed,
                itemCount: e.itemCount
            }
        })
    }
}

export interface IShelfEntry{
    name: string,
    uuid: string,
    capacity: number,
    capacityUsed: number,
    itemCount: number
}

class Shelves extends React.Component<{canModify: boolean}, ShelvesState>{

    constructor(props: {canModify: boolean}){
        super(props);
        this.state = new ShelvesState();
    }

    private renderBreadcrumbs() : JSX.Element{
        return <BetterBreadcrumbs previousRoute={[{link:"/", name:"Dashboard"}]} activeNode="Shelves">
            {this.props.canModify && <Link to="/shelves/new"><Button style={{position: "absolute", right: "1em", top: "50%", transform: "translateY(-50%)"}}  variant="outline-primary">Add new Shelf</Button></Link>}
        </BetterBreadcrumbs>
    }

    render() : JSX.Element{
        return <>
            {this.renderBreadcrumbs()}
            <Container className="mt-4 d-flex flex-column">
                <ShelvesSearchbar parent={this.state} />
                <hr className="w-100"/>
                <ShelvesResult isLoading={this.state.isLoading} data={this.state.data} />
            </Container>
        </>
    }

}

export default observer(Shelves);