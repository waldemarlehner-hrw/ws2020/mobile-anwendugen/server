import { observer } from "mobx-react";
import React from "react";
import Button from "react-bootstrap/Button";
import Container from "react-bootstrap/Container";
import { Link } from "react-router-dom";
import BetterBreadcrumbs from "../components/BetterBreadcrumbs";
import TypesSearchbar from "../components/TypesSearchbar";
import TypesSearchResult from "../components/TypesSearchResult";
import { ItemTypesState } from "../store/ItemTypeState";

class ItemTypes extends React.Component<IItemTypesProps, ItemTypesState>{

    constructor(props: IItemTypesProps){
        super(props);
        this.state = new ItemTypesState();
    }

    private renderBreadcrumbs() : JSX.Element{
        return <BetterBreadcrumbs activeNode="Item Types" previousRoute={[{link:"/", name:"Dashboard"}]}>
            {this.props.canModify && <Link to="/types/new"><Button style={{position:"absolute", right: "1em", top:"50%",transform: "translateY(-50%)"}} variant="outline-primary">Add Type</Button></Link>}
        </BetterBreadcrumbs>
    }
    
    public render() : JSX.Element {
        return <>
            {this.renderBreadcrumbs()}
            <Container className="mt-4 d-flex flex-column">
                <TypesSearchbar parent={this.state} />
                <hr className="w-100" />
                <TypesSearchResult isLoading={this.state.isLoading} data={this.state.data} />
            </Container>
        </>
    }
}

export interface IItemTypesProps{
    canModify: boolean
}

export default observer(ItemTypes);