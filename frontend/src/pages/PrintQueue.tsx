import axios from "axios";
import { observer } from "mobx-react";
import React from "react";
import { Alert, Button, ButtonGroup, Container, ListGroup, ListGroupItem } from "react-bootstrap";
import { XCircleFill } from "react-bootstrap-icons";
import BetterBreadcrumbs from "../components/BetterBreadcrumbs";
import { PersistentPrintQueueState } from "../store/PersistentPrintQueueState";
import FileSaver from "file-saver";

class PrintQueue extends React.Component{
    public render() : JSX.Element {
        return <>
            <BetterBreadcrumbs activeNode="Print Queue" previousRoute={[{name:"Dashboard", link:"/"}]} />
            <Container className="mt-4">
                <h3>QR Printing Queue</h3>
                <p className="text-muted">Use the Shelf or Item Pages to add an it to the printing queue. Alternatively check the box when creating an entry.</p>
                {!PersistentPrintQueueState.instance.isPersistent && <Alert variant="warning">You system does not support Persistent Storage or you have not given permission to use said feature. The Printing Queue will clear when you reload the page.</Alert>}
                {PersistentPrintQueueState.instance.count > 0 ? 
                    this.printEntries() : this.printIsEmpty()
                }
                <ButtonGroup>
                    <Button disabled={PersistentPrintQueueState.instance.selected.length === 0} onClick={()=>this.sendRequest()}>Create PDF</Button>
                    <Button disabled={PersistentPrintQueueState.instance.selected.length === 0} variant="outline-primary" onClick={()=>PersistentPrintQueueState.instance.deselectAll()}>Deselect All</Button>
                    <Button disabled={PersistentPrintQueueState.instance.selected.length === PersistentPrintQueueState.instance.count} variant="outline-primary" onClick={()=>PersistentPrintQueueState.instance.selectAll()}>Select All</Button>
                </ButtonGroup>
            </Container>
        </>
    }
    async sendRequest() {
        const data = PersistentPrintQueueState.instance.selected.map(e => {
            return {
                type: e.type,
                uuid: e.uuid
            }
        });
        const response = await axios.post("/api/qrgenerator/pdf", {data: data},{responseType: "blob"})
        FileSaver.saveAs(response.data, "qrcodes.pdf");
    }
    printEntries(): React.ReactNode {
        return <>
            <p>{PersistentPrintQueueState.instance.count} Entries to print / {PersistentPrintQueueState.instance.selected.length} Selected</p>
            <ListGroup className="my-2">
                {
                    PersistentPrintQueueState.instance.all.map((e,index) => {
                        return <ListGroupItem onClick={()=>PersistentPrintQueueState.instance.swapSelection(e.uuid)} style={{cursor: "pointer"}} active={e.selected} key={e.uuid}>
                            <div  className="d-flex justify-content-between">
                            {e.type} :: {e.uuid}
                            <Button onClick={(e)=>{e.stopPropagation(); PersistentPrintQueueState.instance.removeEntry(index)}} variant={e.selected ? "primary" : "link"}><XCircleFill/></Button>
                            </div>
                        </ListGroupItem>
                    })
                }
            </ListGroup>
        </>
    }
    printIsEmpty(): React.ReactNode {
        return <Alert variant="info">No Items are in the Printing Queue.</Alert>
    }
}

export default observer(PrintQueue);