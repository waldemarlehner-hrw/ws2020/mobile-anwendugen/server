import axios from "axios";
import { observer } from "mobx-react";
import React from "react"
import Button from "react-bootstrap/Button";
import Collapse from "react-bootstrap/Collapse";
import FormControl from "react-bootstrap/FormControl";
import InputGroup from "react-bootstrap/InputGroup";
import { ItemTypesState } from "../store/ItemTypeState";


class TypesSearchbar extends React.Component<ITypesSearchbarProps>{

	
	private timer? : NodeJS.Timer;
	private debounceRelay(callback : ()=>void){
		if(this.timer){
			clearTimeout(this.timer)
		}
		this.timer = setTimeout(()=>{this.fetchNewData()}, 500);
		callback();
	}
	
	private async fetchNewData(){
		this.props.parent.updateIsLoading(true)
		const response = await axios.get("/api/itemtypes", {
			params: this.props.parent.searchbarState.queryObject,
			validateStatus(status){ return [200,400,404].some(e => status === e)}
		})
		if(response.status === 200){
			const results = response.data.data;
			this.props.parent.updateData(results);
		}else{
			this.props.parent.updateData([])
		}
		this.props.parent.updateIsLoading(false);
	}

	componentDidMount(){
		this.fetchNewData()
	}

	public render() : JSX.Element {
		return <>
			<InputGroup className="mb-3">
				<InputGroup.Prepend>
					<InputGroup.Text>
						Type Name
					</InputGroup.Text>
				</InputGroup.Prepend>
				<FormControl 
					value={this.props.parent.searchbarState.typeName} 
					onChange={(e) => {this.debounceRelay(() => this.props.parent.searchbarState.updateTypeName(e.target.value))}} 
					placeholder="Type Name"
				/>
			</InputGroup>
			<Collapse in={this.props.parent.searchbarState.isExpanded}>
				<div>
					<div className="d-flex flex-row justify-content-center my-4">
						<div className="form-check">
							<input className="form-check-input" checked={this.props.parent.searchbarState.onlyDisplayUsed} onChange={e => {this.props.parent.searchbarState.onOnlyDisplayUsedClick(); this.fetchNewData()}} type="checkbox" id="formInputFilterUsed"/>
							<label className="form-check-label" htmlFor="formInputFilterUsed">
								Hide unsused types.
							</label>
						</div>
					</div>
				</div>
			</Collapse>
			<Button variant="link" onClick={()=>this.props.parent.searchbarState.onExpandClick()}>
				Advanced Settings
			</Button>

		</>
	}
}

export interface ITypesSearchbarProps{
	parent: ItemTypesState
}

export default observer(TypesSearchbar);