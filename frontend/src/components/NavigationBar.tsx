import Navbar from "react-bootstrap/Navbar"
import Form from "react-bootstrap/Form"
import Nav from "react-bootstrap/Nav"
import Button from "react-bootstrap/Button"
import {Link, withRouter} from "react-router-dom" 
import axios from "axios"
import { ILoginState } from "../store/LoginState"
import React from "react"
import { AppState } from "../store/AppState"
import {RouteComponentProps} from "react-router-dom"
import { observer } from "mobx-react"
import ButtonGroup from "react-bootstrap/ButtonGroup"
import {PrinterFill as PrinterIcon} from "react-bootstrap-icons"
import Badge from "react-bootstrap/esm/Badge"
import { PersistentPrintQueueState } from "../store/PersistentPrintQueueState"

export class NavigationBar extends React.Component<AppState & RouteComponentProps>{

	public async logOut(){
		await axios.delete("/api/auth");
		const newAuth = await (await axios.get("/api/auth", {
			validateStatus: (status) => [200, 404].some(e => e === status)
			})
		);
		if(newAuth.status === 404){
			this.props.loginState.updateByStatusCall(false)
		}else{
			this.props.loginState.updateByStatusCall(newAuth.data.data as ILoginState)
		}
	}

	public goToLogin(){
		this.props.history.push("/login");
	}

	public render(){
		return (
			<Navbar bg="primary" expand="lg" variant="dark">
						<Link to="/" className="mr-auto">
							<Navbar.Brand >
								<img className="mr-2" width="30" height="30" alt="Brand Icon" src="./vectors/icon-white.svg" />
								<span >EasyWarehouse</span>
							</Navbar.Brand>
						</Link>
						<Nav>
							<Nav.Link onClick={()=>this.props.history.push("/")}>	
								Dashboard
							</Nav.Link>
							{this.props.loginState.isLoggedIn &&
							<Nav.Link className="mx-2" onClick={()=>this.goToPrintQueue()}>
								<PrinterIcon />
								{PersistentPrintQueueState.instance.count > 0 && <Badge variant="primary">{PersistentPrintQueueState.instance.count}</Badge>}
							</Nav.Link>
							}

						</Nav>
						<Form inline className="ml-2">
							{ this.props.loginState.isLoggedIn ?
								<ButtonGroup>
									<Button variant="outline-light" onClick={()=>this.props.history.push("/users/self")}>My Profile</Button>
									<Button variant="outline-light" onClick={()=>this.logOut()}>Log Out</Button>
								</ButtonGroup> :
								<Button variant="outline-light" onClick={()=>this.goToLogin()}>Log In</Button>
							}
						</Form>
						
					</Navbar>
		)
	}
	goToPrintQueue(): void {
		this.props.history.push("/printqueue")
	}
}

export default withRouter(observer(NavigationBar))