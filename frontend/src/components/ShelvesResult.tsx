import { observer } from "mobx-react";
import React from "react";
import Badge from "react-bootstrap/Badge";
import Card from "react-bootstrap/Card";
import { IShelfEntry } from "../pages/Shelves";
import ProgressBar from "react-bootstrap/ProgressBar"
import Spinner from "react-bootstrap/Spinner";
import { RouteComponentProps, withRouter } from "react-router-dom";

class ShelvesResult extends React.Component<{data: IShelfEntry[], isLoading: boolean} & RouteComponentProps>{
    private isFull(e: IShelfEntry){
        return e.capacityUsed > e.capacity
    }
    private percentage(e: IShelfEntry){
        if(this.isFull(e)){
            return 100;
        }else{
            return 100 * (e.capacityUsed / e.capacity);
        }
    }
    private getVariant(e: IShelfEntry){
        if(this.isFull(e)){
            return "danger";
        }
        if(this.percentage(e) > 80){
            return "warning";
        }
        return "success";
    }
    render(){
        if(this.props.isLoading){
            return <div className="mt-5 d-flex flex-row justify-content-center">
                <Spinner animation="grow" variant="primary" />
            </div>
        }
        return <div className="d-flex flex-column">
            <p className="text-muted">{this.props.data.length} Entries found.</p>
            {
                this.props.data.map((e,i) => {
                    return <Card 
                        className="mt-1" key={e.uuid} 
                        style={{cursor: "pointer"}} 
                        onClick={() => {this.props.history.push("/shelves/"+e.uuid)}}>
                        <Card.Body>
                            <Card.Title>
                                {e.name}
                                <Badge className="ml-4">
                                    {`${e.capacityUsed} / ${e.capacity}`}
                                </Badge>
                            </Card.Title>
                            <ProgressBar 
                                now={this.percentage(e)} 
                                label={`${e.capacityUsed} / ${e.capacity}`} 
                                variant={this.getVariant(e)}/>
                        </Card.Body>
                    </Card>
                })
            }
        </div>
    }
}

export default withRouter(observer(ShelvesResult));