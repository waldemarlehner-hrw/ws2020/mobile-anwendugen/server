import React from "react"
import InputGroup from "react-bootstrap/InputGroup"
import FormControl from "react-bootstrap/FormControl"
import Button from "react-bootstrap/Button"
import Collapse from "react-bootstrap/Collapse"
import DropdownButton from "react-bootstrap/DropdownButton"
import DropdownItem from "react-bootstrap/DropdownItem"
import UserSearchbarState from "../store/UserSearchbarState"
import { observer } from "mobx-react"
import axios from "axios"
import { RoleEnum } from "../store/LoginState"
import UserPageState, { IUserPageStateResult } from "../store/UserPageState"

class UserSearchbar extends React.Component<{parent: UserPageState}, UserSearchbarState>{

	constructor(props: {parent: UserPageState}){
		super(props);
		this.state = new UserSearchbarState();
	}

	private async fetchNewData(){
		this.props.parent.updateLoading(true)
		const result = await axios.get("/api/users", {
			params: this.state.queryParams,
			validateStatus: e => [200,404].some(f => f === e)
		})
		if(result.status === 200){
			const data : IUserPageStateResult[] = result.data.data.map((e:any) => {
				return {
					uuid: e.uuid as string,
					username: e.username as string,
					firstname: e.firstName as string,
					lastname: e.lastName as string,
					role: e.role as RoleEnum
				} 
			}) 

			this.props.parent.updateResult(data);
		}else{
			this.props.parent.updateResult([]);
		}
		this.props.parent.updateLoading(false);

	}

	private timer? : NodeJS.Timer;
	private debounceRelay(callback : ()=>void){
		if(this.timer){
			clearTimeout(this.timer)
		}
		this.timer = setTimeout(()=>{this.fetchNewData()}, 500);
		callback();
	}

	public componentDidMount(){
		this.fetchNewData()
	}

	public render() : JSX.Element{
		return <>
			<InputGroup>
				<InputGroup.Prepend>
					<InputGroup.Text>
					{"@"}
					</InputGroup.Text>
				</InputGroup.Prepend>
				<FormControl value={this.state.username} onChange={(e) => {this.debounceRelay(() => this.state.updateUsername(e.target.value))}} placeholder="Username"/>
			</InputGroup>
			<Collapse in={this.state.isExpanded}>
				<div>
					<InputGroup className="my-4 p-0">
						<FormControl value={this.state.firstname} onChange={(e) => {this.debounceRelay(()=> this.state.updateFirstname(e.target.value))}} placeholder="First Name" />
						<FormControl value={this.state.lastname} onChange={(e) => {this.debounceRelay(() => this.state.updateLastname(e.target.value))}} placeholder="Last Name"/>
						<DropdownButton variant="link" title="Roles">
							<DropdownItem active={this.state.roleRoot}  onClick={() => this.debounceRelay(()=>this.state.clickRoot())}>
								Root
							</DropdownItem>
							<DropdownItem active={this.state.roleAdmin}  onClick={() => this.debounceRelay(()=>this.state.clickAdmin())}>
								Admin
							</DropdownItem>
							<DropdownItem active={this.state.roleManager} onClick={() => this.debounceRelay(()=>this.state.clickManager())}>
								Manager
							</DropdownItem>
							<DropdownItem active={this.state.roleRegular}  onClick={() => this.debounceRelay(()=>this.state.clickRegular())}>
								Regular
							</DropdownItem>
						</DropdownButton>
					</InputGroup>
				</div>
			</Collapse>
			<Button variant="link" onClick={()=>this.state.clickExpand()}>
				Advanced Settings
			</Button>
		</>
	}

}


export default observer(UserSearchbar);