import axios from "axios";
import { observer } from "mobx-react";
import React from "react";
import Button from "react-bootstrap/Button";
import Collapse from "react-bootstrap/Collapse";
import FormControl from "react-bootstrap/FormControl";
import InputGroup from "react-bootstrap/InputGroup";
import ListGroup from "react-bootstrap/ListGroup";
import Modal from "react-bootstrap/Modal";
import { ItemsByItemTypeState } from "../store/ItemsByItemTypeState";
import { IShelfEntry } from "../pages/Shelves";
import { CreateItemModalState } from "../store/CreateItemModalState";

class CreateItemModal extends React.Component<{parent: ItemsByItemTypeState, typeUuid: string}, CreateItemModalState>{

    constructor(props: {parent: ItemsByItemTypeState,typeUuid: string}){
        super(props);
        this.state = new CreateItemModalState(props.typeUuid);
    }

    private handleClose(){
        this.state.clear();
        this.props.parent.updateShowCreateModal(false);
    }

    private shelfFragment(){
        return <div>
            <InputGroup>
                <InputGroup.Prepend>
                    <InputGroup.Text>
                        Shelf Name
                    </InputGroup.Text>
                </InputGroup.Prepend>
                <FormControl
                    placeholder="Enter Name..."
                    value={this.state.shelfInput}
                    onChange={e => {this.state.updateShelfInput(e.target.value); this.debounceFetch()}}
                />

            </InputGroup>
            <ListGroup>
            {this.state.autocompleteShelves.map(e => {
                return <ListGroup.Item onClick={() => {this.state.selectShelf(e.uuid)}} key={e.uuid} active={e.uuid === this.state.selectedShelfUuid}>
                    {e.name}
                </ListGroup.Item>
            })}
            </ListGroup>
        </div>
    }

    private timer?: NodeJS.Timer
    private debounceFetch(){
        if(this.timer){
            clearTimeout(this.timer);
        }
        this.timer = setTimeout(()=>this.fetch(), 500)
    }

    private async fetch(){
        const response = await axios.get("/api/shelves", {params: this.state.getObject, validateStatus(status){return [200,404].some(e => e === status)}})
        if(response.status === 200){
            this.state.updateAutocompleteData((response.data.data as IShelfEntry[]).slice(0,6))
        }else{
            this.state.updateAutocompleteData([])
        }
    }

    private async createItem(){
        const response = await axios.post(this.state.postUrl, this.state.postArgs)
        if(response.status === 200){
            const newData = await axios.get("/api/itemtypes/"+this.props.typeUuid,{
                params: {
                    includeItems: true,
                },
                validateStatus(status){
                    return [200,404].some(e => e === status)
                }
            })
            this.props.parent.updateData(newData.data.data)
        }
        this.state.clear();
        this.props.parent.updateShowCreateModal(false);
        
    }

    componentDidMount(){
        this.fetch();
    }

    render(){
        return <Modal show={this.props.parent.showCreateModal} onHide={() => this.handleClose()} backdrop="static">
            <Modal.Header closeButton>
                Add new Item
            </Modal.Header>
            <Modal.Body>
                <InputGroup>
                    <InputGroup.Prepend>
                        <InputGroup.Text>Notes</InputGroup.Text>
                    </InputGroup.Prepend>
                    <FormControl
                        placeholder="Add optional Notes..."
                        as="textarea"
                        value={this.state.notes}
                        onChange={e => this.state.updateNotes(e.target.value)}
                    />
                </InputGroup>
                <p className={this.state.notes.trim().length > 255 ? "text-danger" : "text-muted"}>{this.state.notes.trim().length} / 255</p>
                <FormControl defaultValue="default" as="select" value={this.state.status} onChange={e => this.state.updateStatus(e.target.value)}>
                    <option value="default">Default</option>
                    <option value="outofsystem">Out of System</option>
                    <option value="lost">Lost</option>
                    <option value="reserved">Reserved</option>
                </FormControl>
                <div className="d-flex flex-row justify-content-center">
                    <div className="form-check  my-3">
                        <input type="checkbox" id="formInputFilterUsed" className="form-check-input" checked={this.state.useShelf} onChange={()=>this.state.flipUseShelf()}/>
                        <label className="form-check-label" htmlFor="formInputFilterUsed">Add to a shelf</label>
                    </div>
                </div>
                <Collapse in={this.state.useShelf}>
                    {this.shelfFragment()}
                </Collapse>
                
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={()=>this.handleClose()}>Close</Button>
                <Button variant="primary" disabled={!this.state.canCreate} onClick={()=>this.createItem()}>Create</Button>
            </Modal.Footer>
        </Modal>
    }
}

export default observer(CreateItemModal)