import React from "react";
import Badge from "react-bootstrap/Badge";
import Fade from "react-bootstrap/Fade";

class ModifiedBadge extends React.Component<{show: boolean}>{
    render(){
        return <Fade in={this.props.show}>
            <div><Badge variant="primary">Modified</Badge></div>
        </Fade>
    }
}

export default ModifiedBadge;