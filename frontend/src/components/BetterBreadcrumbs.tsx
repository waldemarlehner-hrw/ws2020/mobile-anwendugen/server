import React from "react";
import Breadcrumb from "react-bootstrap/Breadcrumb";
import { Link } from "react-router-dom";

export interface IBetterBreadcrumbsProps{
    previousRoute: {
        name: string,
        link: string
    }[],
    activeNode: string
}

class BetterBreadcrumbs extends React.Component<IBetterBreadcrumbsProps>{

    render(){
        return <div style={{position:"relative", borderRadius: 0}}>
        <ol className="breadcrumb">
            {
                this.props.previousRoute.map((e,i) => {
                    return <Breadcrumb.Item key={i}><Link to={e.link}>{e.name}</Link></Breadcrumb.Item>
                })
            }
            <Breadcrumb.Item active>{this.props.activeNode}</Breadcrumb.Item>
        </ol>
        {this.props.children}
    </div>
    }
}

export default BetterBreadcrumbs;