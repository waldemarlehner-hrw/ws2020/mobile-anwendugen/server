import { observer } from "mobx-react"
import React from "react"
import Spinner from "react-bootstrap/Spinner"
import Table from "react-bootstrap/Table"
import { RouteComponentProps, withRouter } from "react-router-dom"
import { IItemTypeEntry } from "../store/ItemTypeState"

class TypesSearchResult extends React.Component<{isLoading: boolean, data: IItemTypeEntry[]} & RouteComponentProps>{

    render() : JSX.Element{
        return <>
            <p className="text-muted">Found {this.props.data.length} result{this.props.data.length !== 1 ? "s" : ""}.</p>
            {
                this.props.isLoading ?
                <div className="w-100 d-flex flex-row justify-content-center">
                    <Spinner animation="border" variant="primary"/>
                </div> :
                <div className="table-responsive">
                    <Table striped bordered hover className="">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Description</th>
                                <th>Count</th>
                                <th>Orders</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                this.props.data.map(e => {
                                    return <tr key={e.uuid} style={{cursor: "pointer"}} onClick={(f)=>{this.props.history.push("/types/"+e.uuid)}}>
                                        <td>{e.name}</td>
                                        <td style={{maxWidth: 300, overflow: "hidden", textOverflow: "ellipsis", whiteSpace: "nowrap"}}>
                                            <span>{e.description}</span>        
                                        </td>
                                        <td>{e.itemCount}</td>
                                        <td>{e.ordersCount}</td>
                                    </tr>
                                })
                            }
                        </tbody>
                    </Table>
                </div>
            }
        </>
    }
}

export default withRouter(observer(TypesSearchResult))