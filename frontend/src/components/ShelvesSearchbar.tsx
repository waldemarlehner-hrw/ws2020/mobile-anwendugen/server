import axios from "axios";
import { observer } from "mobx-react";
import React from "react";
import Button from "react-bootstrap/Button";
import Collapse from "react-bootstrap/Collapse";
import FormControl from "react-bootstrap/FormControl";
import InputGroup from "react-bootstrap/InputGroup";
import { ShelvesState } from "../pages/Shelves";

class ShelvesSearchbar extends React.Component<{parent: ShelvesState}>{
    
    private timer?: NodeJS.Timer;
    private debounceFetch(){
        if(this.timer){
            clearTimeout(this.timer);
        }
        this.timer = setTimeout( () => {this.fetchNewData()}, 500);
    }

    private async fetchNewData(){
        this.props.parent.updateIsLoading(true);
        const response = await axios.get("/api/shelves", {
            params: this.props.parent.searchbarState.queryObject,
            validateStatus(status){return [200, 404].some(e => e === status)}
        });
        if(response.status === 200){
            const result = response.data.data;
            this.props.parent.updateData(result);
        }else{
            this.props.parent.updateData([]);
        }
        this.props.parent.updateIsLoading(false);
    }

    componentDidMount(){
        this.fetchNewData();
    }
    
    render(){
        return <>
            <InputGroup className="mb-3">
                <InputGroup.Prepend><InputGroup.Text>Shelf Name</InputGroup.Text></InputGroup.Prepend>
                <FormControl
                    value={this.props.parent.searchbarState.shelfName}
                    onChange={e => {this.props.parent.searchbarState.updateShelfName(e.target.value); this.debounceFetch();}}
                    placeholder="Shelf Name"
                />
            </InputGroup>
            <Collapse in={this.props.parent.searchbarState.isExpanded}>
                <div>
                    <div className="d-flex flex-row justify-content-center my-4">
                        <div className="form-check">
                            <input className="form-check-input" checked={this.props.parent.searchbarState.onlyDisplayUsed} onChange={ e => {
                                this.props.parent.searchbarState.onOnlyDisplayUsedClick(); this.debounceFetch();
                            }} type="checkbox" id="formInputFilterUsed"/>
                            <label className="form-check-label" htmlFor="formInputFilterUsed">
								Hide unsused Shelves.
							</label>
                        </div>
                    </div>
                </div>
            </Collapse>
            <Button variant="link" onClick={()=>this.props.parent.searchbarState.onIsExpandedClick()}>Advanced Settings</Button>
        </>
    }
}

export default observer(ShelvesSearchbar);