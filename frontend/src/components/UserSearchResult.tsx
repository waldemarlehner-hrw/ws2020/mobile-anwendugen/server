import React from "react"
import { RouteComponentProps, withRouter } from "react-router-dom";
import { IUserPageStateResult } from "../store/UserPageState";
import Table from "react-bootstrap/Table" 
import Spinner from "react-bootstrap/Spinner";

interface IUserSeachResultProps{
	data: IUserPageStateResult[],
	loading: boolean
}

class UserSearchResult extends React.Component<IUserSeachResultProps & RouteComponentProps>{

	private renderTable(){
		return (<Table striped bordered hover>
			<thead>
				<tr>
					<th>Username</th>
					<th>Firstname</th>
					<th>Lastname</th>
					<th>Role</th>
				</tr>
			</thead>

			<tbody>
				{
					this.props.data.map((e) => {
						return (
							<tr key={e.uuid} style={{cursor: "pointer"}} onClick={(f)=>{this.props.history.push("/users/"+e.uuid)}}> 
								<td>{e.username}</td>
								<td>{e.firstname}</td>
								<td>{e.lastname}</td>
								<td>{e.role}</td>
							</tr>
						)
					})
				}
				
			</tbody>
		</Table>)
	}

	render() : JSX.Element{
		return(
			<>
				<p className="text-muted">Found {this.props.data.length} result{this.props.data.length !== 1 ? "s" : ""}.</p>
				{
					this.props.loading 
					? 
					<div className="w-100 d-flex flex-row justify-content-center">
						<Spinner className="" animation="border" variant="primary"/>
					</div>
					:
					<div className="table-responsive">
						{this.renderTable()}
					</div>
				}
			</>
		)
	}


}

export default withRouter(UserSearchResult);