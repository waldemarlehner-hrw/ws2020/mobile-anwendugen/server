import { Request, Response} from 'express';
import { User } from '../database/model/User';
import {RoleEnum, AllRolesByPrecedence} from "../database/RoleEnum"
import { StatusCodes } from 'http-status-codes';
import { ExpressSessionData } from '../interfaces/ExpressSessionData';
import Database from '../database/Database';

/**
 * A middleware decorator for authentification. 
 * Pass the roles that are allowed to access the given resource.
 */

 export function Authenticate(allowedRoles: RoleEnum[] | RoleEnum){
	 if(typeof allowedRoles === "string"){
		 const index = AllRolesByPrecedence.indexOf(allowedRoles as string);
		 allowedRoles = AllRolesByPrecedence.slice(0,index+1) as RoleEnum[];
	 }
	 
	 return function(target: Object, key: string | symbol, descriptor: PropertyDescriptor){
		const originalDescriptor = (descriptor ?? Object.getOwnPropertyDescriptor(target, key));
		const originalMethod = originalDescriptor.value;
		descriptor.value = async function(... args : any[]){
			const req: Request = args[0];
			const res: Response = args[1];
			if(allowedRoles.length > 0){
				const session = <ExpressSessionData>req.session
				if(!session.currentUserId){
					return res.status(StatusCodes.FORBIDDEN).send({reason:"Access forbidden: Not logged in"});
				}
				else{
					const user = await Database.connection.getRepository(User).findOne({uuid: session.currentUserId})
					if(user){
						if ((allowedRoles as RoleEnum[]).some(e => e === user.role)){
							// Access to endpoint granted.
							return originalMethod.apply(this, [req,res]);
						}
						else{
							return res.status(StatusCodes.FORBIDDEN).send({reason: "You do not have the required permissions"});
						}

					}else{
						// Force log out as the user one is logged it does no longer exist
						await new Promise<void>( (resolve, reject) => {
							req.session.destroy( (err) => {
								if(err){ reject() }
								else { resolve() }
							});
						})
						return res.status(StatusCodes.FORBIDDEN).send({reason: "The user you are logged in as is no longer valid. Please log in again"})
					}
				}
			}


			if(allowedRoles.length === 0){
				return originalMethod.apply(this, [...args]);
			}
		}
	 }
 }