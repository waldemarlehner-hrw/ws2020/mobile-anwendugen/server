import {config as dotenv} from "dotenv"
import betterlogging from "better-logging"
import Server from "./server";
import "reflect-metadata";
import { Connection, createConnection } from 'typeorm';
import { User } from "./database/model/User";
import { Crypto } from './database/helpers/Crypto';
import Database from './database/Database';
import { existsSync, mkdirSync, readFileSync, writeFileSync} from "fs";
import prompts = require("prompts")


betterlogging(console);
console.log("Running start script...");

dotenv({
	path: "./config/development.env"
});
const port : string | undefined = process.env.PORT || process.env.SELF_PORT;

//Create database connection
(async () => {
	
	// Check if SQLite DB exists. It wont on the first run. Depending on that use either synchronize or no synchronize.
	await initConnection(!existsSync("../data/database.sqlite3"));

	//Check if at least one admin is in the database. If not, create a default root user.
	const rootCount = await Database.connection.getRepository(User).findAndCount({role: "root"})
	if(rootCount[1] === 0){
		// No admin exists. Prompt user to create one.

		const questions = await prompts([
			{
				type: "text",
				name: "username",
				message: "Please enter a username. Min Length: 5",
				validate: (value: string) => (value.trim().length >= 5 ? true : "Username needs to be at least 5 characters long")
			},
			{
				type: "password",
				name: "password",
				message: "Please enter a password. Min Length: 8",
				validate: (value: string) => (value.length >= 8 ? true : "Password needs to be at least 8 characters long")
			},
			{
				type: "text",
				name: "firstname",
				message: "Enter first name",
				validate: (value: string) => (value.trim().length > 0 ? true : "First Name cannot be empty.")
			},
			{
				type: "text",
				name: "lastname",
				message: "Enter last name",
				validate: (value: string) => (value.trim().length > 0 ? true : "Last Name cannot be empty.")
			}
		])
		const hasCreated = await Database.createUser(questions.username, questions.firstname, questions.lastname, questions.password, "root")
		if(hasCreated){
			console.info("Created user successfully...")
		}else{
			console.error("Could not create user...")
		}
		
	}
	await generatePrivateSessionKeyIfNeeded();
	//Create express Server and start.
	const server : Server = new Server(readFileSync(__dirname+"/../data/.private/key").toString());
	server.start(port);
})();


async function generatePrivateSessionKeyIfNeeded(force?: boolean):Promise<void>{
	const privateDir = __dirname+"/../data/.private/";
	force = force ?? false;
	if(!existsSync(privateDir)){
		mkdirSync(privateDir);
	}
	if(!existsSync(privateDir+"key") || force){
		const randString = await Crypto.generateRandomString(1024);
		writeFileSync(privateDir+"key",randString);
		console.warn("No private session key found. Generating key.");
	}
}

async function initConnection(synchronize: boolean) : Promise<void> {
	let connection : Connection;
	if(synchronize){
		connection = await createConnection("sqlitefirsttime");
	}else{
		connection = await createConnection("sqlite");
	}
	Database.connection = connection;
}