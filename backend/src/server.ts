import {Server as ExpressServer} from "@overnightjs/core"
import * as bodyParser from "body-parser";
import { ControllerHelper } from './controllers/ControllerHelper';
import session = require('express-session');
// OvernightJS abstracts express and allows to make use of Typescript Annotations
export default class Server extends ExpressServer{
    
	constructor(secret: string){
		const logServer = process.env.LOG_SERVER?.toLocaleLowerCase() === "true" ?? false;
		super(logServer);
		this.app.use(bodyParser.json());
		this.app.use(bodyParser.urlencoded({extended: true}));
		this.app.use(session({
			secret: secret,
			resave: false,
			saveUninitialized: false
		}));
		this.attachControllers();
	}

	public async start(port: string | undefined) : Promise<void>{
		this.app.listen(port, () => {
			console.info(`Listening on http://localhost:${port}`)
		});
	}

	private attachControllers() : void{
		const allControllers = ControllerHelper.allControllersAsArray();
		const instances = [];
		for (const controller of allControllers){
			instances.push(new controller());
		}
		super.addControllers(instances);
	}


}