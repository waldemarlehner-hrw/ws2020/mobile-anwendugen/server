import { ClassErrorMiddleware, ClassWrapper, Controller, Get, Middleware } from '@overnightjs/core';

import express, {Response, Request} from "express"
import asyncWrapper from "../helpers/AsyncWrapper";
import { DefaultErrorHandler } from "../errorhandlers/DefaultErrorHandler";
import path from "path";

// This is the Controller that routes to the React Frontend.
// Because React is (in it's compiled form) a static site, we can just relay the HTML as public 
//? If you want to update the React App, run `npm run build:react` from the root project.
@Controller("")
@ClassErrorMiddleware(DefaultErrorHandler)
@ClassWrapper(asyncWrapper)
export default class IndexController{

	@Get("*")
	@Middleware(express.static(path.join(__dirname,"../../../frontend/build")))
	private async getCurrentAuth(req: Request, res: Response){
		res.sendFile(path.join(__dirname,"../../../frontend/build/index.html"))
	}
}