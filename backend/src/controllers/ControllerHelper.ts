
import AuthController from "./api/AuthController";
import ItemController from "./api/ItemController";
import ItemTypeController from "./api/ItemTypeController";
import PrintQueueController from "./api/PrintQueueController";
import ShelfController from "./api/ShelfController";
import StatusController from "./api/StatusController";
import UserController from './api/UserController';
import IndexController from "./IndexController"
export class ControllerHelper {
	private constructor(){}

	public static allControllersAsArray() : Array<any> {
		return [
			AuthController,
			ItemController,
			ItemTypeController,
			StatusController,
			UserController,
			ShelfController,
			PrintQueueController,
			IndexController,
		];
	}
} 