import { Controller, Get } from "@overnightjs/core";
import { Request, Response } from "express";
import { StatusCodes } from "http-status-codes";

@Controller("api/status")
export default class StatusController{
	@Get()
	getStatus(_: Request, res: Response) : Response<IStatusResponse> {
		const status : IStatusResponse = {
			version: "0.0.1",
			productName: "easywarehouse"
		}
		return res.status(StatusCodes.OK).send(status)
	}
}

export interface IStatusResponse{
	version: string,
	productName: string
}