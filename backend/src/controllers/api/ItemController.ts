import { ClassErrorMiddleware, ClassWrapper, Controller, Delete, Middleware, Patch, Post } from "@overnightjs/core";
import { Response, Request } from "express";
import { Authenticate } from "../../decorators/Authenticate";
import {Get} from "@overnightjs/core";
import { trimBody, trimParams } from "../../middleware/AutoTrim";
import { body, param } from "express-validator";
import { StatusEnum } from "../../database/model/Item";
import {GetOne} from "./Item/Item.Get"
import { generateResponseObjectOnError } from "../../helpers/ErrorResponseHelper";
import { createItem } from "./Item/Item.Post";
import { modifyItem } from "./Item/Item.Patch";
import { removeItem } from "./Item/Item.Delete";
import { DefaultErrorHandler } from "../../errorhandlers/DefaultErrorHandler";
import asyncWrapper from "../../helpers/AsyncWrapper";

@Controller("api/items")
@ClassErrorMiddleware(DefaultErrorHandler)
@ClassWrapper(asyncWrapper)
export default class ItemController{
	@Get(":id")
	@Authenticate("regular")
	@Middleware([
		trimParams,
		param("id").isUUID("all").optional(false)
	])
	async getItemById(req: Request, res: Response) : Promise<Response>{
		const errorResponse = generateResponseObjectOnError(req,res);
		if(errorResponse){
			return errorResponse;
		}

		return await GetOne(req, res);
	}

	@Post(":itemtypeid/:shelfid?")
	@Authenticate("manager")
	@Middleware([
		trimParams,
		trimBody,
		param("itemtypeid").isInt({min: 0}).optional(false),
		param("shelfid").isUUID(4).optional(true),
		body("notes").isString().isLength({max: 255}).optional(true),
		body("status").isIn(["stored","moving","outofsystem","lost","reserved"]).optional(true)
	])
	async postItem(req: Request, res: Response){
		const errorResponse = generateResponseObjectOnError(req,res);
		if(errorResponse){
			return errorResponse;
		}
		const type = req.params.itemtypeid as string;
		const shelfid = req.params.shelfid as string | undefined;
		const notes = req.body.notes as string | undefined;
		const status = req.body.status as string | undefined;
		return await createItem(type, res, shelfid, status, notes);
	}

	@Patch(":uuid")
	@Authenticate("regular") // Regular Users need to be able to take out items, change Item Notes, etc
	@Middleware([
		trimParams,
		trimBody,
		param("uuid").isUUID("all").optional(false),
		body("notes").isString().isLength({max:255}).optional(true),
		body("status").isString().isIn(["stored","moving","outofsystem","lost","reserved"]).optional(true),
		body("shelf").isString().optional(true)
	])
	async patchItem(req: Request, res: Response){
		const errorResponse = generateResponseObjectOnError(req,res);
		if(errorResponse){
			return errorResponse;
		}
		const uuid = req.params.uuid as string;
		const notes = req.body.notes as string | undefined;
		const status = req.body.status as string | undefined;
		const shelf = req.body.shelf as string | undefined;

		return await modifyItem(uuid, res, notes, status, shelf);
	}

	@Delete(":uuid")
	@Authenticate("manager")
	@Middleware([
		trimParams,
		param("uuid").isUUID(4).optional(false)
	])
	async deleteItem(req: Request, res: Response){
		const errorResponse = generateResponseObjectOnError(req,res);
		if(errorResponse){
			return errorResponse;
		}
		return await removeItem(req, res);
	}
}

export interface IItemObject {
	uuid: string,
	status: StatusEnum,
	notes?:string,
	type: IItemObjectType
	shelf?:IItemObjectShelf
}

export interface IItemObjectType{
	name: string,
	description: string,
	uuid: number,
	universalBarcode?: string,
	url?:string
}

export interface IItemObjectShelf{
	uuid: string,
	name: string,
	coordinates: {x: number, y: number},
	shelf: {
		uuid: string,
		level: number
	},
	description?:string
}