import { Request, Response } from "express";
import { StatusCodes } from "http-status-codes";
import Database from "../../../database/Database";
import { User } from "../../../database/model/User";
import { compareRoles, RoleEnum } from "../../../database/RoleEnum";
import { ExpressSessionData } from "../../../interfaces/ExpressSessionData";
import { Crypto } from "../../../database/helpers/Crypto";

async function postUser(req : Request, res: Response){
    // Check the callees priviledges. If fail -> Servererror as @Authenticate already checked the user
		const callee = await Database.connection.getRepository(User).findOneOrFail({uuid: (req.session as ExpressSessionData).currentUserId})
		if(compareRoles(req.body.role as RoleEnum, callee.role) > 0){
			// Required Perms are there
			
			// Check if a user with this username already exists
			const countWithUName = await Database.connection.getRepository(User).count({username: req.body.username});
			if(countWithUName !== 0){
				return res.status(StatusCodes.CONFLICT).send({reason: "There is already a user with this username"});
			}
			else{
				const passwordHashPair = await Crypto.generate(req.body.password);
				const userRepo = Database.connection.getRepository(User);
				const newUser = userRepo.create();
				newUser.username = (req.body.username as string).trim();
				newUser.firstName = (req.body.firstname as string).trim();
				newUser.lastName = (req.body.lastname as string).trim();
				newUser.passwordSalt = passwordHashPair.salt;
				newUser.passwordHash = passwordHashPair.hash;
				newUser.role = (req.body.role as RoleEnum)
				await userRepo.save(newUser);
				res.status(StatusCodes.OK).send({reason: "User added successfully."});
			}
		}
		else{
			return res.status(StatusCodes.FORBIDDEN).send({reason: "To create a user your role needs to be higher than the role of the created user."})
		}
}

export default {
	postUser
}