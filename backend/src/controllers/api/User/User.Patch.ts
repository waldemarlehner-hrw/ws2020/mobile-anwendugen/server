import { Crypto } from './../../../database/helpers/Crypto';
import { Request, Response } from "express";
import { StatusCodes } from "http-status-codes";
import Database from "../../../database/Database";
import { User } from "../../../database/model/User";
import { ExpressSessionData } from "../../../interfaces/ExpressSessionData";
import { compareRoles, RoleEnum } from "../../../database/RoleEnum";
import UserHelpers from './User.Helpers';

async function patchUserSelf(req : Request, res: Response){
    const userRepo = Database.connection.getRepository(User);
    try{
        const self = await userRepo.findOneOrFail({uuid: (req.session as ExpressSessionData).currentUserId})
        try{
            await applyPatch(self,req.body);
        }
        catch(err : any){
            return res.status(StatusCodes.CONFLICT).send({reason: err})
        }
        
        await userRepo.save(self);
        return res.status(StatusCodes.OK).send({reason: "Update successful.", data: UserHelpers.mapUser(self)})
    }
    catch{
        return res.status(StatusCodes.NOT_FOUND).send({reason: "No User with this ID exist"})
    }
}

async function patchUser(req: Request, res: Response){
    const userRepo = Database.connection.getRepository(User);
    // Args are valid
    const userToPatch = await userRepo.findOne({uuid: req.params.uuid});
    const self = await userRepo.findOneOrFail({uuid: (req.session as ExpressSessionData).currentUserId})
    
    if(!userToPatch){
        return res.status(StatusCodes.NOT_FOUND).send({reason:"Given UUID does not match a user."})
    }
    // Check that own role is higher than the target
    if(compareRoles(userToPatch.role, self.role) <= 0){
        return res.status(StatusCodes.FORBIDDEN).send({reason: "To modify a user their role needs to be lower than yours."})
    }

    // Username needs to be unique. If it is set to be modified, check if someone w/ that username already exists.
    if(req.body.username){
        const count = await userRepo.count({username: (req.body.username as string).trim()});
        if(count > 0){
            return res.status(StatusCodes.CONFLICT).send({reason: "Cannot change username. Someone else has that username already."})
        }
        userToPatch.username = (req.body.username as string).trim();
    }

    if((req.body.firstname)){
        userToPatch.firstName = (req.body.firstname as string).trim();
    }
    if(req.body.lastname){
        userToPatch.lastName = (req.body.lastname as string).trim();
    }
    if(req.body.password){
        const hashPair = await Crypto.generate(req.body.password);
        userToPatch.passwordHash = hashPair.hash;
        userToPatch.passwordSalt = hashPair.salt;
    }
    if(req.body.role){
        userToPatch.role = (req.body.role as string).trim() as RoleEnum
    }

    await userRepo.save(userToPatch);
    return res.status(StatusCodes.OK).send({reason:"Applied changes successfully", data: UserHelpers.mapUser(userToPatch)});
}

async function applyPatch(user: User, args: {username?: any, firstname?: any, lastname?: any, password?: any, oldpassword?: any}):Promise<void>{
    if(args.username){
        const userWithNewNameExists = await Database.connection.getRepository(User).count({username: args.username}) > 0
        if(userWithNewNameExists){
            throw "Cannot change username. Another user already has that username."
        }
        user.username = args.username;
    }
    if(args.firstname){
        user.firstName = (args.firstname as string).trim();
    }
    if(args.lastname){
        user.lastName = (args.lastname as string).trim();
    }
    if(args.password){
        await patchPassword(user, args.oldpassword, args.password)
    }
}

async function patchPassword(user: User, oldpassword: unknown, newpassword: unknown){
    if(!oldpassword){
        throw "Cannot change password. Missing oldpassword."
    }
    if(typeof oldpassword !== "string"){
        throw "Old password is not a string"
    }
    if(typeof newpassword !== "string"){
        throw "New password is not a string"
    }

    if(user.passwordVerify(oldpassword)){
        const passwordHashPair = await Crypto.generate(newpassword);
        user.passwordHash = passwordHashPair.hash;
        user.passwordSalt = passwordHashPair.salt;
    }else{
        throw "Cannot change password. Given oldpassword does not match"
    }
}


export default {
    patchUserSelf,
    patchUser
}