import { Request, Response } from "express";
import { StatusCodes } from "http-status-codes";
import Database from "../../../database/Database";
import { User } from "../../../database/model/User";
import { compareRoles } from "../../../database/RoleEnum";
import { ExpressSessionData } from "../../../interfaces/ExpressSessionData";

async function deleteUser(req: Request, res: Response){
	// Check the callees priviledges. If fail -> Servererror as @Authenticate already checked the user
	const userRepo = Database.connection.getRepository(User);
	const callee = await userRepo.findOneOrFail({uuid: (req.session as ExpressSessionData).currentUserId})
	const userToDelete = await userRepo.findOne({uuid: (req.params.uuid)})
	if(typeof userToDelete === "undefined"){
		return res.status(StatusCodes.NOT_FOUND).send({reason: "Cannot delete because no used with the given UUID exists"});
	}
	if(compareRoles((userToDelete).role, callee.role) > 0){
		await userRepo.delete(userToDelete);
		return res.status(StatusCodes.OK).send({reason: "Deleted user"});
	}else{
		// Not enough rights to delete
		return res.status(StatusCodes.FORBIDDEN).send({reason: "To delete a user their role needs to be lower than yours."})
	}
}

export default {
	deleteUser
}