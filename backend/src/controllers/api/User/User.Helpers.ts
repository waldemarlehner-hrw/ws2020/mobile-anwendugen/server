import { User } from "../../../database/model/User";

function mapUser(user: User){
    return {
        username: user.username,
        firstName: user.firstName,
        lastName: user.lastName,
        role: user.role,
        uuid: user.uuid
    }
}

export default {
    mapUser
}