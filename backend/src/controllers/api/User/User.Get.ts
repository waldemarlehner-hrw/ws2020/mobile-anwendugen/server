import { Request, Response } from "express";
import { StatusCodes } from "http-status-codes";
import Database from "../../../database/Database";
import { User } from "../../../database/model/User";
import Helper from "./User.Helpers"

export async function getAllUsers(req: Request, res: Response){
    const andChain = generateAndChain(req.query)
    const query = buildSQLQuery(andChain);
    const users = await query.getMany();
    const usersMapped = users.map(e => Helper.mapUser(e))
    
    return res.status(usersMapped.length > 0 ? StatusCodes.OK : StatusCodes.NOT_FOUND).send({data: usersMapped});
}

export async function getById(uuid: string, res: Response){
    const user = await Database.connection.getRepository(User).findOne({uuid: uuid});
    if(user){
        return res.status(StatusCodes.OK).send({reason:"Found user", data: Helper.mapUser(user)})
    }else{
        return res.status(StatusCodes.NOT_FOUND).send({reason:"No User with that ID exists."})
    }
}

function buildSQLQuery(conditions: [string, Object][]){
    const query = Database.connection.getRepository(User).createQueryBuilder("user")
    for(let i = 0; i < conditions.length; i++){
        const [string, obj] = conditions[i]
        if(i === 0){
            query.where(string, obj)
        }else{
            query.andWhere(string, obj)
        }
    }
    return query;
}

function generateAndChain(query : any){
    const andChain: [string, Object][] = [];

    if(query.uuid){
        andChain.push(["user.uuid == :uuid", {uuid: query.uuid}])
    }
    else{
        
        if(query.username){
            andChain.push(["user.username LIKE :username", {username: query.username+"%"}])
        }
        if(query.firstname){
            andChain.push(["user.firstname LIKE :firstname", {firstname: query.firstname+"%"}])
        }
        if(query.lastname){
            andChain.push(["user.lastname LIKE :lastname ", {lastname: query.lastname+"%"}])
        }
        if(query.roles){
            andChain.push(["user.role in (:...roles)", {roles: query.roles}])
        }
    }
    return andChain;
}
export default {
    getAllUsers,
    getById
}