import { Request, Response } from "express";
import { StatusCodes } from "http-status-codes";
import Database from "../../../database/Database";
import { Item } from "../../../database/model/Item";
import { IItemObject, IItemObjectType } from "../ItemController";

async function GetItemObject(item:Item) : Promise<IItemObject> {
		
    // Add type of item
    const type = await item.type;

    const typeObject: IItemObjectType = {
        name: type.name,
        description: type.description,
        uuid: type.id
    };

    const shelf = await item.shelf;

    let shelfObject: any = {}
    if(shelf){
        shelfObject.name = shelf.name,
        shelfObject.uuid = shelf.uuid
    }

    const returnObject : IItemObject = {
        uuid: item.uuid,
        status: item.status,
        type: typeObject,
        ...item.notes && {notes: item.notes},
        ...shelf && {shelf: shelfObject}
    }

    
    return returnObject;
}

export async function GetOne(req: Request, res: Response){
    const repo = Database.connection.getRepository(Item);
    const itemToReturn = await repo.findOne(req.params.id);
    if(itemToReturn){
        return res.status(StatusCodes.OK).send({reason:"Retrieved entry", data: await GetItemObject(itemToReturn)});
    }else{
        return res.status(StatusCodes.NOT_FOUND).send({reason: "No Item with given Id"});
    }
}