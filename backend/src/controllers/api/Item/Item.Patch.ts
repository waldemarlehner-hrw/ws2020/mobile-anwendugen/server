import { Response } from "express";
import { StatusCodes } from "http-status-codes";
import Database from "../../../database/Database";
import { Item, StatusEnum } from "../../../database/model/Item";
import { Shelf } from "../../../database/model/Shelf";
import { updateShelfCapacity } from "../Shelf/Shelf.Helper";

export async function modifyItem(uuid: string, res: Response, notes?:string, status?:string, shelfUuid?:string){
    let itemToPatch = await Database.connection.getRepository(Item).findOne({uuid: uuid});
    if(typeof itemToPatch === "undefined"){
        return res.status(StatusCodes.NOT_FOUND).send({reason: "No Item found with the given ID."});
    }
    const oldShelf = await itemToPatch.shelf
    if(notes){
        itemToPatch.notes = notes;
    }
    if(shelfUuid){
        const response = await handleShelf(shelfUuid, itemToPatch,res)
        if(response){
            return response;
        }
        itemToPatch = await Database.connection.getRepository(Item).findOne({uuid: uuid}) as Item;
    }
    if(!["stored","moving"].some(e => status === e)){
        itemToPatch.status = status as StatusEnum;
    }else{
        if(await itemToPatch.shelf){
            itemToPatch.status = "stored"
        }else{
            itemToPatch.status = "moving"
        }
    }
    await Database.connection.getRepository(Item).save(itemToPatch)
    if(oldShelf){
        await updateShelfCapacity(oldShelf.uuid)
    }
    const itemToPatchUpdated = await Database.connection.getRepository(Item).findOne({uuid: uuid})
    const newShelf = await itemToPatch.shelf;
    if(newShelf){
        await updateShelfCapacity(newShelf.uuid)
    }
    return res.status(StatusCodes.OK).send({reason: "Patched successfully."})
}

async function handleShelf(shelfUuid : string, itemToPatch: Item, res: Response){
    
    if(shelfUuid === "remove"){
        if(["stored","moving"].some(e => e === itemToPatch.status)){
            itemToPatch.status = "moving"
        }
        const conn = Database.connection;
        const shelfToRemove = await itemToPatch.shelf
        await conn.createQueryBuilder().relation(Shelf, "items").of(shelfToRemove).remove(itemToPatch.uuid)
        itemToPatch.shelf = undefined
    }else{
        const shelf = await Database.connection.getRepository(Shelf).findOne({uuid:shelfUuid})
        if(typeof shelf === "undefined"){
            return res.status(StatusCodes.NOT_ACCEPTABLE).send({reason:"No Shelf with given ID exist"})
        }
        if(["stored","moving"].some(e => e === itemToPatch.status)){
            itemToPatch.status = "stored"
        }
        await Database.connection.getRepository(Item).save(itemToPatch);
        await Database.connection.createQueryBuilder().relation(Shelf, "items").of(shelf).add(itemToPatch);
    }
}