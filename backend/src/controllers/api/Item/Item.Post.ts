import { Response } from "express";
import { StatusCodes } from "http-status-codes";
import Database from "../../../database/Database";
import { Item, StatusEnum } from "../../../database/model/Item";

import { ItemType } from "../../../database/model/ItemType";
import { Shelf } from "../../../database/model/Shelf";
import { updateShelfCapacity } from "../Shelf/Shelf.Helper";

export async function createItem(itemtypeId: string, res: Response,shelfId?: string, status?: string, notes?:string){
    const type = await Database.connection.getRepository(ItemType).findOne({id: Number(itemtypeId)})
    if(typeof type === "undefined"){
        return res.status(StatusCodes.NOT_ACCEPTABLE).send({reason:"Could not find Item Type with the ID "+itemtypeId})
    }
    const itemToCreate = Database.connection.getRepository(Item).create();
    let shelfToAddTo : Shelf | undefined;
    if(shelfId){
        shelfToAddTo = await Database.connection.getRepository(Shelf).findOne({uuid: shelfId});
        if(typeof shelfToAddTo === "undefined"){
            return res.status(StatusCodes.NOT_ACCEPTABLE).send({reason:"Could not find Shelf with given ID "})
        }else{
            itemToCreate.shelf = Promise.resolve(shelfToAddTo);
        }
    }
    itemToCreate.type = Promise.resolve(type);
    if(notes){
        itemToCreate.notes = notes;
    }
    if(status){
        itemToCreate.status = status as StatusEnum;
    }else{
        itemToCreate.status = typeof shelfId === "undefined" ? "moving" : "stored"
    }
    await Database.connection.getRepository(Item).save(itemToCreate);
    if(shelfId){
        await updateShelfCapacity(shelfId);
    }
    return res.status(StatusCodes.OK).send({reason:"Created successfully"})
}