import { Request, Response } from "express";
import { StatusCodes } from "http-status-codes";
import Database from "../../../database/Database";
import { Item } from "../../../database/model/Item";
import { updateShelfCapacity } from "../Shelf/Shelf.Helper";

export async function removeItem(req: Request, res: Response){
    let itemToDelete = await Database.connection.getRepository(Item).findOne({uuid: req.params.uuid})
    const shelf = await itemToDelete?.shelf;
    if(!itemToDelete){
        return res.status(StatusCodes.NOT_FOUND).send({reason:"Could not find item with that ID"})
    }
    if(itemToDelete.status == "reserved"){
        return res.status(StatusCodes.FORBIDDEN).send({reason:"This item has been marked as reserved. Please change status before deleting."})
    }
    if(!await itemToDelete.shelf){
        itemToDelete.shelf = undefined;
    }
    // Save first to unlink
    itemToDelete = await Database.connection.getRepository(Item).save(itemToDelete);
    // Then Delete
    await Database.connection.getRepository(Item).remove(itemToDelete);
    if(shelf){
        await updateShelfCapacity(shelf.uuid);
    }
    return res.status(StatusCodes.OK).send({reason:"Deleted successfully."});
}