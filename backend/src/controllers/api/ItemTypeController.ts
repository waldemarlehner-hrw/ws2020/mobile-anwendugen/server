import { ClassErrorMiddleware, ClassWrapper, Controller, Delete, Get, Middleware, Patch, Post } from "@overnightjs/core";
import { StatusCodes } from "http-status-codes";
import Database from "../../database/Database";
import { ItemType } from "../../database/model/ItemType";
import { Authenticate } from "../../decorators/Authenticate";
import { DefaultErrorHandler } from "../../errorhandlers/DefaultErrorHandler";
import {Request, Response} from "express"
import { trimBody, trimParams } from "../../middleware/AutoTrim";
import { param, body } from "express-validator"
import { generateResponseObjectOnError } from "../../helpers/ErrorResponseHelper";
import {getItemById as getTypeById, getItemsTypesByQuery} from "./ItemType/ItemType.Get"
import postItemType from "./ItemType/ItemType.Post";
import { deleteType } from "./ItemType/ItemType.Delete";
import { patchType } from "./ItemType/ItemType.Patch";
import asyncWrapper from "../../helpers/AsyncWrapper";

@Controller("api/itemtypes")
@ClassErrorMiddleware(DefaultErrorHandler)
@ClassWrapper(asyncWrapper)
export default class ItemTypeController{
	@Get(":uuid")
	@Authenticate("regular")
	@Middleware([
		param("uuid").isInt({min: 0}).optional(false),
		param("includeItems").isBoolean().optional(true)
	])
	async getItemTypeById(req: Request, res: Response) : Promise<Response>{
		const errorResponse = generateResponseObjectOnError(req,res);
		if(errorResponse){
			return errorResponse;
		}
		const id = Number(req.params.uuid);
		const includeItems = req.query.includeItems == "true" ?? false;
		return await getTypeById(id, res, includeItems)
	}

	@Get("")
	@Authenticate("regular")
	@Middleware([
		param("name").isString().optional(true),
		param("onlyUsed").isBoolean().optional(true)
	])
	async getAllItemTypes(req: Request, res: Response) : Promise<Response>{
		console.warn(req.query)
		const errorResponse = generateResponseObjectOnError(req,res);
		if(errorResponse){
			return errorResponse;
		}
		const onlyUsed = (req.query.onlyUsed as string | undefined)?.toLowerCase().trim() == "true" ?? false
		return await getItemsTypesByQuery({name: req.query.name as string | undefined, onlyUsed: onlyUsed }, res)
	}


	@Post("")
	@Authenticate("manager")
	@Middleware([
		trimBody,
		body("name").isString().isLength({min: 5, max: 255}).optional(false),
		body("description").isString().isLength({max: 1024}).optional(false),
		body("capacity").isNumeric().optional(false)
	])
	async addItemType(req: Request, res: Response){
		const errorResponse = generateResponseObjectOnError(req,res);
		if(errorResponse){
			return errorResponse;
		}
		
		const itemtype = await Database.connection.getRepository(ItemType).findOne({name: req.body.name});
		if(itemtype){
			return res.status(StatusCodes.BAD_REQUEST).send({
				reason:"There already is a ItemType with that name."
			});
		}

		await postItemType(req.body.name, req.body.description, Number(req.body.capacity));
		return res.status(StatusCodes.OK).send({reason:"Added successfully"});
	}

	@Delete(":id")
	@Authenticate("manager")
	@Middleware([
		trimParams,
		param("id").isInt({min:0}).optional(false)
	])
	async deleteItemType(req: Request, res: Response): Promise<Response>{
		const errorResponse = generateResponseObjectOnError(req,res);
		if(errorResponse){
			return errorResponse;
		}

		const didDelete = await deleteType(Number(req.params.id));
		const message = didDelete ? "Deleted successfully." : "Could not find ItemType."
		return res.status(didDelete ? StatusCodes.OK : StatusCodes.NOT_FOUND).send({reason: message});
	}

	@Patch(":id")
	@Authenticate("manager")
	@Middleware([
		trimParams,
		param("id").isInt({min: 0}).optional(false),
		body("name").isString().isLength({min: 5, max: 255}).optional(true),
		body("description").isString().isLength({max: 1024}).optional(true),
		body("capacity").isNumeric().optional(true)
	])
	async patchItemType(req: Request, res: Response) : Promise<Response>{
		const errorResponse = generateResponseObjectOnError(req,res);
		if(errorResponse){
			return errorResponse;
		}

		return await patchType(req.params.id, req.body.name, res, req.body.description, req.body.capacity);
	}
}