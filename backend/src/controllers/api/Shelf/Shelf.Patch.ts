
import { StatusCodes } from 'http-status-codes';
import { Response } from "express";
import Database from "../../../database/Database";
import { Shelf } from "../../../database/model/Shelf";

export async function patchShelf(uuid: string, {name, capacity} : {name?: string, capacity?: number}, res: Response){
    const shelfToPatch = await Database.connection.getRepository(Shelf).findOne({uuid: uuid});
    if(shelfToPatch){
        if(typeof name === "undefined" && typeof capacity === "undefined"){
            return res.status(StatusCodes.NOT_MODIFIED).send({reason:"Not modified because no patch args were supplied"})
        }
        if(name){
            shelfToPatch.name = name;
        }
        if(capacity){
            shelfToPatch.capacity = capacity;
        }
        await Database.connection.getRepository(Shelf).save(shelfToPatch);
        return res.status(StatusCodes.OK).send({reason:"Patch successful."})
    }else{
        return res.status(StatusCodes.NOT_FOUND).send({reason:"Cannot find Shelf with the given ID"})
    }
}