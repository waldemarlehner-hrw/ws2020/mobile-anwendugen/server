import { Response } from "express";
import { StatusCodes } from "http-status-codes";
import Database from "../../../database/Database";
import { Shelf } from "../../../database/model/Shelf";

export async function deleteShelf(uuid: string, res: Response){
    const shelf = await Database.connection.getRepository(Shelf).findOne({uuid: uuid})
    if(shelf){
        const items = (await shelf.items);
        if(items.length > 0){
            return res.status(StatusCodes.CONFLICT).send({reason:"Shelf has items. You can the IDs from the Data Array. Relocate them from the shelf or delete them first.", data: items.map(e => e.uuid)})
        }else{
            await Database.connection.getRepository(Shelf).delete({uuid: uuid})
            return res.status(StatusCodes.OK).send({reason: "Deleted successfully."})
        }
    }else{
        return res.status(StatusCodes.NOT_FOUND).send({reason:"Cannot delete: No Shelf with that ID found."})
    }
}