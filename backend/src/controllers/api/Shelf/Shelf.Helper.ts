import Database from "../../../database/Database";
import { Shelf } from "../../../database/model/Shelf";

export async function updateShelfCapacity(uuid: string){
    const shelf = await Database.connection.getRepository(Shelf).findOne({uuid});
    if(shelf){
        const items = await shelf.items
        let sumCapacity = 0;
        for(const item of items){
            sumCapacity += (await (item.type)).capacity
        } 
        shelf.capacityUsed = sumCapacity;
        console.warn(`used shelf capacity old:${shelf.capacityUsed}; new:${sumCapacity}`)
        await Database.connection.getRepository(Shelf).save(shelf);
    }
}