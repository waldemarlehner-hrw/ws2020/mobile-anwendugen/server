import { Request, Response } from "express";
import { StatusCodes } from "http-status-codes";
import { Like } from "typeorm";
import Database from "../../../database/Database";
import { Item } from "../../../database/model/Item";
import { Shelf } from "../../../database/model/Shelf";

export async function getOne(req: Request, res: Response){
    const entry = await Database.connection.getRepository(Shelf).findOne({uuid: req.params.uuid})
    if(entry){
        const withItems = (req.query.withItems ?? false) == "true"
        const returnObject = mapShelf(entry, withItems);
        res.status(StatusCodes.OK).send({data: await returnObject})
    }else{
        return res.status(StatusCodes.NOT_FOUND).send({reason: "No Shelf with that ID exists"})
    }
}

export async function getMany(onlyUsed: boolean, nameLike: string, res: Response){
    const repo = Database.connection.getRepository(Shelf);

    const queryBuilder = repo.createQueryBuilder("shelf");
    if(nameLike.length > 0){
        queryBuilder.where("shelf.name LIKE :name", {name: nameLike+"%"})    
    }
    let returnObject: any = {}
    if(onlyUsed){
        queryBuilder.innerJoinAndSelect("shelf.items", "item");
        queryBuilder.groupBy("shelf.uuid")
        queryBuilder.having("COUNT(item.uuid) > 0");
    }

    returnObject = (await queryBuilder.execute() as any[]).map(e => {
        console.warn(e)
        return {
            name: e.shelf_name,
            uuid: e.shelf_uuid,
            capacityUsed: e.shelf_capacityUsed,
            capacity: e.shelf_capacity
        }
    })

    if(returnObject.length > 0){
        return res.status(StatusCodes.OK).send({data: returnObject});
    }else{
        return res.status(StatusCodes.NOT_FOUND).send({reason: "No items found with the given filters.", data: []})
    }
}

async function getType(e : Item){
    const type = await e.type
    return {item: e, type: type}
}

async function mapShelf(shelf: Shelf, withItems? : boolean) : Promise<IShelfEntry>{
    let itemsRaw = (await shelf.items)
    const extractedType = await Promise.all(itemsRaw.map(e => getType(e)))
    
    const items = extractedType.map(e => {
        return {
            uuid: e.item.uuid,
            ...e.item.notes && {notes: e.item.notes},
            status: e.item.status,
            itemtype: {
                uuid: e.type.id,
                name: e.type.name,
                description: e.type.description
            }
        }
    })

    return {
        name: shelf.name,
        uuid: shelf.uuid,
        capacity: shelf.capacity,
        capacityUsed: shelf.capacityUsed,
        itemCount: items.length,
        ...withItems && {
            items: items
        }
    }
}

export interface IShelfEntry{
    name: string,
    uuid: string,
    capacity: number,
    capacityUsed: number,
    itemCount?: number,
    items?: {
        uuid: string,
        notes?: string,
        itemtype: {
            uuid: number,
            name: string,
            description: string
        }
    }[]
}
