import { Response } from "express";
import { StatusCodes } from "http-status-codes";
import Database from "../../../database/Database";
import { Shelf } from "../../../database/model/Shelf";

export async function createShelf(name: string, capacity: number, res: Response){
    const newShelf = Database.connection.getRepository(Shelf).create()
    newShelf.capacityUsed = 0;
    newShelf.capacity = capacity;
    newShelf.name = name;

    await Database.connection.getRepository(Shelf).save(newShelf);
    return res.status(StatusCodes.OK).send({reason:"Created successfully."})
}