import { ClassErrorMiddleware, ClassWrapper, Controller, Delete, Get, Middleware, Patch, Post } from '@overnightjs/core';
import {Response, Request} from "express"
import {AllRolesByPrecedence } from "../../database/RoleEnum"
import {Authenticate} from "../../decorators/Authenticate"
import { param, body } from 'express-validator';
import {DefaultErrorHandler} from "../../errorhandlers/DefaultErrorHandler"
import { trimParams } from '../../middleware/AutoTrim';
import { generateResponseObjectOnError } from '../../helpers/ErrorResponseHelper';
import GetUser from './User/User.Get';
import PostUser from "./User/User.Post"
import DeleteUser from "./User/User.Delete"
import PatchUser from "./User/User.Patch"
import asyncWrapper from '../../helpers/AsyncWrapper';

@Controller("api/users")
@ClassErrorMiddleware(DefaultErrorHandler)
@ClassWrapper(asyncWrapper)
export default class UserController{
	@Get(":uuid")
	@Authenticate("manager")
	@Middleware([
		trimParams,
		param("uuid").isUUID("all").optional(false),
	])
	async getUserById(req: Request, res: Response){
		console.info(req.query)
		const errorResponse = generateResponseObjectOnError(req,res);
		if(errorResponse){
			return errorResponse;
		}
		return await GetUser.getById(req.params.uuid,res);
	}
	

	@Get("")
	@Authenticate("manager")
	@Middleware([
		trimParams,
		param("uuid").isUUID("all").optional(true),
		param("username").optional(true),
		param("firstname").optional(true),
		param("lastname").optional(true),
		param("roles").optional(true).isArray()
	])
	async getAllUsers(req: Request, res: Response){
		console.info(req.query)
		const errorResponse = generateResponseObjectOnError(req,res);
		if(errorResponse){
			return errorResponse;
		}
		return await GetUser.getAllUsers(req,res);
	}

	@Post("")
	@Authenticate("manager")
	@Middleware([
		body("username").isLength({min:5, max: 64}),
		body("firstname").isLength({max: 256}),
		body("lastname").isLength({max: 256}),
		body("password").isLength({min:8}),
		body("role").isIn(AllRolesByPrecedence)
	])
	async postUser(req: Request, res: Response){
		const errorResponse = generateResponseObjectOnError(req,res);
		if(errorResponse){
			return errorResponse;
		}

		return await PostUser.postUser(req,res);
	}

	@Delete(":uuid")
	@Authenticate("manager")
	@Middleware([
		trimParams,
		param("uuid").isUUID("4")
	])
	async deleteUser(req: Request, res: Response){
		const errorResponse = generateResponseObjectOnError(req,res);
		if(errorResponse){
			return errorResponse;
		}

		return await DeleteUser.deleteUser(req,res);
	}

	@Patch("self")
	@Middleware([
		/*trimBody*/ // cannot trim body because the password fields might start/end with spaces.
		body("username").isLength({min:5, max: 64}).optional(),
		body("firstname").isLength({max: 256}).optional(),
		body("lastname").isLength({max: 256}).optional(),
		body("password").isLength({min:8}).optional(),
		body("oldpassword").isLength({min: 8}).optional(),
	])
	async patchUserSelf(req: Request, res: Response){
		const errorResponse = generateResponseObjectOnError(req,res);
		if(errorResponse){
			return errorResponse;
		}
		return await PatchUser.patchUserSelf(req,res);
	}

	@Patch(":uuid")
	@Authenticate("manager")
	@Middleware([
		/*trimBody*/ // cannot trim body because the password fields might start/end with spaces.
		trimParams,
		param("uuid").isUUID("4"),
		body("username").isLength({min:5, max: 64}).optional(),
		body("firstname").isLength({max: 256}).optional(),
		body("lastname").isLength({max: 256}).optional(),
		body("password").isLength({min:5}).optional(),
	])
	async patchUser(req: Request, res: Response){
		const errorResponse = generateResponseObjectOnError(req,res);
		if(errorResponse){
			return errorResponse;
		}
		return await PatchUser.patchUser(req,res);
	}
}