import { ClassErrorMiddleware, ClassWrapper, Controller, Middleware, Post } from "@overnightjs/core";
import { body } from "express-validator";
import { Authenticate } from "../../decorators/Authenticate";
import { DefaultErrorHandler } from "../../errorhandlers/DefaultErrorHandler";
import asyncWrapper from "../../helpers/AsyncWrapper";
import { Request, Response } from "express";
import { generateResponseObjectOnError } from "../../helpers/ErrorResponseHelper";
import Database from "../../database/Database";
import { Shelf } from "../../database/model/Shelf";
import { Item } from "../../database/model/Item";
import { ItemType } from "../../database/model/ItemType";
import QRCode from "qrcode";
import ejs from "ejs";
import { readFile } from "fs";
import { promisify } from "util";
import pdf from "html-pdf"
import path from "path"
import { StatusCodes } from "http-status-codes";


@Controller("api/qrgenerator")
@ClassErrorMiddleware(DefaultErrorHandler)
@ClassWrapper(asyncWrapper)
export default class PrintQueueController{
    @Post("pdf")
    @Authenticate("regular")
    @Middleware(
        body("data").isArray().optional(false).custom(e => {
            if(!Array.isArray(e)){
                throw new Error("Data must be an Array {uuid, type}[]");
            }
            const v4 = new RegExp(/^[0-9A-F]{8}-[0-9A-F]{4}-4[0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i);
            for(const entry of e){
                if(typeof entry.type !== "string" && !["shelf","item"].some(e => entry.type === e)){
                    throw new Error("Missing Type.")
                }
                if(typeof entry.uuid !== "string"){
                    throw new Error("UUID needs to be astring");
                }
                if(!(entry.uuid as string).match(v4)){
                    throw new Error("Entry UUID is not a valid UUIDv4")
                }
            }
            return true;
        })
    )
    async createPdf(req: Request, res: Response){
        const errs = generateResponseObjectOnError(req,res);
        if(errs){
            return errs;
        }
        let data: QRData[] = [];
        let errors: string[] = [];
        for(const entry of req.body.data){
            let functionToUse;
            if(entry.type === "shelf"){
                functionToUse = handleShelf;
            }else{
                functionToUse = handleItem;
            }

            await functionToUse(data,errors, entry.uuid);
        }
        console.info(data)
        console.warn(data.length)
        const unresolvedPromises = data.map(e => createQRImage(e))
        const converted = await Promise.all(unresolvedPromises)
        console.info(converted.length)
        const readFilePromise = promisify(readFile);
        const ejsTemplate = (await readFilePromise(path.join(__dirname,"QRCodes/template.ejs"))).toString();
        const html = ejs.render(ejsTemplate, {data:converted})
        // https://stackoverflow.com/a/43456725/7583353
        pdf.create(html, {
            format: "A4",
            orientation: "portrait"
        }).toStream((err,stream) => {
            if(err){
                console.error(err);
                return res.status(StatusCodes.INTERNAL_SERVER_ERROR).send({data: err, reason: "Failed to create PDF"})
            }else{
                res.status(StatusCodes.OK);
                res.contentType("application/x-pdf");
                res.setHeader("Content-Disposition", "attachment; filename=qrcode.pdf");
                stream.on("end", ()=> {
                    return res.end();
                })
                stream.pipe(res);
            }
        })
    }
}


interface QRData{
    data:string,
    mainText: string,
    subText: string
}

interface QRDataBase64{
    base64URI: string,
    mainText: string,
    subText: string
}

async function handleShelf(data: QRData[],errors : string[], uuid: string){
    const shelf = await Database.connection.getRepository(Shelf).findOne({uuid})
    if(shelf){
        data.push({
            data: "s"+uuid,
            mainText: shelf.name,
            subText: "Shelf::"+uuid.substr(0,5)
        })
    }else{
        errors.push("Shelf ID "+uuid+" not found.")
    }
}

async function handleItem(data: QRData[],errors: string[], uuid: string){
    const item = await Database.connection.getRepository(Item).findOne({uuid});
    if(item){
        const itemType = await item.type as ItemType;
        data.push({
            data:"i"+uuid,
            mainText: itemType.name,
            subText: "Item::"+uuid.substr(0,5)
        })
    }
}

async function createQRImage(data: QRData) : Promise<QRDataBase64>{
    
    const base64 = await QRCode.toDataURL(data.data);
    return {
        subText: data.subText,
        mainText: data.mainText,
        base64URI: base64
    }
}