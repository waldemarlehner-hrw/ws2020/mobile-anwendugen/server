import {StatusCodes} from "http-status-codes"
import { ClassErrorMiddleware, ClassWrapper, Controller, Delete, Get, Post } from '@overnightjs/core';
import {Response, Request} from "express"
import { ExpressSessionData } from '../../interfaces/ExpressSessionData';
import Database from '../../database/Database';
import { User } from '../../database/model/User';
import { DefaultErrorHandler } from "../../errorhandlers/DefaultErrorHandler";
import asyncWrapper from "../../helpers/AsyncWrapper";

@Controller("api/auth")
@ClassErrorMiddleware(DefaultErrorHandler)
@ClassWrapper(asyncWrapper)
export default class AuthController{
    
	@Get()
	async getCurrentAuth(req: Request, res: Response){
		const session = <ExpressSessionData>req.session;
		if(!session.currentUserId){
			//No Session -> No Login
			return res.status(StatusCodes.NOT_FOUND).send({reason:"Not logged in"});
		}
		else{
			try{
				const user = await Database.connection.getRepository(User).findOneOrFail({
					uuid: session.currentUserId
				});
				
				const dataObject:any = {
					username: user.username,
					firstname: user.firstName,
					lastname: user.lastName,
					role: user.role,
					uuid: user.uuid
				};

				return res.status(StatusCodes.OK).send({
					reason: "Logged in",
					data: dataObject
				})
			}
			catch{
				// User not found -> clear session
				req.session.destroy(()=>{});
				return res.status(StatusCodes.INTERNAL_SERVER_ERROR).send({
					reason:"Logged in for a user that does not exist. This may occur when the user was just removed. The session has been invalidated and you may log in now."
				});
			}
		}
	}

	@Delete()
	async deleteCurrentSession(req: Request, res: Response){
		const session =  <ExpressSessionData>req.session;
		if(session?.currentUserId){
			try{
				await new Promise<void>((resolve, reject )=> {
					req.session.destroy(e => {
						if(e){
							reject();
						}else{
							resolve();
						}
					})
					return res.status(StatusCodes.OK).send({reason:"Session invalidated."})
				})
			}
			catch(err){
				return res.status(StatusCodes.INTERNAL_SERVER_ERROR).send({reason:"Could not delete session."})
			}
		}else{
			return res.status(StatusCodes.OK).send({reason:"You were not logged in."})
		}
	}

	@Post()
	async createNewSession(req: Request, res: Response){
		const params = this.parseUsernameAndPassword(req.body?.username, req.body?.password);
		if(params.errorList.length > 0){
			return res.status(StatusCodes.BAD_REQUEST).send({reason: "One or multiple errors. See attached errors array.", errors: params.errorList})
		}
		else{
			// Check if user exists
			const user = await Database.connection.getRepository(User).findOne({username: params.username})
			if(user){
				// User exists. Check password
				if(user.passwordVerify(params.password ?? "")){
					// Password matches hash
					(req.session as any).currentUserId = user.uuid
					return res.status(StatusCodes.OK).send({reason:"Log in successful"});
				}else{
					// No match
					return res.status(StatusCodes.NOT_FOUND).send({reason:"Username and password do not match"})
				}
			}
			else{
				// User was not found
				return res.status(StatusCodes.NOT_FOUND).send({reason:"Username and password do not match"})
			}
		}
	}

	private parseUsernameAndPassword(username: unknown, password: unknown){
		const [usernameMaybe, usernameError] = this.parseUserName(username);
		const [passwordMaybe, passwordError] = this.parsePassword(password);

		const returnObject : {username?: string, password?: string, errorList: string[]} = {
			errorList: [...usernameError,...passwordError],
			...usernameMaybe && {username: usernameMaybe},
			...passwordMaybe && {password: passwordMaybe}
		}
		return returnObject;
	}

	private parsePassword(password: unknown) : [string | undefined, string[]]{
		if(typeof password != "string"){
			return [undefined, ["Given Username is not a string"]]
		}else{
			const asStringtrimmed = (password as string).trim();
			if(asStringtrimmed.length < 8){
				return [undefined, ["Username needs to be at least 8 Characters long"]]
			}else{
				return [asStringtrimmed, []]
			}
		}
	}

	private parseUserName(username: unknown) : [string | undefined, string[]]{
		if(typeof username != "string"){
			return [undefined, ["Given Username is not a string"]]
		}else{
			const asStringtrimmed = (username as string).trim();
			if(asStringtrimmed.length < 5){
				return [undefined, ["Username needs to be at least 5 Characters long"]]
			}
			if(asStringtrimmed.length > 64){
				return [undefined, ["Username cannot be longer than 64 characters."]]
			}else{
				return [asStringtrimmed, []]
			}
		}
	}

	

}