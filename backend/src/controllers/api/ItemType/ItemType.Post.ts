import { ItemType } from "../../../database/model/ItemType"
import Database from "../../../database/Database"

export default async function postItemType(name: string, description: string, capacity: number){
    const type = Database.connection.getRepository(ItemType).create({
        description: description,
        name: name,
        capacity: capacity
    });
    Database.connection.getRepository(ItemType).save(type);
    return;
}