import { ItemType } from "../../../database/model/ItemType"
import Database from "../../../database/Database"

export async function deleteType(id : number) : Promise<boolean>{
    const type = await Database.connection.getRepository(ItemType).findOne({id: id});
    if(type){
        await Database.connection.getRepository(ItemType).remove(type);
        return true;
    }else{
        return false;
    }
}