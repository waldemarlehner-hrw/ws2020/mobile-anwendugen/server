import { StatusCodes } from 'http-status-codes';
import { Response } from "express";
import { ItemType } from "../../../database/model/ItemType"
import Database from "../../../database/Database"
import { Like } from "typeorm";
export async function patchType(id: string, name: string | undefined,res : Response, description: string | undefined, capacity: string | undefined){
    const objectToUpdate = await Database.connection.getRepository(ItemType).findOne({id: Number(id)});
		if(objectToUpdate){
			const repo = Database.connection.getRepository(ItemType);
			
			if(name){
				objectToUpdate.name = name;
			}
			if(description){
				objectToUpdate.description = description;
			}
			if(capacity){
				objectToUpdate.capacity = Number(capacity);
			}
			await repo.save(objectToUpdate);
			return res.status(StatusCodes.OK).send({
				reason: "Patch successful."
			});
		}else{
			return res.status(StatusCodes.NOT_FOUND).send({
				reason: "No ItemType with the given Id exists."
			});
		}
}