import { StatusCodes } from 'http-status-codes';
import { Response } from "express";
import { ItemType } from "../../../database/model/ItemType"
import Database from "../../../database/Database"
import { Like } from "typeorm";
import { Item } from '../../../database/model/Item';

export async function getItemById(id: number, res:Response, addItems: boolean){
    let type = await Database.connection.getRepository(ItemType).findOne({id: id});

    if(type){
        let responseObject : any = await mapType(type);
        if(addItems){
            const response = await type.items;
            const items = await Promise.all(response.map(e => mapItem(e)));
            responseObject.items = items;
        }
        return res.status(StatusCodes.OK).send({data: responseObject})
    }else{
        return res.status(StatusCodes.NOT_FOUND).send({reason:"No ItemType with that ID exists."})
    }
}


export async function getItemsTypesByQuery({name, onlyUsed} : {name?:string, onlyUsed?:boolean}, res: Response) : Promise<Response> {
    let types : ItemType[];
    console.info(onlyUsed)
    if(name){
        types = await Database.connection.getRepository(ItemType)
        .find({name: Like(name+"%")});
        if(onlyUsed){
            types = await filterArray(types);
        }
    }else{
        types = await Database.connection.getRepository(ItemType).find();
        if(onlyUsed){
            types = await filterArray(types);
        }
    }
    const data = [];
    for(let type of types){
        data.push(await mapType(type))
    }
    return res.status(types.length > 0 ? StatusCodes.OK : StatusCodes.NOT_FOUND).send({data: data})
}

async function mapType(e: ItemType){
    const itemCount = (await e.items).length
    const ordersCount = (await e.orders).length
    return {
        uuid: e.id,
        name: e.name,
        description: e.description,
        capacity: e.capacity,
        ordersCount: ordersCount,
        itemCount: itemCount
    }
}
async function filterArray(e: ItemType[]) : Promise<ItemType[]>{
    const returnArray: ItemType[] = []
    for(const entry of e){
        if(await filterUsedOnly(entry)){
            returnArray.push(entry)
        }
    }
    return returnArray;
}

async function filterUsedOnly(e : ItemType){
    return (await e.items).length > 0 || (await e.orders).length > 0
}

async function mapItem(e : Item){
    const shelf = await e.shelf;
    return {
        uuid: e.uuid,
        status: e.status,
        ...e.notes && {notes: e.notes},
        ...shelf && {
            shelf: {
                uuid: shelf.uuid,
                name: shelf.name
            }
        }
    }
}
