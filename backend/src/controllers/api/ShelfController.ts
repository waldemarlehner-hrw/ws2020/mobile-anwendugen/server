import { ClassErrorMiddleware, ClassWrapper, Controller, Delete, Get, Middleware, Patch, Post } from "@overnightjs/core";
import { Request, Response } from "express";
import { body, param } from "express-validator";
import { Authenticate } from "../../decorators/Authenticate";
import { DefaultErrorHandler } from "../../errorhandlers/DefaultErrorHandler";
import asyncWrapper from "../../helpers/AsyncWrapper";
import { generateResponseObjectOnError } from "../../helpers/ErrorResponseHelper";
import { trimBody, trimParams } from "../../middleware/AutoTrim";
import { deleteShelf } from "./Shelf/Shelf.Delete";
import { getMany, getOne } from "./Shelf/Shelf.Get";
import { patchShelf } from "./Shelf/Shelf.Patch";
import { createShelf } from "./Shelf/Shelf.Post";

@Controller("api/shelves")
@ClassErrorMiddleware(DefaultErrorHandler)
@ClassWrapper(asyncWrapper)
export default class ShelfController{
    
    @Get(":uuid")
    @Authenticate("regular")
    @Middleware([
        param("uuid").isUUID(4).optional(false),
        param("withItems").isBoolean().optional(true)
    ])
    async getById(req: Request, res: Response){
        const errors = generateResponseObjectOnError(req,res);
        if(errors){
            return errors;
        }
        return await getOne(req, res);
    }
    
    @Get("")
    @Authenticate("regular")
    @Middleware([
        trimParams,
        param("nameLike").isString().optional(true),
        param("noEmpty").isBoolean().optional(true)
    ])
    async getAll(req: Request, res: Response){
        const errors = generateResponseObjectOnError(req,res);
        if(errors){
            return errors;
        }
        console.info(req.query)
        const nameLike = typeof req.query.nameLike === "string" ? req.query.nameLike.trim() : ""
        const noEmpty = req.query.noEmpty == "true"
        return await getMany(noEmpty, nameLike, res);
    }

    @Delete(":uuid")
    @Authenticate("manager")
    @Middleware([
        trimParams,
        param("uuid").isUUID(4).optional(false)
    ])
    async deleteById(req: Request, res: Response){
        const errors = generateResponseObjectOnError(req,res);
        if(errors){
            return errors;
        }

        return await deleteShelf(req.params.uuid, res);
    }

    @Patch(":uuid")
    @Authenticate("manager")
    @Middleware([
        trimParams,
        trimBody,
        param("uuid").isUUID(4).optional(false),
        body("name").isString().optional(true),
        body("capacity").isFloat({min: 0}).optional(true)
    ])
    async patchById(req: Request, res: Response){
        const errors = generateResponseObjectOnError(req,res);
        if(errors){
            return errors;
        }

        return await patchShelf(req.params.uuid, req.body, res);
    }

    @Post("")
    @Authenticate("manager")
    @Middleware([
        trimBody,
        body("name").isString().optional(false),
        body("capacity").isString().optional(false)
    ])
    async postShelf(req: Request, res: Response){
        const errors = generateResponseObjectOnError(req,res);
        if(errors){
            return errors;
        }

        return await createShelf(req.body.name, req.body.capacity, res);
    }
}