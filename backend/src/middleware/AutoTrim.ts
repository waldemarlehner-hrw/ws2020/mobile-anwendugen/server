import { Request, Response } from 'express';
// from https://stackoverflow.com/a/54556626

export function trimBody(req : Request, _ : Response, next : Function){
	for(const [key,value] of Object.entries(req.body)){
		if(typeof(req.body[key]) === "string"){
			req.body[key] = (value as string).trim();
		}
	}
	next();
}

export function trimParams(req : Request, _ : Response, next : Function){
	for(const [key,value] of Object.entries(req.params)){
		if(typeof(req.params[key]) === "string"){
			req.params[key] = (value as string).trim();
		}
	}
	for(const [key,value] of Object.entries(req.query)){
		if(typeof(req.query[key]) === "string"){
			req.query[key] = (value as string).trim();
		}
	}
	next();
}