export type RoleEnum = "regular" | "manager" | "admin" | "root";

export function isRoleEnum(string :string) : boolean{
	return AllRolesByPrecedence.some( (e) => e === string)
}

/**
 * Checks which role has the higher precedence.
 * @param r1 First Role
 * @param r2 Second Role
 * @returns A number indicating which role is higher. `return < 0` means r1 is higher, `return > 0` means r2 is higher, `return == 0` means that both are at the same level.
 */
export function compareRoles(r1: RoleEnum, r2: RoleEnum): number{
	const pos1 = AllRolesByPrecedence.indexOf(r1);
	const pos2 = AllRolesByPrecedence.indexOf(r2);
	return pos1-pos2;
}

export const AllRolesByPrecedence = ["root", "admin", "manager", "regular"]