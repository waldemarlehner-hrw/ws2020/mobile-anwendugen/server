import {sha512} from "js-sha512"
import {async as salter} from "crypto-random-string"

export class Crypto{
	private constructor(){}

	public static async generate(password: string, saltLen?: number) : Promise<HashPair> {
		const salt = await this.generateSalt(saltLen ?? 64);
		const hash =  sha512(salt+password);
		return {salt, hash}
	}

	public static verify(password: string, hashpair : HashPair) : boolean {
		const {hash, salt} = hashpair;
		return sha512(salt+password) === hash;
	}

	public static generateRandomString(len: number) : Promise<string> {
		return this.generateSalt(len);
	}

	private static generateSalt(len: number) : Promise<string>{
		return salter({length: len })
	}
}


export type HashPair = {
	hash: string,
	salt: string
}