import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { User } from './User';

@Entity()
export class Device{

	@PrimaryGeneratedColumn("uuid")
	uuid!:string;

	@Column("varchar",{length: 128})
	hash!:string;

	@Column("varchar",{length:64})
	salt!:string;

	@ManyToOne(()=>User, user => user.devices)
	user!:User;

}