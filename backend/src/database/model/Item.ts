import { Column, Entity, JoinTable, ManyToMany, ManyToOne, OneToMany, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { ItemType } from './ItemType';
import { Shelf } from './Shelf';

@Entity()
export class Item {
	@PrimaryGeneratedColumn("uuid")
	uuid!: string;

	@Column("varchar", { length: 255 , default: ""})
	notes!: string;

	@Column({
		type: "simple-enum",
		enum: ["stored","moving","outofsystem","lost","reserved"],
		default: "stored"
	})
	status!: StatusEnum

	@ManyToOne(()=>ItemType,type => type.items, {eager: false})
	type!: Promise<ItemType>;

	@ManyToOne(()=>Shelf, shelf => shelf.items, {cascade: ["update"], nullable: true})
	shelf?: Promise<Shelf>;

}


export type StatusEnum = "stored" | "moving" | "outofsystem" | "lost" | "reserved";