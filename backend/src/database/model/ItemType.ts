import { Column, Entity, ManyToMany, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Item } from './Item';
import { OrderToItemType } from './OrderToItemType';

@Entity()
export class ItemType{
	@PrimaryGeneratedColumn()
	id!: number;

	@Column("varchar", {length: 255})
	name!: string;

	@Column("varchar", {length: 1024})
	description!: string;

	@Column("float")
	capacity!: number;

	@OneToMany(() => Item, item => item.type)
	items!: Promise<Item[]>;

	@OneToMany(()=>OrderToItemType, itemTypes => itemTypes.itemTypes)
	orders!: Promise<OrderToItemType[]>
}