import { Entity, ManyToMany, OneToMany, ManyToOne, PrimaryGeneratedColumn, Column } from "typeorm";
import { ItemType } from './ItemType';
import { Order } from './Order';

@Entity()
export class OrderToItemType{

	@PrimaryGeneratedColumn()
	OrderToItemId!:number

	@ManyToOne(()=>Order, order => order.itemTypes)
	orders!: Order

	@ManyToOne(()=>ItemType, type => type.orders)
	itemTypes!: ItemType

	@Column("int")
	count!: number;
}