import { Column, Entity, ManyToMany, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Item } from './Item';

@Entity()
export class Shelf{
	@PrimaryGeneratedColumn("uuid")
	uuid!:string;

	@Column()
	name!:string;

	@Column("float")
	capacity!:number;

	@Column("float")
	capacityUsed!:number;

	@OneToMany(()=>Item, item => item.shelf)
	items!:Promise<Item[]>;

}