import { Column, Entity, JoinColumn, OneToMany, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import { OrderToItemType } from './OrderToItemType';
import { User } from './User';

@Entity()
export class Order{
	
	@PrimaryGeneratedColumn("uuid")
	uuid!:string;

	@OneToMany(()=>OrderToItemType, orders => orders.orders)
	itemTypes!:Promise<OrderToItemType[]>;

	@OneToOne(()=>User)
	@JoinColumn()
	assignedUser?: User;

	@Column({
		type: "simple-enum",
		enum: ["todo", "doing", "done"],
		default: "todo"
	})
	status!: OrderStatus;

}

export type OrderStatus = "todo" | "doing" | "done"