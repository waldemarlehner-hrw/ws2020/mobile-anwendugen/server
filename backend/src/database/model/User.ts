import { AfterLoad, Column, Entity, OneToMany, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Crypto } from '../helpers/Crypto';
import { Device } from './Device';
import { Order } from './Order';
import {RoleEnum, AllRolesByPrecedence} from "../RoleEnum"

@Entity()
export class User{

	/************** MODEL ****************/
	@PrimaryGeneratedColumn("uuid")
	uuid!:string

	@Column("varchar", {length: 64})
	username!:string;

	@Column("varchar",{length:256})
	firstName!:string;

	@Column("varchar",{length:256})
	lastName!:string;

	@Column("varchar",{length:128})
	passwordHash!:string;

	@Column("varchar",{length:64})
	passwordSalt!:string;

	@Column({
		type: "simple-enum",
		enum: AllRolesByPrecedence,
		default: "regular"
	})
	role!: RoleEnum;

	@OneToMany(()=>Device, device => device.user)
	devices!: Promise<Device[]>;
	
	@OneToOne(()=>Order)
	assignedOrder?: Order
	
	passwordVerify(password: string) : boolean {
		return Crypto.verify(password, {hash: this.passwordHash, salt: this.passwordSalt});
	}
	
}