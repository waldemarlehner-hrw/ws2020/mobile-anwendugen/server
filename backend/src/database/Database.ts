import { Connection } from 'typeorm';
import { User} from './model/User';
import {RoleEnum} from "./RoleEnum"
import {Crypto} from "./helpers/Crypto"

export default class Database{
	public static connection : Connection;

	public static async createUser(
		userName: string,
		firstName: string,
		lastName: string,
		password: string,
		role: RoleEnum
	) : Promise<boolean> {
		const hashSaltpair = await Crypto.generate(password);
		console.warn("No user with admin priviledges in database. Creating default root user...");
		try{
			await Database.connection
			.createQueryBuilder()
			.insert()
			.into(User)
			.values([{
				firstName: firstName,
				lastName: lastName,
				username: userName,
				passwordHash: hashSaltpair.hash,
				passwordSalt: hashSaltpair.salt,
				role: role
			}])
			.execute();
			return true;
		}
		catch(ex){
			console.exception(ex);
			return false;
		}
		
	}

}