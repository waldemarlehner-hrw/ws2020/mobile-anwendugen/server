import { Session } from "express-session";

export interface ExpressSessionData extends Session{
	currentUserId?: string
}