const asyncWrapper = require("express-async-handler");

export default asyncWrapper;