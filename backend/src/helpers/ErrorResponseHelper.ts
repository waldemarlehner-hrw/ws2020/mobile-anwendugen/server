import { StatusCodes } from 'http-status-codes';
import { validationResult } from 'express-validator';
import { Request, Response } from "express";

export interface IArgumentErrorResponse{
    reason: string,
    data: string[]
}

export function generateResponseObjectOnError(req: Request, res: Response) : undefined | Response<IArgumentErrorResponse>{
    const errors = validationResult(req);
    if(errors.isEmpty()){
        return undefined;
    }else{
        const array = errors.array().map(e => `[${e.param}]:${e.msg}`)
        return res.status(StatusCodes.BAD_REQUEST).send({reason: "Arguments could not be validated. See data for more information.", data: array})
    }
}