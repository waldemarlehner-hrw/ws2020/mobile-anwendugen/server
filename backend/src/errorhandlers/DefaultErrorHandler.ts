import { Request, Response } from 'express';
import { StatusCodes } from 'http-status-codes';

/** A simple error handler to return a 500 on Error. 
 * Also logs the error with name, message and stack when in development mode.
 */
 export function DefaultErrorHandler(err: any, req: Request, res: Response, next: any){
	// Check if in dev mode
	const data: any = { reason: "An internal error occured."}
	const env = process.env.NODE_ENV || "development"
	if(env === "development"){
		data.error = err.stack;
	}
	console.error(err.stack);
	return res.status(StatusCodes.INTERNAL_SERVER_ERROR).send(data);
}